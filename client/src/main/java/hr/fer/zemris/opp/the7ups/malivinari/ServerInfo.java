package hr.fer.zemris.opp.the7ups.malivinari;

public class ServerInfo {

	private static int port;
	private static String host;
	
	public static int getPort() {
		return port;
	}
	public static void setPort(int port) {
		if (port < 1 || port > 65535){
			throw new IllegalArgumentException("Invalid port!");
		}
		ServerInfo.port = port;
	}
	public static String getHost() {
		return host;
	}
	public static void setHost(String host) {
		ServerInfo.host = host;
	}
	
	public static String getUrl(String resource){
		return "http://" + host + ":" + port + "/" + resource;
	}
	
}
