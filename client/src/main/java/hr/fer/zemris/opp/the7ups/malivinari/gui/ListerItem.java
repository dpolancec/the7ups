package hr.fer.zemris.opp.the7ups.malivinari.gui;

import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.StackPane;

public class ListerItem<T> extends StackPane {

	private T item;
	private SkinnableLabel topTextLabel = new SkinnableLabel();
	private SkinnableLabel bottomTextLabel = new SkinnableLabel();
	private StackPane imageStack = new StackPane();
	private ImageView imageView;
	private boolean selected;
	
	public static final int LIST_ITEM_SIZE = 150;
	public static final int USER_LIST_ITEM_SIZE = 200;
	public static final int IMAGE_SIZE = 64;
	
	public ListerItem(T item, String topText, String bottomText, Image image) {
		super();
		this.item = item;
		if (item instanceof User){
			this.setMaxSize(USER_LIST_ITEM_SIZE, USER_LIST_ITEM_SIZE);
			this.setMinSize(USER_LIST_ITEM_SIZE, USER_LIST_ITEM_SIZE);
		} else {
			this.setMaxSize(LIST_ITEM_SIZE, LIST_ITEM_SIZE);
			this.setMinSize(LIST_ITEM_SIZE, LIST_ITEM_SIZE);
		}
		
		imageStack.setMaxSize(IMAGE_SIZE, IMAGE_SIZE);
		imageStack.setMinSize(IMAGE_SIZE, IMAGE_SIZE);
		
		imageStack.setLayoutX(this.getMaxWidth()/2 - IMAGE_SIZE/2);
		imageStack.setLayoutY(30);
		
		imageView = new ImageView(image);
		imageView.setFitHeight(IMAGE_SIZE);
		imageView.setFitWidth(IMAGE_SIZE);
		imageView.resize(IMAGE_SIZE, IMAGE_SIZE);
		imageStack.getChildren().add(imageView);
		imageStack.setMaxSize(IMAGE_SIZE, IMAGE_SIZE);
		
		topTextLabel.setText(topText);
		bottomTextLabel.setText(bottomText);
		bottomTextLabel.setWrapText(true);
		
		StackPane.setAlignment(topTextLabel, Pos.TOP_CENTER);
		StackPane.setAlignment(bottomTextLabel, Pos.BOTTOM_CENTER);
		
		this.getChildren().addAll(imageStack, topTextLabel, bottomTextLabel);
	}
	
	public void setOnImageClick(EventHandler<MouseEvent> handler){
		imageView.addEventHandler(MouseEvent.MOUSE_CLICKED, (event) -> {
			selected = !selected;
			if (selected){
				this.setBorder(
						new Border(
								new BorderStroke(SkinContext.getSkinProvider().getFontColor(), BorderStrokeStyle.SOLID,
										null, BorderStroke.MEDIUM)));
			} else {
				this.setBorder(Border.EMPTY);
			}
			
			handler.handle(event);
			event.consume();
		});
	}

	public T getItem() {
		return item;
	}

	public boolean isSelected() {
		return selected;
	}
	
}
