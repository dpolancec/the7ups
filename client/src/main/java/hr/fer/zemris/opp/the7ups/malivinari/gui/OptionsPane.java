package hr.fer.zemris.opp.the7ups.malivinari.gui;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import hr.fer.zemris.opp.the7ups.malivinari.gui.skins.DefaultSkinProvider;
import hr.fer.zemris.opp.the7ups.malivinari.gui.skins.SkinProviders;
import hr.fer.zemris.opp.the7ups.malivinari.gui.skins.WhiteBlackSkinProvider;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

public class OptionsPane extends ContentBackgroundPane {
	
	private static final int INPUT_PANE_WIDTH = 400;
	private static final int INPUT_PANE_HEIGHT = 400;

	private Label addressLabel = new SkinnableLabel("Enter server address*:");
	private Label portLabel = new SkinnableLabel("Enter server port*:");
	private Label skinLabel = new SkinnableLabel("Select skin:");
	private Label infoLabel = new SkinnableLabel("*Application must be restarted in order to apply these settings");
	
	private TextField addressTextField;
	private TextField portTextField;
	
	private ComboBox<SkinProvider> skinChooser = new ComboBox<>();
	
	private Button applyButton;
	
	private Alert wrongData = new Alert(AlertType.ERROR);
	
	private static final int INPUT_LENGTH = 160;
	private static final int INPUT_HEIGHT = 30;
	
	public OptionsPane (){
		super();
		
		wrongData.setTitle("Error");
		wrongData.setHeaderText("Invalid data!");
		
		skinChooser.setItems(FXCollections.observableArrayList(SkinProviders.DEFAULT, SkinProviders.WHITE_BLACK));
		skinChooser.setValue(SkinProviders.DEFAULT);
		
		InputStream activePropsStream = getClass().getClassLoader().getResourceAsStream("active.properties");
		Properties oldProps = new Properties();
		try {
			oldProps.load(activePropsStream);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		addressTextField = new TextField(oldProps.getProperty("server.host"));
		addressTextField.setPrefWidth(INPUT_LENGTH);
		addressTextField.setPrefHeight(INPUT_HEIGHT);
		
		portTextField = new TextField(oldProps.getProperty("server.port"));
		portTextField.setPrefWidth(INPUT_LENGTH);
		portTextField.setPrefHeight(INPUT_HEIGHT);
		
		Image confirmImage = new Image(getClass().getResourceAsStream("/images/confirm.png"));
		applyButton = new Button();
		applyButton.setGraphic(new ImageView(confirmImage));
		applyButton.setText("Apply");
		applyButton.setTooltip(new Tooltip("Apply settings"));
		
		BorderPane addressPane = new BorderPane();
		addressPane.setLeft(addressLabel);
		addressPane.setRight(addressTextField);
		BorderPane.setMargin(addressLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(addressTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		BorderPane portPane = new BorderPane();
		portPane.setLeft(portLabel);
		portPane.setRight(portTextField);
		BorderPane.setMargin(portLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(portTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		BorderPane skinPane = new BorderPane();
		skinPane.setLeft(skinLabel);
		skinPane.setRight(skinChooser);
		BorderPane.setMargin(skinLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(skinChooser, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		GridPane inputPane = new GridPane();
		inputPane.setVgap(GuiConstants.DEFAULT_V_GAP);
		inputPane.setHgap(GuiConstants.DEFAULT_H_GAP);
		
		inputPane.add(addressPane, 0, 0);
		inputPane.add(portPane, 0, 2);
		inputPane.add(skinPane, 0, 4);
		inputPane.setAlignment(Pos.CENTER);
		
		BorderPane mainPane = new BorderPane();
//		StackPane.setAlignment(inputPane, Pos.CENTER);
//		StackPane.setAlignment(loginButton, Pos.BOTTOM_CENTER);
		
		mainPane.setCenter(inputPane);
		mainPane.setBottom(applyButton);
		mainPane.setPrefWidth(INPUT_PANE_WIDTH);
		mainPane.setMaxWidth(INPUT_PANE_WIDTH);
		mainPane.setMaxHeight(INPUT_PANE_HEIGHT);
		mainPane.setOpacity(1.0);
		
		BorderPane.setAlignment(applyButton, Pos.CENTER);
		BorderPane.setMargin(applyButton, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				3*GuiConstants.DEFAULT_V_GAP, 0));
		
		this.addContent(mainPane);
	}
	
	public void showWrongDataAlert(){
		wrongData.showAndWait();
	}
	
	public void setOnApply(EventHandler<ActionEvent> handler){
		applyButton.setOnAction((event) -> {
			if (getAddress().isEmpty()){
				blankFieldAlert("Address").showAndWait();
				return;
			}
			
			if (getPort().isEmpty()){
				blankFieldAlert("Port").showAndWait();
				return;
			}
			
			Properties properties = new Properties();
			properties.put("server.port", portTextField.getText());
			properties.put("server.host", addressTextField.getText());
			properties.put("client.skin", skinChooser.getValue().getClass().getName());
			
			SkinContext.setSkinProvider(skinChooser.getValue());
			try {
				properties.store(new FileOutputStream(getClass().getClassLoader().getResource("active.properties").toURI().getPath()), "");
			} catch (Exception e) {
				GuiUtils.exceptionAlert("Error", "An error ocurred while applying settings", "", e);
				e.printStackTrace();
				return;
			}
			handler.handle(event);
			clearFields();
		});
	}
	
	public String getAddress(){
		return addressTextField.getText().trim();
	}
	
	public String getPort(){
		return portTextField.getText().trim();
	}
	
	private Alert blankFieldAlert(String fieldname){
		Alert blankField = new Alert(AlertType.ERROR);
		blankField.setTitle("Error");
		blankField.setHeaderText(fieldname  + " must not be blank.");
		
		return blankField;
	}
	
	public void clearFields(){
		addressTextField.setText("");
		portTextField.setText("");
	}
	
}
