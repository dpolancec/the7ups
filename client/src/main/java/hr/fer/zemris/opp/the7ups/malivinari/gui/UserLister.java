package hr.fer.zemris.opp.the7ups.malivinari.gui;

import java.io.InputStream;

import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.services.SessionService;
import hr.fer.zemris.opp.the7ups.malivinari.services.UserService;
import javafx.scene.image.Image;

public class UserLister extends Lister<User> {
	
	private UserService userService;
	private SessionService sessionService;
	
	public UserLister(){
		super();
	}

	@Override
	public String topText(User item) {
		String online = "";
		String blocked = "";
		if (sessionService.isLoggedIn(item.getUsername(), getClientContext().getToken()).getData()){
			online = "(Online)";
		}
		if (userService.getBlockedUsers(getClientContext().getToken()).getData().contains(item)){
			blocked = "(Blocked)";
		}
		return item.getUsername() + " " + online + blocked;
	}

	@Override
	public String bottomText(User item) {
		String text = (item.getName() != null ? item.getName() : "") + " " + (item.getLastName() != null ? item.getLastName() : "");
		text += (item.getStatus() != null ? ("\n" + "Status: " + item.getStatus()) : "");
		return text;
	}

	@Override
	public Image image(User item) {
		InputStream is = userService.getUserImage(item.getUsername()).getData();
		if (is == null){
			return new Image(getClass().getClassLoader().getResourceAsStream("images/noImage.png"));
		} else {
			return new Image(is);
		}
		
	}

	public synchronized UserService getUserService() {
		return userService;
	}

	public synchronized void setUserService(UserService userService) {
		this.userService = userService;
	}

	public synchronized SessionService getSessionService() {
		return sessionService;
	}

	public synchronized void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}
	
}
