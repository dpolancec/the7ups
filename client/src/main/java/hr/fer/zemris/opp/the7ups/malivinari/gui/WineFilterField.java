package hr.fer.zemris.opp.the7ups.malivinari.gui;

import javafx.scene.control.TextField;

public class WineFilterField extends TextField {

	public WineFilterField(WineLister lister){
		super("");
		this.textProperty().addListener((observable, oldValue, newValue) -> {
			
			lister.setItems(lister.getOriginalItems(), (wine) -> {
				if (newValue.isEmpty()){
					return true;
				}
				
				
				return wine.getName().startsWith(newValue);
			});
		});
		
		this.setEditable(true);
		this.setMaxWidth(120);
	}
}
