package hr.fer.zemris.opp.the7ups.malivinari.models;

import java.util.Date;
import java.util.Set;

public class ServerEvent {
	
	private Date timeCreated = new Date();
	private String eventId;
	private Set<String> relevantUsers;
	private String type;
	private boolean isRequest;
	private String additionalInfo;
	private String initiatorUsername;
	
	public ServerEvent() {
		super();
	}
	public ServerEvent(Set<String> relevantUsers, String type, boolean isRequest) {
		super();
		this.relevantUsers = relevantUsers;
		this.type = type;
		this.isRequest = isRequest;
	}
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public boolean isRequest() {
		return isRequest;
	}
	public void setRequest(boolean isRequest) {
		this.isRequest = isRequest;
	}
	public Date getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	public Set<String> getRelevantUsers() {
		return relevantUsers;
	}
	public void setRelevantUsers(Set<String> relevantUsers) {
		this.relevantUsers = relevantUsers;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((eventId == null) ? 0 : eventId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServerEvent other = (ServerEvent) obj;
		if (eventId == null) {
			if (other.eventId != null)
				return false;
		} else if (!eventId.equals(other.eventId))
			return false;
		return true;
	}
	public String getInitiatorUsername() {
		return initiatorUsername;
	}
	public void setInitiatorUsername(String initiatorUsername) {
		this.initiatorUsername = initiatorUsername;
	}
	public String getAdditionalInfo() {
		return additionalInfo;
	}
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	
}
