package hr.fer.zemris.opp.the7ups.malivinari.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

public class RegisterPane extends ContentBackgroundPane {
	
	private static final int INPUT_PANE_WIDTH = 400;
	private static final int INPUT_PANE_HEIGHT = 400;

	private Label usernameLabel = new Label("Enter username:");
	private Label passwordLabel = new Label("Enter password:");
	private Label repeatPasswordLabel = new Label("Repeat password:");
	private Label emailLabel = new Label("Enter email:");
	
	private TextField usernameTextField;
	private TextField emailTextField;
	private TextField passwordTextField;
	private TextField repeatPasswordTextField;
	
	private Button registerButton;
	
	private Alert usernameAlreadyExists;
	private Alert emailAlreadyExists;
	
	private static final int INPUT_LENGTH = 160;
	private static final int INPUT_HEIGHT = 30;
	
	public RegisterPane (){
		super();
		
		usernameLabel.setTextFill(Color.WHITE);
		passwordLabel.setTextFill(Color.WHITE);
		repeatPasswordLabel.setTextFill(Color.WHITE);
		emailLabel.setTextFill(Color.WHITE);
		
		usernameTextField = new TextField();
		usernameTextField.setPrefWidth(INPUT_LENGTH);
		usernameTextField.setPrefHeight(INPUT_HEIGHT);
		
		emailTextField = new TextField();
		emailTextField.setPrefWidth(INPUT_LENGTH);
		emailTextField.setPrefHeight(INPUT_HEIGHT);
		
		passwordTextField = new PasswordField();
		passwordTextField.setPrefWidth(INPUT_LENGTH);
		passwordTextField.setPrefHeight(INPUT_HEIGHT);
		
		repeatPasswordTextField = new PasswordField();
		repeatPasswordTextField.setPrefWidth(INPUT_LENGTH);
		repeatPasswordTextField.setPrefHeight(INPUT_HEIGHT);
		
		Image confirmImage = new Image(getClass().getResourceAsStream("/images/confirm.png"));
		registerButton = new Button();
		registerButton.setGraphic(new ImageView(confirmImage));
		registerButton.setText("Register");
		registerButton.setTooltip(new Tooltip("Register new account with this data"));
		
		BorderPane usernamePane = new BorderPane();
		usernamePane.setLeft(usernameLabel);
		usernamePane.setRight(usernameTextField);
		BorderPane.setMargin(usernameLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(usernameTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		
		BorderPane emailPane = new BorderPane();
		emailPane.setLeft(emailLabel);
		emailPane.setRight(emailTextField);
		BorderPane.setMargin(emailLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(emailTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		BorderPane passwordPane = new BorderPane();
		passwordPane.setLeft(passwordLabel);
		passwordPane.setRight(passwordTextField);
		BorderPane.setMargin(passwordLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(passwordTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		BorderPane repeatPasswordPane = new BorderPane();
		repeatPasswordPane.setLeft(repeatPasswordLabel);
		repeatPasswordPane.setRight(repeatPasswordTextField);
		BorderPane.setMargin(repeatPasswordLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(repeatPasswordTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		GridPane inputPane = new GridPane();
		inputPane.setVgap(GuiConstants.DEFAULT_V_GAP);
		inputPane.setHgap(GuiConstants.DEFAULT_H_GAP);
		
		inputPane.add(usernamePane, 0, 0);
		inputPane.add(emailPane, 0, 1);
		inputPane.add(passwordPane, 0, 2);
		inputPane.add(repeatPasswordPane, 0, 3);
		inputPane.setAlignment(Pos.CENTER);
		
		BorderPane mainPane = new BorderPane();
		StackPane.setAlignment(inputPane, Pos.CENTER);
		StackPane.setAlignment(registerButton, Pos.BOTTOM_CENTER);
		
		mainPane.setCenter(inputPane);
		mainPane.setBottom(registerButton);
		mainPane.setPrefWidth(INPUT_PANE_WIDTH);
		mainPane.setMaxWidth(INPUT_PANE_WIDTH);
		mainPane.setMaxHeight(INPUT_PANE_HEIGHT);
		mainPane.setOpacity(1.0);
		
		BorderPane.setAlignment(registerButton, Pos.CENTER);
		BorderPane.setMargin(registerButton, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				3*GuiConstants.DEFAULT_V_GAP, 0));
		
		this.addContent(mainPane);
	}
	
	public void showUsernameExistAlert(){
		usernameAlreadyExists.showAndWait();
	}
	
	public void showEmailExistAlert(){
		emailAlreadyExists.showAndWait();
	}
	
	public void setOnRegister(EventHandler<ActionEvent> handler){
		registerButton.setOnAction((event) -> {
			if (usernameTextField.getText().trim().isEmpty()){
				blankFieldAlert("Username").showAndWait();
				return;
			}
			
			if (passwordTextField.getText().trim().isEmpty()){
				blankFieldAlert("Password").showAndWait();
				return;
			}
			
			if (emailTextField.getText().trim().isEmpty()){
				blankFieldAlert("Email").showAndWait();
				return;
			}
			
			if (!passwordTextField.getText().equals(repeatPasswordTextField.getText())){
				Alert passwordsDontMatch = new Alert(AlertType.ERROR);
				passwordsDontMatch.setTitle("Error");
				passwordsDontMatch.setHeaderText("Passwords do not match.");
				passwordsDontMatch.showAndWait();
				return;
			}
			
			handler.handle(event);
			clearFields();
		});
	}
	
	public String getUsername(){
		return usernameTextField.getText().trim();
	}
	
	public String getPassword(){
		return passwordTextField.getText().trim();
	}
	
	public String getEmail(){
		return emailTextField.getText().trim();
	}
	
	private Alert blankFieldAlert(String fieldname){
		Alert blankField = new Alert(AlertType.ERROR);
		blankField.setTitle("Error");
		blankField.setHeaderText(fieldname  + " must not be blank.");
		
		return blankField;
	}
	
	public void clearFields(){
		usernameTextField.setText("");
		passwordTextField.setText("");
		repeatPasswordTextField.setText("");
		emailTextField.setText("");
	}
	
}
