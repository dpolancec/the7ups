package hr.fer.zemris.opp.the7ups.malivinari.gui;

import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;

public class BackdropPane extends StackPane implements Skinnable {

	public BackdropPane(){
		SkinContext.add(this);
		this.setBackground(new Background(SkinContext.getSkinProvider().getBackdrop()));
		this.setOpacity(SkinContext.getSkinProvider().getBackdropOpacity());
	}
	
	public BackdropPane(Region region) {
		super(region);
		this.setBackground(new Background(SkinContext.getSkinProvider().getBackdrop()));
		this.setOpacity(SkinContext.getSkinProvider().getBackdropOpacity());
	}

	@Override
	public void notifyProviderChanged(SkinProvider newProvider) {
		this.setBackground(new Background(newProvider.getBackdrop()));
		this.setOpacity(newProvider.getBackdropOpacity());
	}

}
