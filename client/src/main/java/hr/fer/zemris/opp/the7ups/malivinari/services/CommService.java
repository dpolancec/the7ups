package hr.fer.zemris.opp.the7ups.malivinari.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import hr.fer.zemris.opp.the7ups.malivinari.ServerInfo;
import hr.fer.zemris.opp.the7ups.malivinari.responses.CommListResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.CommResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.GeneralResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.GroupListResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.MessageListResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.RequestResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.UserListResponse;

@Service
public class CommService {

	public RequestResponse createCommCreateRequest(String user, String token){
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		
		if (token != null){
			params.add("token", token);
		}
		
		if (user != null){
			params.add("user", user);
		}
		RestTemplate template = new RestTemplate();
		RequestResponse response = template.postForObject(ServerInfo.getUrl("private-comm-request-create"), params,
				RequestResponse.class);
		
		System.out.println(response.getStatus());
		return response;
	}
	
	public RequestResponse createCommCreateRequest(int groupId, String token){
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		
		if (token != null){
			params.add("token", token);
		}
		
		params.add("id", Integer.toString(groupId));

		RestTemplate template = new RestTemplate();
		RequestResponse response = template.postForObject(ServerInfo.getUrl("comm-group-request-create"), params,
				RequestResponse.class);
		
		System.out.println(response.getStatus());
		return response;
	}
	
	public CommResponse getComm(int commId, String token){
		
		RestTemplate template = new RestTemplate();
		CommResponse response = template.getForObject(ServerInfo.getUrl("comm/{id}?token={token}"), 
				CommResponse.class, commId, token);
		System.out.println(response.getStatus());
		
		return response;
	}
	
	public GeneralResponse leaveComm(int commId, String token){
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("id", Integer.toString(commId));
		
		if (token != null){
			params.add("token", token);
		}
		
		RestTemplate template = new RestTemplate();
		GeneralResponse response = template.postForObject(ServerInfo.getUrl("comm-leave"), params,
				GeneralResponse.class);
		
		return response;
	}
	
	public UserListResponse getCommParticipants(int commId, String token){
		
		RestTemplate template = new RestTemplate();
		UserListResponse response = template.getForObject(ServerInfo.getUrl("participants/{id}?token={token}"), 
				UserListResponse.class, Integer.toString(commId), token);
		
		return response;
	}
	
	public CommListResponse getComms(String token){
		RestTemplate template = new RestTemplate();
		CommListResponse response = template.getForObject(ServerInfo.getUrl("comms?token={token}"), 
				CommListResponse.class, token);
		
		System.out.println(response.getStatus());
		System.out.println(response.getData().size());
		
		return response;
	}
	
	public MessageListResponse getMessages(int commId, String token){
		RestTemplate template = new RestTemplate();
		MessageListResponse response = template.getForObject(ServerInfo.getUrl("messages/{id}?token={token}"), 
				MessageListResponse.class, Integer.toString(commId), token);
		
		return response;
	}
	
	public GeneralResponse appendComm(int commId, String token, String text){
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("id", Integer.toString(commId));
		
		if (token != null){
			params.add("token", token);
		}
		
		if (token != null){
			params.add("text", text);
		}
		
		RestTemplate template = new RestTemplate();
		GeneralResponse response = template.postForObject(ServerInfo.getUrl("comm-append"), params,
				GeneralResponse.class);
		
		return response;
	}
	
}
