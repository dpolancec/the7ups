package hr.fer.zemris.opp.the7ups.malivinari.gui;

import javafx.scene.Node;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;

public class ListerPane extends ContentBackgroundPane {

	private static final int MAX_WIDTH = 1280;
	
	private BorderPane mainPane = new BorderPane();
	private Lister lister;
	private ToolBar toolbar = new ToolBar();
	private BorderPane bodyBorderPane = new BorderPane();
	private FlowPane filterPane = new FlowPane();
	
	public ListerPane(Node ... toolbarItems){
		toolbar = new ToolBar(toolbarItems);
		mainPane.setTop(toolbar);
		bodyBorderPane.setBottom(filterPane);
		bodyBorderPane.setMaxWidth(MAX_WIDTH);
		filterPane.setMaxWidth(MAX_WIDTH);
		ContentBackgroundPane bodyPane = new ContentBackgroundPane();
		bodyPane.addContent(bodyBorderPane);
		mainPane.setCenter(bodyPane);
		mainPane.backgroundProperty().bind(this.backgroundProperty());
		
		this.addContent(mainPane);
	}
	
	public synchronized void setLister(Lister lister){
		this.lister = lister;
		lister.setMaxWidth(MAX_WIDTH);
		bodyBorderPane.setCenter(lister);
	}
	
	public synchronized Lister getLister() {
		return lister;
	}

	public synchronized void setFilters(Node ... filters){
		filterPane.getChildren().clear();
		filterPane.getChildren().addAll(filters);
	}
	
}
