package hr.fer.zemris.opp.the7ups.malivinari.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import hr.fer.zemris.opp.the7ups.malivinari.ServerInfo;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.responses.GeneralResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.GroupListResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.GroupResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.RequestResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.UserListResponse;

@Service
public class GroupService {
	
	public RequestResponse createGroupCreateRequest(List<User> invitedUsers, String groupName, String token){
		
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
		if (groupName != null){
			params.add("name", groupName);
		}
		
		if (token != null){
			params.add("token", token);
		}
		
		int numInv = invitedUsers.size();
		for (int i = 0; i < numInv; i++){
			params.add("user[]", invitedUsers.get(i).getUsername());
		}
		
		RestTemplate template = new RestTemplate();
		RequestResponse response = template.postForObject(ServerInfo.getUrl("group/request-create"), params,
				RequestResponse.class);
		
		System.out.println(response.getStatus());
		
		return response;
	}
	
	public GroupResponse getGroup(int groupId){
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		
		params.add("id", Integer.toString(groupId));
		
		RestTemplate template = new RestTemplate();
		GroupResponse response = template.getForObject(ServerInfo.getUrl("group/{id}"), 
				GroupResponse.class, params);
		
		return response;
	}
	
	public RequestResponse createGroupDisbandRequest(int groupId, String token){
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("id", Integer.toString(groupId));
		
		if (token != null){
			params.add("token", token);
		}
		
		RestTemplate template = new RestTemplate();
		RequestResponse response = template.postForObject(ServerInfo.getUrl("group/request-disband"), params,
				RequestResponse.class);
		
		return response;
	}
	
	public UserListResponse getGroupMembers(int groupId, String token){
		
		RestTemplate template = new RestTemplate();
		UserListResponse response = template.getForObject(ServerInfo.getUrl("members/{id}?token={token}"), 
				UserListResponse.class, Integer.toString(groupId), token);
		
		return response;
	}
	
	public GroupListResponse getGroups(String username, String token){
		RestTemplate template = new RestTemplate();
		GroupListResponse response = template.getForObject(ServerInfo.getUrl("groups/{username}?token={token}"), 
				GroupListResponse.class, username, token);
		
		System.out.println(response.getStatus());
		System.out.println(response.getData().size());
		
		return response;
	}
	
	public GeneralResponse leaveGroup(int groupId, String token){
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		
		params.add("id", Integer.toString(groupId));
		params.add("token", token);
		
		RestTemplate template = new RestTemplate();
		GeneralResponse response = template.postForObject(ServerInfo.getUrl("group-leave"), params,
				GeneralResponse.class);
		
		return response;
	}
	
}
