package hr.fer.zemris.opp.the7ups.malivinari.gui;

import java.util.Iterator;
import java.util.List;

import hr.fer.zemris.opp.the7ups.malivinari.models.Group;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.services.GroupService;
import javafx.scene.image.Image;

public class GroupLister extends Lister<Group> {

	private GroupService groupService;
	
	@Override
	public String topText(Group item) {
		return item.getName();
	}

	@Override
	public String bottomText(Group item) {
		String users = "Members: ";
		List<User> members = groupService.getGroupMembers(item.getId(), getClientContext().getToken()).getData();
		Iterator<User> memIter = members.iterator();
		while (memIter.hasNext()){
			String username = memIter.next().getUsername();
			
			users += username;
			if (memIter.hasNext()){
				users += ", ";
			}
		}
		
		return users;
	}

	@Override
	public Image image(Group item) {
		return new Image(getClass().getResourceAsStream("/images/group.png"));
	}

	public GroupService getGroupService() {
		return groupService;
	}

	public void setGroupService(GroupService groupService) {
		this.groupService = groupService;
	}

}
