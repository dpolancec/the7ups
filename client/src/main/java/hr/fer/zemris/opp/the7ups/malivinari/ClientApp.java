package hr.fer.zemris.opp.the7ups.malivinari;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import hr.fer.zemris.opp.the7ups.malivinari.client.ClientContext;
import hr.fer.zemris.opp.the7ups.malivinari.gui.AddGroupPane;
import hr.fer.zemris.opp.the7ups.malivinari.gui.AddWinePane;
import hr.fer.zemris.opp.the7ups.malivinari.gui.CommLister;
import hr.fer.zemris.opp.the7ups.malivinari.gui.ContentBackgroundPane;
import hr.fer.zemris.opp.the7ups.malivinari.gui.GroupLister;
import hr.fer.zemris.opp.the7ups.malivinari.gui.GuiConstants;
import hr.fer.zemris.opp.the7ups.malivinari.gui.GuiUtils;
import hr.fer.zemris.opp.the7ups.malivinari.gui.Inspector;
import hr.fer.zemris.opp.the7ups.malivinari.gui.ListerPane;
import hr.fer.zemris.opp.the7ups.malivinari.gui.LoginPane;
import hr.fer.zemris.opp.the7ups.malivinari.gui.Messenger;
import hr.fer.zemris.opp.the7ups.malivinari.gui.OptionsPane;
import hr.fer.zemris.opp.the7ups.malivinari.gui.RegisterPane;
import hr.fer.zemris.opp.the7ups.malivinari.gui.SkinContext;
import hr.fer.zemris.opp.the7ups.malivinari.gui.SkinProvider;
import hr.fer.zemris.opp.the7ups.malivinari.gui.UserEditPane;
import hr.fer.zemris.opp.the7ups.malivinari.gui.UserFilterField;
import hr.fer.zemris.opp.the7ups.malivinari.gui.UserLister;
import hr.fer.zemris.opp.the7ups.malivinari.gui.WineFilterField;
import hr.fer.zemris.opp.the7ups.malivinari.gui.WineLister;
import hr.fer.zemris.opp.the7ups.malivinari.events.EventTypes;
import hr.fer.zemris.opp.the7ups.malivinari.models.Communication;
import hr.fer.zemris.opp.the7ups.malivinari.models.CommunicationType;
import hr.fer.zemris.opp.the7ups.malivinari.models.Message;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.models.Wine;
import hr.fer.zemris.opp.the7ups.malivinari.models.Group;
import hr.fer.zemris.opp.the7ups.malivinari.responses.CommListResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.CommResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.GeneralResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.GroupResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.RequestResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.ServerEventResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.Status;
import hr.fer.zemris.opp.the7ups.malivinari.responses.TokenResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.UserResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.WineResponse;
import hr.fer.zemris.opp.the7ups.malivinari.services.CommService;
import hr.fer.zemris.opp.the7ups.malivinari.services.EventService;
import hr.fer.zemris.opp.the7ups.malivinari.services.GroupService;
import hr.fer.zemris.opp.the7ups.malivinari.services.SessionService;
import hr.fer.zemris.opp.the7ups.malivinari.services.UserService;
import hr.fer.zemris.opp.the7ups.malivinari.services.WineService;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.Window;

@Configuration
@ComponentScan("hr.fer.zemris.opp.the7ups.malivinari")
@PropertySource("classpath:active.properties")
public class ClientApp extends Application {

	private static final double LANDING_WIDTH = 640;
	private static final double LANDING_HEIGHT = 480;
	
	private static final double LOGIN_WIDTH = 640;
	private static final double LOGIN_HEIGHT = 320;
	
	private static final double MESSENGER_WIDTH = 1280;
	private static final double MESSENGER_HEIGHT = 760;
	
	private static final double UPDATE_WIDTH = 1024;
	private static final double UPDATE_HEIGHT = 760;
	
	private static final double INSPECTOR_WIDTH = 480;
	private static final double INSPECTOR_HEIGHT = 320;
	
	private static final long LOGIN_RETRY = 100;
	
	private static final double MY_WIDTH = 1440;
	private static final double MY_HEIGHT = 1000;
	
	private static final int PING_INTERVAL = 1_000;
	
	private Environment env;
	private Stage landingStage;
	private Stage primaryStage;
	private Stage loginStage;
	private Stage registerStage;
	private Stage optionsStage;
	
	private SessionService sessionService;
	private UserService userService;
	private WineService wineService;
	private GroupService groupService;
	private EventService eventService;
	private ClientContext clientContext;
	private CommService commService;
	private boolean stopPing;
	
	private Thread pinger;
	private Thread listener;
	
	private Label infoLabel = new Label();
	private Label messageCountLabel = new Label();
	private int messageCounter = 0;
	
	private LoginPane loginPane = new LoginPane();
	private ListerPane mainPane;
	
	private UserLister userLister = new UserLister();
	private WineLister wineLister = new WineLister();
	private GroupLister groupLister = new GroupLister();
	private CommLister commLister = new CommLister();
	
	private TextField userFilter = new UserFilterField(userLister);
	private TextField wineFilter = new WineFilterField(wineLister);
	
	private Button userAllUsersButton = new Button();
	private Button userBlockButton = new Button();
	private Button userAddWineButton = new Button();
	private Button userUpdateButton = new Button();
	
	private Button userGetMyWinesButton = new Button();
	private Button userGetOtherWinesButton = new Button();
	private Button userUnblockButton = new Button();
	private Button userBlockedUsersButton = new Button();
	
	private Button userGetMyGroupsButton = new Button();
	private Button userGetOtherGroupsButton = new Button();
	private Button userInspectButton = new Button();
	
	private Button groupAddButton = new Button();
	private Button groupLeaveButton = new Button();
	
	private Button userMyCommsButton = new Button();
	private Button commRequestButton = new Button();
	private Button commGetMessagesButton = new Button();
	
	private Button wineRemoveButton = new Button();
	
	private Map<Integer, Messenger> activeMessengers = new HashMap<>();
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		this.primaryStage = primaryStage;
		ApplicationContext context = new AnnotationConfigApplicationContext(ClientApp.class);
		env = context.getBean(Environment.class);
		userService = context.getBean(UserService.class);
		sessionService = context.getBean(SessionService.class);
		wineService = context.getBean(WineService.class);
		groupService = context.getBean(GroupService.class);
		eventService = context.getBean(EventService.class);
		clientContext = context.getBean(ClientContext.class);
		commService = context.getBean(CommService.class);
		
		wineLister.setWineService(wineService);
		userLister.setUserService(userService);
		userLister.setSessionService(sessionService);
		groupLister.setGroupService(groupService);
		commLister.setCommService(commService);
		commLister.setClientContext(clientContext);
		
		try{
			ServerInfo.setPort(Integer.parseInt(env.getProperty("server.port")));
			ServerInfo.setHost(env.getProperty("server.host"));
			SkinContext.setSkinProvider((SkinProvider) getClass()
					.getClassLoader().loadClass(env.getProperty("client.skin")).newInstance());
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Configuration error");
			alert.setHeaderText("Active configuration file corrupt, loading default configuration file");
			alert.showAndWait();
			Properties properties = new Properties();
			properties.load(getClass().getClassLoader().getResourceAsStream("default.properties"));
			ServerInfo.setPort(Integer.parseInt(properties.getProperty("server.port")));
			ServerInfo.setHost(properties.getProperty("server.host"));
			SkinContext.setSkinProvider((SkinProvider) getClass()
					.getClassLoader().loadClass(properties.getProperty("client.skin")).newInstance());
		}
		
		System.out.println("Sending requests to: " + ServerInfo.getHost() + ":" + ServerInfo.getPort());
		landingStage = new Stage();
		Scene landingScene = new Scene(getLanding(), LANDING_WIDTH, LANDING_HEIGHT);
		landingStage.setScene(landingScene);
		landingStage.show();
	}
	
	private ContentBackgroundPane getLanding(){
		ContentBackgroundPane contentPane = new ContentBackgroundPane();
		GridPane menuPane = new GridPane();
		menuPane.setVgap(GuiConstants.DEFAULT_V_GAP);
		menuPane.setHgap(GuiConstants.DEFAULT_H_GAP);
		
		Button loginPageButton = new Button("Log in");
		loginPageButton.setOnAction((event) -> {
			showLogin();
			landingStage.hide();
		});
		
		Button registerPageButton = new Button("Register");
		registerPageButton.setOnAction((event) -> {
			showRegister();
			landingStage.hide();
		});
		
		Button optionsPageButton = new Button("Options");
		optionsPageButton.setOnAction((event) -> {
			showOptions();
			landingStage.hide();
		});
		
		menuPane.add(loginPageButton, 0, 0);
		menuPane.add(registerPageButton, 0, 1);
		menuPane.add(optionsPageButton, 0, 2);
		
		loginPageButton.setMinSize(90, 25);
		registerPageButton.setMinSize(90, 25);
		optionsPageButton.setMinSize(90, 25);
		menuPane.setAlignment(Pos.CENTER);
		
		StackPane.setMargin(menuPane, new Insets(3*GuiConstants.DEFAULT_V_GAP, 3*GuiConstants.DEFAULT_H_GAP,
				3*GuiConstants.DEFAULT_V_GAP, 3*GuiConstants.DEFAULT_H_GAP));
		menuPane.setMaxHeight(100);
		menuPane.setMaxWidth(300);
		
		contentPane.addContent(menuPane);
		return contentPane;
	}

	private void showRegister() {
		registerStage = new Stage();
		RegisterPane registerPane = new RegisterPane();
		registerPane.setOnRegister((regEvent) -> {
			UserResponse response = null;
			try{
				response = userService.registerUser(null, null, registerPane.getUsername(), 
						registerPane.getPassword(), registerPane.getEmail());
				if (response == null){
					throw new RuntimeException();
				}
			} catch (Exception e){
				e.printStackTrace();
				GuiUtils.exceptionAlert("Error", "User registration error", 
						"An error occurred during registration.", e);
				return;
			}
			
			if (response.getStatus() == Status.USERNAME_TAKEN){
				GuiUtils.errorAlert("Error", "Username taken").showAndWait();
				return;
			}
			
			if (response.getStatus() == Status.EMAIL_TAKEN){
				GuiUtils.errorAlert("Error", "Email taken").showAndWait();
				return;
			}
			
			if (response.getStatus() == Status.BAD_USERNAME){
				GuiUtils.errorAlert("Error", "Username must be atleast 6 characters long").showAndWait();
				return;
			}
			
			if (response.getStatus() == Status.BAD_PASSWORD){
				GuiUtils.errorAlert("Error", "Password must be between 8 and 24 characters long").showAndWait();
				return;
			}
			
			landingStage.show();
			registerStage.hide();
		});
		Scene regScene = new Scene(registerPane, LOGIN_WIDTH, LOGIN_HEIGHT);
		registerStage.setScene(regScene);
		registerStage.setTitle("Register");
		registerStage.show();
		
	}

	private void showLogin() {
		loginStage = new Stage();
		Scene loginScene = new Scene(loginPane, LOGIN_WIDTH, LOGIN_HEIGHT);
		loginStage.setScene(loginScene);
		loginStage.setTitle("Log in");
		
		loginPane.setOnLogin((event) -> {
			
			String username = loginPane.getUsername();
			String password = loginPane.getPassword();
			
			try{
				TokenResponse response;
				try{
					response = sessionService.logIn(username, password);
				} catch (HttpClientErrorException hce){
					try {
						Thread.sleep(LOGIN_RETRY);
					} catch (Exception e) {
						e.printStackTrace();
					}
					response = sessionService.logIn(username, password);
				}
				
				
				if (response.getStatus() == Status.FORBIDDEN){
					GuiUtils.errorAlert("Error", "Wrong username or password").showAndWait();
				}
				
				if (response.getStatus() == Status.TOO_MANY_LOGGED_IN){
					GuiUtils.errorAlert("Error", "Too many users are logged in at the moment. "
							+ "Please wait 10 minutes and try again.").showAndWait();
				}
				
				if (response.getStatus() == Status.OK){
					System.out.println(response.getData());
					clientContext.setToken(response.getData());
					clientContext.setUsername(username);
					System.out.println(clientContext.getToken());
					if (pinger == null || !pinger.isAlive()){
						pinger = new Thread(() -> {
							//System.out.println("Pinger started");
							while (primaryStage != null || primaryStage.isShowing()){
								try{
									GeneralResponse pingResponse = sessionService.see(clientContext.getToken());
									//System.out.println(pingResponse.getStatus());
									if (pingResponse.getStatus() != Status.OK){
										break;
									}
									//System.out.println("Ping successful");
								} catch (Exception e){
									e.printStackTrace();
									break;
								}
								
								try {
									Thread.sleep(PING_INTERVAL);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
							
							Platform.runLater(() -> {
								GuiUtils.noConnectionAlert().showAndWait();
								primaryStage.hide();
								if (loginStage == null || !loginStage.isShowing()){
									loginStage.show();
								}
							});
							
						});
//						
						pinger.setDaemon(true);
						pinger.start();
						
//						if (listener == null || !listener.isAlive()){
//							listener = new Thread(() -> {
//								System.out.println("Listener started");
//								while (!isStopPing()){
//									try {
//										Thread.sleep(PING_INTERVAL);
//									} catch (Exception e) {
//										e.printStackTrace();
//									}
//									
//									try{
//										ServerEventResponse eventResponse = eventService.collect(clientContext.getToken());
//										System.out.println(eventResponse);
//										if (eventResponse.getStatus() != Status.OK){
//											continue;
//										}
//										
//										Platform.runLater(() -> {
//											switch (eventResponse.getType()) {
//											case EventTypes.GROUP_REQUEST:
//												groupConfirm(eventResponse); break;
//											case EventTypes.REJECT:
//												GuiUtils.errorAlert("Request rejected", "A user rejected the request")
//														.showAndWait(); break;
//											case EventTypes.SUCCESS:
//												GuiUtils.infoAlert("Request accepted",
//														"Request initiated by " + eventResponse.getInitiatorUsername()
//																+ " has been accepted by all users")
//														.showAndWait(); break;
//											}
//										});
//										
//									} catch (Exception e){}
//									
//								}
						if (listener == null || !listener.isAlive()){
							listener = new Thread(() -> {
								System.out.println("Listener started");
								while (true){
									System.out.println("Listener entered a new loop");
									try{
										ServerEventResponse eventResponse = eventService.listen(clientContext.getToken());
										System.out.println(eventResponse);
										if (eventResponse.getStatus() != Status.OK){
											continue;
										}
										
										Platform.runLater(() -> {
											switch (eventResponse.getType()) {
											case EventTypes.GROUP_REQUEST:
												groupConfirm(eventResponse); break;
											case EventTypes.COMM_REQUEST:
												commConfirm(eventResponse); break;
											case EventTypes.REJECT:
												GuiUtils.errorAlert("Request rejected", "A user rejected the request initiated by "
														+ eventResponse.getFinalizedRequestInitiator())
														.showAndWait(); break;
											case EventTypes.SUCCESS:
												GuiUtils.infoAlert("Request accepted",
														"Request initiated by " + eventResponse.getFinalizedRequestInitiator()
																+ " has been accepted by all users")
														.showAndWait();
												break;
											case EventTypes.COMM_SHOW:
												int commId = Integer.parseInt(eventResponse.getAdditionalInfo());
												showNewMessenger(commId);
												break;
											case EventTypes.COMM_APPEND:
												String additionalInfo = eventResponse.getAdditionalInfo();
												int appendCommId = Integer.parseInt(additionalInfo.substring(0, additionalInfo.indexOf(";")));
												Messenger messenger = activeMessengers.get(appendCommId);
												if (messenger == null){
													break;
												}
												String message = additionalInfo.substring(additionalInfo.indexOf(";") + 1);
												messenger.appendMessage(message);
												Stage messengerWindow = (Stage) messenger.getScene().getWindow();
												if (!messengerWindow.isFocused()){
													messengerWindow.hide();
													messengerWindow.show();
												}
												break;
											case EventTypes.COMM_END:
												int endCommId = Integer.parseInt(eventResponse.getAdditionalInfo());
												Messenger endMessenger = activeMessengers.remove(endCommId);
												if (endMessenger == null){
													break;
												}
												Stage endMessengerWindow = (Stage) endMessenger.getScene().getWindow();
												GuiUtils.infoAlert("Notification", "Communication has ended").showAndWait();
												endMessengerWindow.hide();
												break;
											}
											
										});
										
									} catch (Exception e){
										e.printStackTrace();
									}
									
								}
								//System.out.println("Listener dead");
							});
							
							listener.setDaemon(true);
							listener.start();
						}
						
						if (primaryStage == null || !primaryStage.isShowing()){
							showPrimary();
						}
						loginStage.hide();
					}
				}
			} catch (ResourceAccessException e) {
				GuiUtils.noConnectionAlert().showAndWait();
			}
			
		});
		
		loginStage.show();
	}
	
	private void showOptions(){
		optionsStage = new Stage();
		OptionsPane optionsPane = new OptionsPane();
		Scene optionsScene = new Scene(optionsPane, LOGIN_WIDTH, LOGIN_HEIGHT);
		optionsStage.setScene(optionsScene);
		optionsPane.setOnApply((event) -> {
			landingStage.show();
			optionsStage.hide();
		});
		optionsStage.show();
	}
	
	private synchronized void showNewMessenger(int commId) {
		Messenger messenger = new Messenger();
		String token = clientContext.getToken();
		Stage messageStage = new Stage();
		Scene messageScene = new Scene(messenger, MESSENGER_WIDTH, MESSENGER_HEIGHT);
		messageStage.setScene(messageScene);
		System.out.println("CommId: " + commId);
//		try {
//			Thread.sleep(DATABASE_LATENCY_COMPENSATION);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
		Communication comm = commService.getComm(commId, clientContext.getToken()).getData();
		System.out.println("Comm: " + comm.getId());
		
		System.out.println(comm);
		if (comm.getTimeEnded() != null){
			List<Message> messages = commService.getMessages(commId, token).getData();
			messages.sort((m1, m2) -> m1.getTimeCreated().compareTo(m2.getTimeCreated()));
			for (Message message: messages){
				messenger.appendMessage(message.getBody());
			}
			
			messenger.hideInput();
		} else {
			activeMessengers.put(commId, messenger);
			messenger.setOnSend((event) -> {
				commService.appendComm(commId, token, messenger.getSendText());
//				if (!messageStage.isFocused()){
//					messageStage.hide();
//					messageStage.show();
//				}
			});
			messageStage.setOnCloseRequest((event) -> {
				commService.leaveComm(commId, token);
				activeMessengers.remove(commId);
			});
		}
		
		if (comm.getType().equals(CommunicationType.GROUP)){
			messageStage.setTitle(comm.getGroupName());
		} else {
			List<User> participants = commService.getCommParticipants(commId, token).getData();
			String title = "";
			Iterator<User> userIter = participants.iterator();
			while (userIter.hasNext()){
				User user = userIter.next();
				title += user.getUsername();
				if (userIter.hasNext()){
					title += ", ";
				}
			}
			
			messageStage.setTitle(title);
		}
		
		messageStage.show();
	}

	private void commConfirm(ServerEventResponse eventResponse) {
		if (sessionService.isBusy(clientContext.getUsername()).getData()){
			eventService.decline(clientContext.getToken(), eventResponse.getEventId());
			return;
		}
		String initiator = eventResponse.getInitiatorUsername();
		if (initiator.equals(clientContext.getUsername())){
			eventService.accept(clientContext.getToken(), eventResponse.getEventId());
			return;
		}
		
		ButtonType type = GuiUtils.confirmAlert("Communication request", initiator +  " wants to communicate with you. "
				+ "Do you agree?", "").showAndWait().get();
		if (!type.equals(ButtonType.OK)){
			eventService.decline(clientContext.getToken(), eventResponse.getEventId());
		} else {
			eventService.accept(clientContext.getToken(), eventResponse.getEventId());
		}
		
	}

	private void groupConfirm(ServerEventResponse eventResponse) {
		String initiator = eventResponse.getInitiatorUsername();
		if (initiator.equals(clientContext.getUsername())){
			eventService.accept(clientContext.getToken(), eventResponse.getEventId());
			return;
		}
		String otherInvitedText = "Invited users: ";
		Iterator<String> relIter = eventResponse.getRelevantUsers().iterator();
		while (relIter.hasNext()){
			String user = relIter.next();
			if (user.equals(initiator)){
				continue;
			}
			
			otherInvitedText += user;
			if (relIter.hasNext()){
				otherInvitedText += ", ";
			}
		}
		
		otherInvitedText += ".";
		ButtonType type = GuiUtils.confirmAlert("Group request", initiator +  " invited you to join their group. "
				+ "Do you want to join?", otherInvitedText).showAndWait().get();
		if (!type.equals(ButtonType.OK)){
			eventService.decline(clientContext.getToken(), eventResponse.getEventId());
		} else {
			eventService.accept(clientContext.getToken(), eventResponse.getEventId());
		}
	}

	private void showPrimary(){
		// button definitions
		groupLister.setClientContext(clientContext);
		userLister.setClientContext(clientContext);
		userLister.setItems(userService.getAllUsers().getData(), null);
		Image imageGetAllUsers = new Image(getClass().getResourceAsStream("/images/download.png"));
		userAllUsersButton.setGraphic(new ImageView(imageGetAllUsers));
		userAllUsersButton.setTooltip(new Tooltip("All users"));
		
		userBlockedUsersButton.setGraphic(new ImageView(imageGetAllUsers));
		userBlockedUsersButton.setTooltip(new Tooltip("My blocked users"));

		Image imageAddWine = new Image(getClass().getResourceAsStream("/images/add.png"));
		userAddWineButton.setGraphic(new ImageView(imageAddWine));
		userAddWineButton.setTooltip(new Tooltip("Add wine"));
		
		Image imageUpdate = new Image(getClass().getResourceAsStream("/images/update.png"));
		userUpdateButton.setGraphic(new ImageView(imageUpdate));
		userUpdateButton.setTooltip(new Tooltip("Update your info"));

		Image imageGetWines = new Image(getClass().getResourceAsStream("/images/drink.png"));
		userGetMyWinesButton.setGraphic(new ImageView(imageGetWines));
		userGetMyWinesButton.setTooltip(new Tooltip("My wines"));
		
		Image imageComms = new Image(getClass().getResourceAsStream("/images/message.png"));
		userMyCommsButton.setGraphic(new ImageView(imageComms));
		userMyCommsButton.setTooltip(new Tooltip("My communications"));
		
		Image imageInspect = new Image(getClass().getResourceAsStream("/images/info.png"));
		userInspectButton.setGraphic(new ImageView(imageInspect));
		userInspectButton.setTooltip(new Tooltip("User info"));
		
		commRequestButton.setGraphic(new ImageView(imageComms));
		commRequestButton.setTooltip(new Tooltip("Send communication request"));
		
		Image imageMail = new Image(getClass().getResourceAsStream("/images/email.png"));
		commGetMessagesButton.setGraphic(new ImageView(imageMail));
		commGetMessagesButton.setTooltip(new Tooltip("View messages"));
		
		userGetOtherWinesButton.setGraphic(new ImageView(imageGetWines));
		userGetOtherWinesButton.setTooltip(new Tooltip("Other's wines"));
		
		Image imageBlock = new Image(getClass().getResourceAsStream("/images/cancel.png"));
		userBlockButton.setGraphic(new ImageView(imageBlock));
		userBlockButton.setTooltip(new Tooltip("Block selected users"));
		
		groupLeaveButton.setGraphic(new ImageView(imageBlock));
		groupLeaveButton.setTooltip(new Tooltip("Leave group(s)"));
		
		wineRemoveButton.setGraphic(new ImageView(imageBlock));
		wineRemoveButton.setTooltip(new Tooltip("Remove wine"));
		
		Image imageUnblock = new Image(getClass().getResourceAsStream("/images/add.png"));
		userUnblockButton.setGraphic(new ImageView(imageUnblock));
		userUnblockButton.setTooltip(new Tooltip("Unblock selected users"));
		
		Image imageGroup = new Image(getClass().getResourceAsStream("/images/group.png"));
		userGetMyGroupsButton.setGraphic(new ImageView(imageGroup));
		userGetMyGroupsButton.setTooltip(new Tooltip("My groups"));
		
		userGetOtherGroupsButton.setGraphic(new ImageView(imageGroup));
		userGetOtherGroupsButton.setTooltip(new Tooltip("Other groups"));
		
		Image imageGroupAdd = new Image(getClass().getResourceAsStream("/images/group_add.png"));
		groupAddButton.setGraphic(new ImageView(imageGroupAdd));
		groupAddButton.setTooltip(new Tooltip("Invite selected users to group"));
		
		userAllUsersButton.setOnAction((event) -> {
			infoLabel.setText("All registered users");
			showUserLister();
			List<User> users = userService.getAllUsers().getData();
			users.removeIf((user) -> userService.getBlockedUsers(clientContext.getToken()).getData().contains(user));
			userLister.setItems(userService.getAllUsers().getData(), null);
		});
		
		userBlockedUsersButton.setOnAction((event) -> {
			infoLabel.setText("Your blocked users");
			showUserLister();
			userLister.setItems(myBlockedUsers(), null);
		});
		
		mainPane = new ListerPane(userAllUsersButton, userUpdateButton, userAddWineButton, wineRemoveButton, 
				userGetMyWinesButton, userGetMyGroupsButton, userMyCommsButton, userBlockedUsersButton,
				new Separator(), userInspectButton, userGetOtherWinesButton, userGetOtherGroupsButton, groupAddButton, groupLeaveButton,
				commRequestButton, commGetMessagesButton, userBlockButton, userUnblockButton, 
				new Separator(), infoLabel);
		
		userAddWineButton.setOnAction((event) -> {
		
			Stage addWineStage = new Stage();
			AddWinePane addWinePane = new AddWinePane();
			addWinePane.setOnAdd((addEvent) -> {
				try{
					WineResponse response = wineService.addWine(addWinePane.getWineName(), clientContext.getToken(),
							addWinePane.getImgPath());
					System.out.println(response.getData().getName());
				} catch (Exception e){
					GuiUtils.exceptionAlert("Error", "An error occurred while adding wine", 
							"", e);
				}
				addWineStage.hide();
			});
			Scene addWineScene = new Scene(addWinePane, LOGIN_WIDTH, LOGIN_HEIGHT);
			addWineStage.setScene(addWineScene);
			addWineStage.setTitle("Add wine");
			addWineStage.show();
		});
		
		wineRemoveButton.setOnAction((event) -> {
			
			Set<Wine> selectedWines = wineLister.getSelectedItems();
			if (selectedWines.size() == 0){
				GuiUtils.invalidSelectionAlert().showAndWait();
				return;
			}
			
			for (Wine wine: selectedWines){
				wineService.removeWine(wine.getId(), clientContext.getToken());
			}
		});
		
		userUpdateButton.setOnAction((event) -> {
			
			Stage updateProfileStage = new Stage();
			User me = userService.getUser(clientContext.getUsername(), clientContext.getToken()).getData();
			UserEditPane userEditPane = new UserEditPane(me);
			userEditPane.setOnAdd((addEvent) -> {
				User user = userEditPane.getUser();
				try{
					UserResponse response = userService.update(user.getName(), user.getLastName(), userEditPane.getPassword(), 
							userEditPane.getOldPassword(), user.getEmail(), user.getWineryName(), user.getWineryAddress(), user.getStatus(), 
							userEditPane.getImgPath(), clientContext.getToken());
					switch (response.getStatus()){
					case Status.FORBIDDEN:
						GuiUtils.errorAlert("Error", "Incorrect password.").showAndWait(); return;
					case Status.BAD_EMAIL:
						GuiUtils.errorAlert("Error", "Invalid e-mail address.").showAndWait(); return;
					case Status.BAD_PASSWORD:
						GuiUtils.errorAlert("Error", "Invalid password. Passwords must be at least 8 and at most 24 characters long")
						.showAndWait(); return;
					case Status.EMAIL_TAKEN:
						GuiUtils.errorAlert("Error", "Provided e-mail address is already in use.").showAndWait(); return;
					}
				} catch (Exception e){
					e.printStackTrace();
					GuiUtils.exceptionAlert("Error", "An error occurred while adding wine", 
							"", e);
					return;
				}
				updateProfileStage.hide();
			});
			Scene updateProfileScene = new Scene(userEditPane, UPDATE_WIDTH, UPDATE_HEIGHT);
			updateProfileStage.setScene(updateProfileScene);
			updateProfileStage.setTitle("Update your info");
			updateProfileStage.show();
		});
		
		userGetMyGroupsButton.setOnAction((event) -> {
			infoLabel.setText("My groups");
			showGroupLister();
			groupLister.setItems(groupService.getGroups(clientContext.getUsername(), clientContext.getToken()).getData(),
					null);
		});
		
		userGetMyWinesButton.setOnAction((event) -> {
			infoLabel.setText("My wines");
			showWineLister();
			System.out.println(wineService.getWines(clientContext.getUsername(), clientContext.getToken()).getData());
			wineLister.setItems(wineService.getWines(clientContext.getUsername(), clientContext.getToken()).getData(),
					null);
		});
		
		groupLeaveButton.setOnAction((event) -> {
			Set<Group> selectedGroups = groupLister.getSelectedItems();
			if (selectedGroups.size() == 0){
				GuiUtils.invalidSelectionAlert().showAndWait();
				return;
			}
			
			for (Group group: selectedGroups){
				groupService.leaveGroup(group.getId(), clientContext.getToken());
			}
		});
		
		userGetOtherGroupsButton.setOnAction((event) -> {
			Set<User> selected = userLister.getSelectedItems();
			if (selected.isEmpty()){
				GuiUtils.invalidSelectionAlert().showAndWait();
				return;
			}
			
			List<Group> groups = new ArrayList<>();
			String usernames = "";
			for (User user: selected){
				groups.addAll(groupService.getGroups(user.getUsername(), clientContext.getToken()).getData());
				usernames += user.getUsername()+ "'s, ";
			}
			usernames = usernames.substring(0, usernames.lastIndexOf(", ")) + " ";
			
			userLister.clearSelected();
			infoLabel.setText(usernames + "groups");
			showGroupLister();
			groupLister.setItems(groups, null);
		});
		
		userGetOtherWinesButton.setOnAction((event) -> {
			Set<User> selected = userLister.getSelectedItems();
			if (selected.isEmpty()){
				GuiUtils.invalidSelectionAlert().showAndWait();
				return;
			}
			
			List<Wine> wines = new ArrayList<>();
			String usernames = "";
			for (User user: selected){
				wines.addAll(wineService.getWines(user.getUsername(), clientContext.getToken()).getData());
				usernames += user.getUsername()+ "'s, ";
			}
			usernames = usernames.substring(0, usernames.lastIndexOf(", ")) + " ";
			
			userLister.clearSelected();
			infoLabel.setText(usernames + "wines");
			showWineLister();
			wineLister.setItems(wines, null);
		});
		
		userInspectButton.setOnAction((event) -> {
			Set<User> selected = userLister.getSelectedItems();
			if (selected.isEmpty()){
				GuiUtils.invalidSelectionAlert().showAndWait();
				return;
			}
			
			for (User user: selected){
				Inspector inspector = new Inspector();
				inspector.addSectionTitle(user.getUsername());
				inspector.addSeparator();
				inspector.addSectionTitle("Personal information");
				inspector.addEntry("First name: ", user.getName());
				inspector.addEntry("Last name: ", user.getLastName());
				inspector.addEntry("Status: ", user.getStatus());
				inspector.addSeparator();
				inspector.addSectionTitle("Business information");
				inspector.addEntry("Winery name: ", user.getWineryName());
				inspector.addEntry("Winery address: ", user.getWineryAddress());
				inspector.setImage(userService.getUserImage(user.getUsername()).getData());
				
				Stage inspectorStage = new Stage();
				Scene InspectorScene = new Scene(inspector, INSPECTOR_WIDTH, INSPECTOR_HEIGHT);
				inspectorStage.setScene(InspectorScene);
				inspectorStage.setTitle(user.getUsername());
				inspectorStage.show();
			}
		});
		
		userBlockButton.setOnAction((event) -> {
			Set<User> selected = userLister.getSelectedItems();
			selected.removeAll(myBlockedUsers());
			if (selected.isEmpty()){
				GuiUtils.invalidSelectionAlert().showAndWait();
				return;
			}
			
			for (User user: selected){
				GeneralResponse response = userService.blockUser(clientContext.getToken(), user.getUsername());
				System.out.println(response.getStatus());
			}
		});
		
		userUnblockButton.setOnAction((event) -> {
			Set<User> selected = userLister.getSelectedItems();
			selected.retainAll(myBlockedUsers());
			if (selected.isEmpty()){
				GuiUtils.invalidSelectionAlert().showAndWait();
				return;
			}
			
			for (User user: selected){
				userService.unblockUser(clientContext.getToken(), user.getUsername());
			}
		});
		
		groupAddButton.setOnAction((event) -> {
			Stage addGroupStage = new Stage();
			AddGroupPane addGroupPane = new AddGroupPane(userLister.getSelectedItems());
			addGroupPane.setOnAdd((addEvent) -> {
				try{
					RequestResponse response = groupService.createGroupCreateRequest(addGroupPane.getMembers(), 
							addGroupPane.getName(),clientContext.getToken());
					
					switch (response.getStatus()){
					case Status.BAD_GROUP_DATA:
						GuiUtils.errorAlert("Error", "Group name must not be blank.")
						.showAndWait(); return;
					case Status.NOT_FOUND:
						GuiUtils.errorAlert("Error", "One or more of the users are no longer online.")
						.showAndWait(); return;
					case Status.BAD_REQUEST:
						GuiUtils.errorAlert("Error", "One or more of the invited users have blocked one or more"
								+ " of the other users.")
						.showAndWait(); return;
					}
					
					clientContext.addRequest(response.getData().getEvent().getEventId(), response.getData());
				} catch (Exception e){
					GuiUtils.exceptionAlert("Error", "An error occurred while creating the group create request", 
							"", e).showAndWait();
				}
				addGroupStage.hide();
			});
			Scene addGroupScene = new Scene(addGroupPane, LOGIN_WIDTH, LOGIN_HEIGHT);
			addGroupStage.setScene(addGroupScene);
			addGroupStage.setTitle("Create a group");
			addGroupStage.showAndWait();
		});
		
		userMyCommsButton.setOnAction((event) -> {
			infoLabel.setText("My communications");
			List<Communication> communications = commService.getComms(clientContext.getToken()).getData();
			communications.sort((comm1, comm2) -> -comm1.getTimeCreated().compareTo(comm2.getTimeCreated()));
			showCommLister();
			commLister.setItems(communications, null);
		});
		
		commRequestButton.setOnAction((event) -> {
			if (mainPane.getLister() == userLister){
				Set<User> selectedUsers = userLister.getSelectedItems();
				List<String> usernames = new ArrayList<>();
				for (User user: selectedUsers){
					usernames.add(user.getUsername());
				}
				
				usernames.remove(clientContext.getUsername());
				if (usernames.size() != 1){
					GuiUtils.invalidSelectionAlert().showAndWait();
					return;
				}
				
				commService.createCommCreateRequest(usernames.get(0), clientContext.getToken());
			};
			
			if (mainPane.getLister() == groupLister){
				Set<Group> selectedGroups = groupLister.getSelectedItems();
				if (selectedGroups.size() != 1){
					GuiUtils.invalidSelectionAlert().showAndWait();
					return;
				}
				
				Iterator<Group> iter = selectedGroups.iterator();
				Group group = iter.next();
				
				commService.createCommCreateRequest(group.getId(), clientContext.getToken());
			};
		});
		
		commGetMessagesButton.setOnAction((event) -> {
			if (mainPane.getLister() != commLister){
				GuiUtils.invalidSelectionAlert().showAndWait();
				return;
			}
			
			for (Communication comm: commLister.getSelectedItems()){
				showNewMessenger(comm.getId());
			}
		});
		
		primaryStage = new Stage();
		primaryStage.setScene(new Scene(mainPane, MY_WIDTH, MY_HEIGHT));
		primaryStage.setTitle("Happy Winers - " + clientContext.getUsername());
		primaryStage.setOnCloseRequest((event) -> {
			if (clientContext != null){
				if (clientContext.getToken() != null){
					sessionService.logOut(clientContext.getToken());
				}
			}
		});
		primaryStage.show();
	}
	
	private void showCommLister() {
		clearListers();
		userGetOtherWinesButton.setDisable(true);
		userBlockButton.setDisable(true);
		userUnblockButton.setDisable(true);
		groupAddButton.setDisable(true);
		groupLeaveButton.setDisable(true);
		groupLeaveButton.setDisable(true);
		wineRemoveButton.setDisable(true);
		userInspectButton.setDisable(true);
		mainPane.setLister(commLister);
	}

	private void showGroupLister() {
		clearListers();
		userGetOtherWinesButton.setDisable(true);
		userBlockButton.setDisable(true);
		userUnblockButton.setDisable(true);
		groupAddButton.setDisable(true);
		groupLeaveButton.setDisable(false);
		wineRemoveButton.setDisable(true);
		commGetMessagesButton.setDisable(true);
		userInspectButton.setDisable(true);
		mainPane.setLister(groupLister);
	}

	private void showWineLister(){
		clearListers();
		userGetOtherWinesButton.setDisable(true);
		userBlockButton.setDisable(true);
		userUnblockButton.setDisable(true);
		groupAddButton.setDisable(true);
		wineRemoveButton.setDisable(false);
		commGetMessagesButton.setDisable(true);
		groupLeaveButton.setDisable(true);
		userInspectButton.setDisable(true);
		mainPane.setLister(wineLister);
		mainPane.setFilters(wineFilter);
	}
	
	private void showUserLister(){
		clearListers();
		userGetOtherWinesButton.setDisable(false);
		userBlockButton.setDisable(false);
		userUnblockButton.setDisable(false);
		groupAddButton.setDisable(false);
		groupLeaveButton.setDisable(true);
		wineRemoveButton.setDisable(true);
		commGetMessagesButton.setDisable(true);
		userInspectButton.setDisable(false);
		mainPane.setLister(userLister);
		mainPane.setFilters(userFilter);
	}

	private synchronized boolean isStopPing() {
		return stopPing;
	}

	private synchronized void setStopPing(boolean stopPing) {
		this.stopPing = stopPing;
	}
	
	private List<User> myBlockedUsers(){
		return userService.getBlockedUsers(clientContext.getToken()).getData();
	}
	
	private void clearListers(){
		groupLister.clearSelected();
		userLister.clearSelected();
		commLister.clearSelected();
		wineLister.clearSelected();
	}
	
}
