package hr.fer.zemris.opp.the7ups.malivinari.gui;

import java.text.DateFormat;
import java.util.Set;

import hr.fer.zemris.opp.the7ups.malivinari.client.ClientContext;
import hr.fer.zemris.opp.the7ups.malivinari.models.Communication;
import hr.fer.zemris.opp.the7ups.malivinari.models.CommunicationType;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.responses.Status;
import hr.fer.zemris.opp.the7ups.malivinari.responses.UserListResponse;
import hr.fer.zemris.opp.the7ups.malivinari.services.CommService;
import javafx.scene.image.Image;

public class CommLister extends Lister<Communication> {

	private CommService commService;
	
	@Override
	public String topText(Communication item) {
		CommunicationType type = item.getType();
		if (type.equals(CommunicationType.GROUP)){
			return item.getGroupName();
		}
		
		UserListResponse response = commService.getCommParticipants(item.getId(), getClientContext().getToken());
		if (response.getStatus() == Status.OK){
			if (response.getData().size() == 1){
				return response.getData().get(0).getUsername() + ", (Deleted)";
			}
			return response.getData().get(0).getUsername() + ", " + response.getData().get(1).getUsername();
		}
		
		return "";
	}

	@Override
	public String bottomText(Communication item) {
		String started = "Started: " + DateFormat.getDateTimeInstance().format(item.getTimeCreated());
		String ended = "";
		if (item.getTimeEnded() != null){
			ended += "Ended: " + DateFormat.getDateTimeInstance().format(item.getTimeEnded());
		}
		return started + "\n" + ended;
	}

	@Override
	public Image image(Communication item) {
		return new Image(getClass().getClassLoader().getResourceAsStream("images/email.png"));
	}

	public CommService getCommService() {
		return commService;
	}

	public void setCommService(CommService commService) {
		this.commService = commService;
	}

}
