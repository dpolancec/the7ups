package hr.fer.zemris.opp.the7ups.malivinari.models;

public class EventTypes {

	public static final String GROUP_REQUEST = "groupRequest";
	public static final String COMM_REQUEST = "commRequest";
	public static final String GROUP_DISBAND = "groupDisband";
	public static final String COMM_DISBAND = "commDisband";
	public static final String GROUP_DISBAND_REQUEST = "groupDisbandRequest";
	public static final String SERVER_SHUTDOWN_REQUEST = "serverShutdownRequest";
	
}
