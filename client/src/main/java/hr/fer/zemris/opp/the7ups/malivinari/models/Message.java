package hr.fer.zemris.opp.the7ups.malivinari.models;

public class Message extends Model{

	private String body;
	private Communication communication;
	
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public Communication getCommunication() {
		return communication;
	}
	public void setCommunication(Communication communication) {
		this.communication = communication;
	}
	
	
}
