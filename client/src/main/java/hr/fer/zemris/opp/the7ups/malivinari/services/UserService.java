package hr.fer.zemris.opp.the7ups.malivinari.services;

import java.io.ByteArrayInputStream;
import java.util.Base64;

import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import hr.fer.zemris.opp.the7ups.malivinari.ServerInfo;
import hr.fer.zemris.opp.the7ups.malivinari.responses.GeneralResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.InputStreamResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.TokenResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.UserListResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.UserResponse;

@Service
public class UserService {
	
	public UserService() {}
	
	public UserResponse registerUser(String name, String lastName, String username, String password, String email){
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		if (name != null){
			params.add("name", name);
		}
		
		if (lastName != null){
			params.add("lastName", lastName);
		}
		
		params.add("username", username);
		params.add("password", password);
		params.add("email", email);
		
		RestTemplate template = new RestTemplate();
		UserResponse response = template.postForObject(ServerInfo.getUrl("register"), params, UserResponse.class);
		
		return response;
	}
	
	public UserResponse getUser(String username){
		RestTemplate template = new RestTemplate();
		UserResponse response = template.getForObject(ServerInfo.getUrl("user-username/{username}"), 
				UserResponse.class, username);
		
		return response;
	}
	
	public UserResponse getUser(String username, String token){
		RestTemplate template = new RestTemplate();
		UserResponse response = template.getForObject(ServerInfo.getUrl("user-username/{username}?token={token}"), 
				UserResponse.class, username, token);
		
		return response;
	}
	
	public UserResponse getUser(int id){
		RestTemplate template = new RestTemplate();
		UserResponse response = template.getForObject(ServerInfo.getUrl("user-id/{id}"),
				UserResponse.class, Integer.toString(id));
		
		return response;
	}
	
	public UserResponse getUserByEmail(String email){
		RestTemplate template = new RestTemplate();
		UserResponse response = template.getForObject(ServerInfo.getUrl("user-email/{email}"), 
				UserResponse.class, email);
		
		return response;
	}
	
	public UserResponse update(String name, String lastName, String password, String oldPassword, String email,
			String wineryName, String wineryAddress, String status, String imageFilePath, String token){
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
		if (name != null){
			params.add("name", name);
		}
		if (lastName != null){
			params.add("lastName", lastName);
		}
		if (password != null && !password.isEmpty()){
			params.add("password", password);
		}
		if (oldPassword != null && !oldPassword.isEmpty()){
			params.add("oldPassword", oldPassword);
		}
		if (email != null){
			params.add("email", email);
		}
		if (wineryName != null){
			params.add("wineryName", wineryName);
		}
		if (wineryAddress != null){
			params.add("wineryAddress", wineryAddress);
		}
		if (status != null){
			params.add("status", status);
		}
		if (imageFilePath != null && !imageFilePath.isEmpty()){
			params.add("image", new FileSystemResource(imageFilePath));
		}
		if (token != null){
			params.add("token", token);
		}
		
		RestTemplate template = new RestTemplate();
		UserResponse response;
		if (imageFilePath != null && !imageFilePath.isEmpty()){
			response = template.postForObject(ServerInfo.getUrl("update"), params, UserResponse.class);
		} else {
			response = template.postForObject(ServerInfo.getUrl("update-no-img"), params, UserResponse.class);
		}
		
		return response;
	}
	
	public UserListResponse getBlockedUsers(String token){
		RestTemplate template = new RestTemplate();
		UserListResponse response = template.getForObject(ServerInfo.getUrl("users-blocked/?token={token}"), 
				UserListResponse.class, token);
		return response;
	}
	
	public UserListResponse getAllUsers(){
		RestTemplate template = new RestTemplate();
		UserListResponse response = template.getForObject(ServerInfo.getUrl("users/"), 
				UserListResponse.class);
		return response;
	}
	
	public GeneralResponse blockUser(String token, String username){
		RestTemplate template = new RestTemplate();
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		
		params.add("user", username);
		params.add("token", token);
		GeneralResponse response = template.postForObject(ServerInfo.getUrl("block"), params,
				GeneralResponse.class);
		return response;
	}
	
	public GeneralResponse unblockUser(String token, String username){
		RestTemplate template = new RestTemplate();
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		
		params.add("user", username);
		params.add("token", token);
		GeneralResponse response = template.postForObject(ServerInfo.getUrl("unblock"), params,
				GeneralResponse.class);
		return response;
	}
	
	public InputStreamResponse getUserImage(String username){
		RestTemplate template = new RestTemplate();
		// token response abuse
		TokenResponse response = template.getForObject(ServerInfo.getUrl("user-img/{username}"),
				TokenResponse.class, username);
		
		InputStreamResponse imgResponse = new InputStreamResponse();
		imgResponse.setStatus(response.getStatus());
		if (response.getData() != null){
			imgResponse.setData(new ByteArrayInputStream(Base64.getDecoder().decode(response.getData())));
		}
		

		return imgResponse;
	}

}
