package hr.fer.zemris.opp.the7ups.malivinari.models;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Communication extends Model{
	
	public static final String Q_ALL = "Communication.findAll";

	private Set<Message> messages = new HashSet<Message>();
	private Date timeEnded;
	private CommunicationType type;
	private String groupName;
	private Set<User> participants = new HashSet<User>();
	
	public Set<Message> getMessages() {
		return messages;
	}
	
	public void setMessages(Set<Message> messages) {
		this.messages = messages;
	}
	
	public Date getTimeEnded() {
		return timeEnded;
	}
	
	public void setTimeEnded(Date timeEnded) {
		this.timeEnded = timeEnded;
	}
	
	public CommunicationType getType() {
		return type;
	}
	
	public void setType(CommunicationType type) {
		this.type = type;
	}

	public Set<User> getParticipants() {
		return participants;
	}

	public void setParticipants(Set<User> participants) {
		this.participants = participants;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
}
