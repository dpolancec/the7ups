package hr.fer.zemris.opp.the7ups.malivinari.models;

import java.util.HashSet;
import java.util.Set;

public class Group extends NamedModel{

	private Set<User> members = new HashSet<User>();

	public Set<User> getMembers() {
		return members;
	}
	public void setMembers(Set<User> members) {
		this.members = members;
	}
	
}
