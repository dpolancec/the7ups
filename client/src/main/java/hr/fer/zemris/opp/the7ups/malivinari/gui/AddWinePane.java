package hr.fer.zemris.opp.the7ups.malivinari.gui;

import java.io.File;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class AddWinePane extends ContentBackgroundPane {

	private static final int INPUT_PANE_WIDTH = 400;
	private static final int INPUT_PANE_HEIGHT = 400;

	private Label wineNameLabel = new SkinnableLabel("Enter wine name:");
	private Label filePathLabel = new SkinnableLabel();
	
	private Button uploadImageButton = new Button("Choose Image");
	
	private TextField wineNameTextField;
	
	private Button addButton;
	
	private Alert wrongData = new Alert(AlertType.ERROR);
	
	private static final int INPUT_LENGTH = 160;
	private static final int INPUT_HEIGHT = 30;
	
	public AddWinePane (){
		super();
		
		wrongData.setTitle("Error");
		wrongData.setHeaderText("Wrong wine data!");
		
		wineNameTextField = new TextField();
		wineNameTextField.setPrefWidth(INPUT_LENGTH);
		wineNameTextField.setPrefHeight(INPUT_HEIGHT);
		
		Image confirmImage = new Image(getClass().getResourceAsStream("/images/confirm.png"));
		addButton = new Button();
		addButton.setGraphic(new ImageView(confirmImage));
		addButton.setText("Confirm");
		addButton.setTooltip(new Tooltip("Add new wine"));
		
		uploadImageButton.setTooltip(new Tooltip("Choose an image for your wine"));
		uploadImageButton.setOnAction((uploadEvent) -> {
			File file = GuiUtils.imageFileChooser().showOpenDialog(this.getScene().getWindow());
			if (!file.exists() || !file.isFile()){
				GuiUtils.invalidFileAlert().showAndWait();
				return;
			}
			
			filePathLabel.setText(file.getAbsolutePath());
		});
		
		BorderPane wineNamePane = new BorderPane();
		wineNamePane.setLeft(wineNameLabel);
		wineNamePane.setRight(wineNameTextField);
		BorderPane.setMargin(wineNameLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(wineNameTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		GridPane inputPane = new GridPane();
		inputPane.setVgap(GuiConstants.DEFAULT_V_GAP);
		inputPane.setHgap(GuiConstants.DEFAULT_H_GAP);
		
		inputPane.add(wineNamePane, 0, 0);
		inputPane.add(uploadImageButton, 0, 1);
		inputPane.add(filePathLabel, 0, 2);
		filePathLabel.setWrapText(true);
		inputPane.setAlignment(Pos.CENTER);
		
		BorderPane mainPane = new BorderPane();
		
		mainPane.setCenter(inputPane);
		mainPane.setBottom(addButton);
		mainPane.setPrefWidth(INPUT_PANE_WIDTH);
		mainPane.setMaxWidth(INPUT_PANE_WIDTH);
		mainPane.setMaxHeight(INPUT_PANE_HEIGHT);
		mainPane.setOpacity(1.0);
		
		BorderPane.setAlignment(addButton, Pos.CENTER);
		BorderPane.setMargin(addButton, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				3*GuiConstants.DEFAULT_V_GAP, 0));
		
		this.addContent(mainPane);
	}
	
	public void showWrongDataAlert(){
		wrongData.showAndWait();
	}
	
	public void setOnAdd(EventHandler<ActionEvent> handler){
		addButton.setOnAction((event) -> {
			if (getWineName().isEmpty()){
				blankFieldAlert("Wine name").showAndWait();
				return;
			}
			
			handler.handle(event);
			clearFields();
		});
	}
	
	public String getWineName(){
		return wineNameTextField.getText().trim();
	}
	
	private Alert blankFieldAlert(String fieldname){
		Alert blankField = new Alert(AlertType.ERROR);
		blankField.setTitle("Error");
		blankField.setHeaderText(fieldname  + " must not be blank.");
		
		return blankField;
	}
	
	public void clearFields(){
		wineNameTextField.setText("");
	}
	
	public String getImgPath(){
		return filePathLabel.getText();
	}
}
