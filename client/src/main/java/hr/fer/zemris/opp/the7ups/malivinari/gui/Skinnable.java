package hr.fer.zemris.opp.the7ups.malivinari.gui;

public interface Skinnable {
	
	void notifyProviderChanged(SkinProvider newProvider);
	
}
