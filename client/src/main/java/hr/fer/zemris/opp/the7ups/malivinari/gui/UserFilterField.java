package hr.fer.zemris.opp.the7ups.malivinari.gui;

import javafx.scene.control.TextField;

public class UserFilterField extends TextField {

	public UserFilterField(UserLister lister){
		super("");
		this.textProperty().addListener((observable, oldValue, newValue) -> {
			
			lister.setItems(lister.getUserService().getAllUsers().getData(), (user) -> {
				if (newValue.isEmpty()){
					return true;
				}
				
				
				return user.getUsername().startsWith(newValue) 
						|| user.getName() != null && user.getName().startsWith(newValue) 
						|| user.getLastName() != null && user.getLastName().startsWith(newValue);
			});
		});
		
		this.setEditable(true);
		this.setMaxWidth(120);
	}
}
