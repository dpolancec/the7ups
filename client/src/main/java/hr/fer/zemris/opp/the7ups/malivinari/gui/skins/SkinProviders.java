package hr.fer.zemris.opp.the7ups.malivinari.gui.skins;

import hr.fer.zemris.opp.the7ups.malivinari.gui.SkinProvider;

public class SkinProviders {

	public static final SkinProvider DEFAULT = new DefaultSkinProvider();
	public static final SkinProvider WHITE_BLACK = new WhiteBlackSkinProvider();
	
	private SkinProviders(){};
}
