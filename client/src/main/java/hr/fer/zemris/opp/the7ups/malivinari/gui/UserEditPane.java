package hr.fer.zemris.opp.the7ups.malivinari.gui;

import java.io.File;

import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class UserEditPane extends ContentBackgroundPane {

	private static final int INPUT_PANE_WIDTH = 400;
	private static final int INPUT_PANE_HEIGHT = 400;

	private Label nameLabel = new SkinnableLabel("Enter first name:");
	private Label lastNameLabel = new SkinnableLabel("Enter last name:");
	private Label emailLabel = new SkinnableLabel("Enter email:");
	private Label wineryNameLabel = new SkinnableLabel("Enter winery name:");
	private Label wineryAddressLabel = new SkinnableLabel("Enter winery address:");
	private Label passwordLabel = new SkinnableLabel("Enter new password:");
	private Label repeatPasswordLabel = new SkinnableLabel("Repeat new password:");
	private Label oldPasswordLabel = new SkinnableLabel("Enter old password:");
	private Label statusLabel = new SkinnableLabel("Enter status: ");
	private Label filePathLabel = new SkinnableLabel();
	
	private Button uploadImageButton = new Button("Choose Image");
	
	private TextField nameTextField;
	private TextField lastNameTextField;
	private TextField emailTextField;
	private TextField wineryNameTextField;
	private TextField wineryAddressTextField;
	private TextField passwordTextField;
	private TextField repeatPasswordTextField;
	private TextField oldPasswordTextField;
	private TextField statusTextField;
	
	private Button addButton;
	
	private Alert wrongData = new Alert(AlertType.ERROR);
	
	private static final int INPUT_LENGTH = 160;
	private static final int INPUT_HEIGHT = 30;
	
	public UserEditPane (User user){
		super();
		
		wrongData.setTitle("Error");
		wrongData.setHeaderText("Wrong wine data!");
		
		nameTextField = new TextField(user.getName() == null ? "" : user.getName());
		nameTextField.setPrefWidth(INPUT_LENGTH);
		nameTextField.setPrefHeight(INPUT_HEIGHT);
		
		lastNameTextField = new TextField(user.getLastName() == null ? "" : user.getLastName());
		lastNameTextField.setPrefWidth(INPUT_LENGTH);
		lastNameTextField.setPrefHeight(INPUT_HEIGHT);
		
		emailTextField = new TextField(user.getEmail() == null ? "" : user.getEmail());
		emailTextField.setPrefWidth(INPUT_LENGTH);
		emailTextField.setPrefHeight(INPUT_HEIGHT);
		
		wineryNameTextField = new TextField(user.getWineryName() == null ? "" : user.getWineryName());
		wineryNameTextField.setPrefWidth(INPUT_LENGTH);
		wineryNameTextField.setPrefHeight(INPUT_HEIGHT);
		
		wineryAddressTextField = new TextField(user.getWineryAddress() == null ? "" : user.getWineryAddress());
		wineryAddressTextField.setPrefWidth(INPUT_LENGTH);
		wineryAddressTextField.setPrefHeight(INPUT_HEIGHT);
		
		statusTextField = new TextField(user.getStatus() == null ? "" : user.getStatus());
		statusTextField.setPrefWidth(INPUT_LENGTH);
		statusTextField.setPrefHeight(INPUT_HEIGHT);
		
		passwordTextField = new PasswordField();
		passwordTextField.setPrefWidth(INPUT_LENGTH);
		passwordTextField.setPrefHeight(INPUT_HEIGHT);
		
		repeatPasswordTextField = new PasswordField();
		repeatPasswordTextField.setPrefWidth(INPUT_LENGTH);
		repeatPasswordTextField.setPrefHeight(INPUT_HEIGHT);
		
		oldPasswordTextField = new PasswordField();
		oldPasswordTextField.setPrefWidth(INPUT_LENGTH);
		oldPasswordTextField.setPrefHeight(INPUT_HEIGHT);
		
		uploadImageButton.setTooltip(new Tooltip("Choose an image"));
		uploadImageButton.setOnAction((uploadEvent) -> {
			File file = GuiUtils.imageFileChooser().showOpenDialog(this.getScene().getWindow());
			if (!file.exists() || !file.isFile()){
				GuiUtils.invalidFileAlert().showAndWait();
				return;
			}
			
			filePathLabel.setText(file.getAbsolutePath());
		});
		
		Image confirmImage = new Image(getClass().getResourceAsStream("/images/confirm.png"));
		addButton = new Button();
		addButton.setGraphic(new ImageView(confirmImage));
		addButton.setText("Confirm");
		addButton.setTooltip(new Tooltip("Edit your info"));
		
		BorderPane namePane = new BorderPane();
		namePane.setLeft(nameLabel);
		namePane.setRight(nameTextField);
		BorderPane.setMargin(nameLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(nameTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		BorderPane lastNamePane = new BorderPane();
		lastNamePane.setLeft(lastNameLabel);
		lastNamePane.setRight(lastNameTextField);
		BorderPane.setMargin(lastNameLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(lastNameTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		BorderPane emailPane = new BorderPane();
		emailPane.setLeft(emailLabel);
		emailPane.setRight(emailTextField);
		BorderPane.setMargin(emailLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(emailTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		BorderPane wineryNamePane = new BorderPane();
		wineryNamePane.setLeft(wineryNameLabel);
		wineryNamePane.setRight(wineryNameTextField);
		BorderPane.setMargin(wineryNameLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(wineryNameTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		BorderPane wineryAddressPane = new BorderPane();
		wineryAddressPane.setLeft(wineryAddressLabel);
		wineryAddressPane.setRight(wineryAddressTextField);
		BorderPane.setMargin(wineryAddressLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(wineryAddressTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		BorderPane statusPane = new BorderPane();
		statusPane.setLeft(statusLabel);
		statusPane.setRight(statusTextField);
		BorderPane.setMargin(statusLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(statusTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		BorderPane passwordPane = new BorderPane();
		passwordPane.setLeft(passwordLabel);
		passwordPane.setRight(passwordTextField);
		BorderPane.setMargin(passwordLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(passwordTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		BorderPane repeatPasswordPane = new BorderPane();
		repeatPasswordPane.setLeft(repeatPasswordLabel);
		repeatPasswordPane.setRight(repeatPasswordTextField);
		BorderPane.setMargin(repeatPasswordLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(repeatPasswordTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		BorderPane oldPasswordPane = new BorderPane();
		oldPasswordPane.setLeft(oldPasswordLabel);
		oldPasswordPane.setRight(oldPasswordTextField);
		BorderPane.setMargin(oldPasswordLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(oldPasswordTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		GridPane inputPane = new GridPane();
		inputPane.setVgap(GuiConstants.DEFAULT_V_GAP);
		inputPane.setHgap(GuiConstants.DEFAULT_H_GAP);
		
		inputPane.add(namePane, 0, 0);
		inputPane.add(lastNamePane, 0, 1);
		inputPane.add(emailPane, 0, 2);
		inputPane.add(wineryNamePane, 0, 3);
		inputPane.add(wineryAddressPane, 0, 4);
		inputPane.add(statusPane, 0, 5);
		inputPane.add(passwordPane, 0, 6);
		inputPane.add(repeatPasswordPane, 0, 7);
		inputPane.add(oldPasswordPane, 0, 8);
		
		inputPane.add(uploadImageButton, 0, 9);
		inputPane.add(filePathLabel, 0, 10);
		filePathLabel.setWrapText(true);
		inputPane.setAlignment(Pos.CENTER);
		
		BorderPane mainPane = new BorderPane();
		
		mainPane.setCenter(inputPane);
		mainPane.setBottom(addButton);
		mainPane.setPrefWidth(INPUT_PANE_WIDTH);
		mainPane.setMaxWidth(INPUT_PANE_WIDTH);
		mainPane.setMaxHeight(INPUT_PANE_HEIGHT);
		mainPane.setOpacity(1.0);
		
		BorderPane.setAlignment(addButton, Pos.CENTER);
		BorderPane.setMargin(addButton, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				3*GuiConstants.DEFAULT_V_GAP, 0));
		
		this.addContent(mainPane);
	}
	
	public void showWrongDataAlert(){
		wrongData.showAndWait();
	}
	
	public void setOnAdd(EventHandler<ActionEvent> handler){
		addButton.setOnAction((event) -> {
			if (!getPassword().equals(repeatPasswordTextField.getText())){
				GuiUtils.errorAlert("Error", "Passwords do not match!").showAndWait();
				return;
			}
			
			handler.handle(event);
		});
	}
	
	public User getUser(){
		User user = new User();
		user.setName(nameTextField.getText());
		user.setLastName(lastNameTextField.getText());
		user.setEmail(emailTextField.getText());
		user.setWineryName(wineryNameTextField.getText());
		user.setWineryAddress(wineryAddressTextField.getText());
		user.setStatus(statusTextField.getText());
		
		return user;
	}
	
	public String getPassword(){
		return passwordTextField.getText();
	}
	
	public String getOldPassword(){
		return oldPasswordTextField.getText();
	}
	
	public String getImgPath(){
		return filePathLabel.getText();
	}
}
