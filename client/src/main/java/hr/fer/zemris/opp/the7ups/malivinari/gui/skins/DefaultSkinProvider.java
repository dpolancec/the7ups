package hr.fer.zemris.opp.the7ups.malivinari.gui.skins;

import hr.fer.zemris.opp.the7ups.malivinari.gui.SkinProvider;
import javafx.scene.image.Image;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;

public class DefaultSkinProvider implements SkinProvider {

	private static final int CORNER_RADIUS = 5;
	BackgroundImage background = new BackgroundImage(new Image("/images/regBackground.jpg"),
	        BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT,
	          new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, true));
	BackgroundFill fill = new BackgroundFill(Color.web("#7F1734"), new CornerRadii(CORNER_RADIUS), null);
	private Color fontColor = Color.WHITE;
	private double backdropOpacity = 0.8;
	
	@Override
	public BackgroundImage getBackgroundImage() {
		return background;
	}

	@Override
	public BackgroundFill getBackdrop() {
		return fill;
	}

	@Override
	public Color getFontColor() {
		return fontColor;
	}

	@Override
	public double getBackdropOpacity() {
		return backdropOpacity;
	}
	
	@Override
	public String toString() {
		return "Barrels";
	}

}
