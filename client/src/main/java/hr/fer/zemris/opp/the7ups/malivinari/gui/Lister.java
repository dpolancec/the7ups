package hr.fer.zemris.opp.the7ups.malivinari.gui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import hr.fer.zemris.opp.the7ups.malivinari.client.ClientContext;
import hr.fer.zemris.opp.the7ups.malivinari.models.Model;
import hr.fer.zemris.opp.the7ups.malivinari.models.NamedModel;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;

public abstract class Lister<T> extends StackPane{

	public static int LISTER_WIDTH = 1280;
	
	private List<T> originalItems;
	private List<T> displayItems = new ArrayList<>();
	private FlowPane listPane = new FlowPane();
	private Set<T> selectedItems = new LinkedHashSet<>();
	private ClientContext clientContext;
	
	public Lister() {
		super();
	}
	
	public void setItems(List<T> items, Predicate<T> filter){
		
		listPane.setHgap(GuiConstants.DEFAULT_H_GAP);
		listPane.setVgap(GuiConstants.DEFAULT_V_GAP);
		
		selectedItems.clear();
		displayItems.clear();
		displayItems.addAll(items);
		if (filter == null){
			originalItems = new ArrayList<>(items);
		}
		applySilentFilter(filter);
		this.setMaxWidth(LISTER_WIDTH);
		listPane.getChildren().clear();
		if (originalItems.isEmpty()){
			return;
		}
		T object = originalItems.get(0);
		if (object instanceof Model){
			if (object instanceof NamedModel){
				if (object instanceof User){
					displayItems.sort((first, second) -> {
						User userFirst = (User) first;
						User userSecond = (User) second;
						
						return userFirst.getUsername().compareTo(userSecond.getUsername());
					});
				} else {
					displayItems.sort((first, second) -> {
						NamedModel namedFirst = (NamedModel) first;
						NamedModel namedSecond = (NamedModel) second;
						
						return namedFirst.getName().compareTo(namedSecond.getName());
					});
				}
			} else {
				displayItems.sort((first, second) -> {
					Model modelFirst = (Model) first;
					Model modelSecond = (Model) second;
					
					return -modelFirst.getTimeCreated().compareTo(modelSecond.getTimeCreated());
				});
			}
		}
		
		for (T item: displayItems){
			ListerItem<T> listerItem = new ListerItem<>(item, topText(item), bottomText(item), image(item));
			listerItem.setOnImageClick((event) -> {
				if (listerItem.isSelected()){
					selectedItems.add(item);
				} else {
					selectedItems.remove(item);
				}
			});
			listPane.getChildren().add(listerItem);
		}
		this.setMaxWidth(LISTER_WIDTH);
		if (!this.getChildren().contains(listPane)){
			this.getChildren().add(listPane);
		}
	}

	private void applySilentFilter(Predicate<T> filter) {
		if (filter == null){
			return;
		}
		
		System.out.println("Apply");
		System.out.println(displayItems.size());
		Iterator<T> iter = displayItems.iterator();
		while(iter.hasNext()){
			T next = iter.next();
			if (!filter.test(next)){
				iter.remove();
				System.out.println("removed");
			}
		}
	}
	
	public void reset(){
		displayItems.addAll(originalItems);
		selectedItems.clear();
	}
	
	public synchronized Set<T> getSelectedItems() {
		return selectedItems;
	}
	
	public synchronized void clearSelected(){
		selectedItems.clear();
	}

	public abstract String topText(T item);
	public abstract String bottomText(T item);
	public abstract Image image(T item);

	public ClientContext getClientContext() {
		return clientContext;
	}

	public void setClientContext(ClientContext clientContext) {
		this.clientContext = clientContext;
	}

	public synchronized List<T> getDisplayItems() {
		return displayItems;
	}

	public synchronized List<T> getOriginalItems() {
		return originalItems;
	}
	
	

}
