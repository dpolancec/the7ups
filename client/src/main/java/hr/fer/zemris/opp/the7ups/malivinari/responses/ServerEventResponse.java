package hr.fer.zemris.opp.the7ups.malivinari.responses;

import java.util.Date;
import java.util.List;

public class ServerEventResponse {
	private Date timeCreated;
	private String eventId;
	private List<String> relevantUsers;
	private String type;
	private boolean isRequest;
	private String initiatorUsername;
	private String additionalInfo;
	private String finalizedRequestInitiator;
	private int status;
	
	public Date getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public List<String> getRelevantUsers() {
		return relevantUsers;
	}
	public void setRelevantUsers(List<String> relevantUsers) {
		this.relevantUsers = relevantUsers;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public boolean isRequest() {
		return isRequest;
	}
	public void setRequest(boolean isRequest) {
		this.isRequest = isRequest;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getInitiatorUsername() {
		return initiatorUsername;
	}
	public void setInitiatorUsername(String initiatorUsername) {
		this.initiatorUsername = initiatorUsername;
	}
	@Override
	public String toString() {
		return "ServerEventResponse [timeCreated=" + timeCreated + ", eventId=" + eventId + ", relevantUsers="
				+ relevantUsers + ", type=" + type + ", isRequest=" + isRequest + ", initiatorUsername="
				+ initiatorUsername + ", status=" + status + "]";
	}
	public String getAdditionalInfo() {
		return additionalInfo;
	}
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	public String getFinalizedRequestInitiator() {
		return finalizedRequestInitiator;
	}
	public void setFinalizedRequestInitiator(String finalizedRequestInitiator) {
		this.finalizedRequestInitiator = finalizedRequestInitiator;
	}
	
	

}
