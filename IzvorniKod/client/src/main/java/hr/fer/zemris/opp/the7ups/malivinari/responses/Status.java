package hr.fer.zemris.opp.the7ups.malivinari.responses;

public class Status {

	public static final int OK = 200;
	public static final int FORBIDDEN = 403;
	public static final int NOT_FOUND = 404;
	public static final int ERR = 500;
	public static final int BAD_USERNAME = 1000;
	public static final int BAD_PASSWORD = 1001;
	public static final int BAD_EMAIL = 1002;
	public static final int BAD_USER_DATA = 1003;
	public static final int USERNAME_TAKEN = 2000;
	public static final int EMAIL_TAKEN = 2001;
	public static final int BAD_WINE_DATA = 3000;
	public static final int BAD_GROUP_DATA = 4000;
	public static final int BAD_REQUEST = 10000;
	public static final int TOO_MANY_LOGGED_IN = 30000;
	public static final int NOT_CHANGED = 304;
	public static final int BUSY = 10001;
	
	private Status() {}
	
}
