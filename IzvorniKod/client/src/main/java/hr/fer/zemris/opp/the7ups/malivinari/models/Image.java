package hr.fer.zemris.opp.the7ups.malivinari.models;

import java.sql.Blob;
import java.sql.SQLException;

public class Image extends Model{

	private User owner;
	private Wine wine;
	private Blob data;
	
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public Blob getData() {
		return data;
	}
	public void setData(Blob data) {
		this.data = data;
	}
	
	public byte[] getImageBytes() throws SQLException{
		if (data == null){
			return null;
		}
		return data.getBytes(1, (int)data.length());
	}
	public Wine getWine() {
		return wine;
	}
	public void setWine(Wine wine) {
		this.wine = wine;
	}
	
}
