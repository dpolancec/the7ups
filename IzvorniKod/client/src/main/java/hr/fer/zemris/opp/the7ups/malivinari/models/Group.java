package hr.fer.zemris.opp.the7ups.malivinari.models;

import java.util.HashSet;
import java.util.Set;

public class Group extends NamedModel{

	private Set<User> members = new HashSet<User>();
	private Set<Communication> communications = new HashSet<Communication>();
	
	public Set<User> getMembers() {
		return members;
	}
	public void setMembers(Set<User> members) {
		this.members = members;
	}
	public Set<Communication> getCommunications() {
		return communications;
	}
	public void setCommunications(Set<Communication> communications) {
		this.communications = communications;
	}
	
}
