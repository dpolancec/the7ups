package hr.fer.zemris.opp.the7ups.malivinari.gui;

import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;

public class ContentBackgroundPane extends BackgroundPane {

	private StackPane contentPane = new StackPane();
	
	public ContentBackgroundPane(){
		contentPane.getChildren().add(new BackdropPane());
		this.getChildren().add(contentPane);
	}
	
	public ContentBackgroundPane(BorderPane bodyBorderPane) {
		// TODO Auto-generated constructor stub
	}

	public void addContent(Node...content){
		contentPane.getChildren().addAll(content);
		resizeContentPane();
	}

	private void resizeContentPane() {
		Node minMaxHeightNode = contentPane.getChildren().stream().min((first, second) -> {
			if (first instanceof Region && second instanceof Region){
				Region firstRegion = (Region)first;
				Region secondRegion = (Region)second;
				
				double firstRegionMaxHeight = firstRegion.getMaxHeight();
				double secondRegionMaxHeight = secondRegion.getMaxHeight();
				
				if (firstRegionMaxHeight < 0 && secondRegionMaxHeight >= 0){
					return 1;
				}
				
				if (secondRegionMaxHeight < 0 && firstRegionMaxHeight >= 0){
					return -1;
				}
				
				return Double.compare(firstRegion.getMaxHeight(), secondRegion.getMaxHeight());
			} else {
				return 1;
			}
		}).get();
		
		if (minMaxHeightNode instanceof Region){
			System.out.println(((Region) minMaxHeightNode).getMaxHeight());
			contentPane.setMaxHeight(((Region) minMaxHeightNode).getMaxHeight());
		}
		
		Node minMaxWidthNode = contentPane.getChildren().stream().min((first, second) -> {
			if (first instanceof Region && second instanceof Region){
				Region firstRegion = (Region)first;
				Region secondRegion = (Region)second;
				
				double firstRegionMaxWidth = firstRegion.getMaxWidth();
				double secondRegionMaxWidth = secondRegion.getMaxWidth();
				
				if (firstRegionMaxWidth < 0 && secondRegionMaxWidth >= 0){
					return 1;
				}
				
				if (secondRegionMaxWidth < 0 && firstRegionMaxWidth >= 0){
					return -1;
				}
				
				return Double.compare(firstRegionMaxWidth, secondRegionMaxWidth);
			} else {
				return 1;
			}
		}).get();
		
		if (minMaxWidthNode instanceof Region){
			contentPane.setMaxWidth(((Region) minMaxWidthNode).getMaxWidth());
		}
	}
	
}
