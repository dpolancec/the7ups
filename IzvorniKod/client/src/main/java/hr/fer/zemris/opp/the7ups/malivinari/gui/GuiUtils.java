package hr.fer.zemris.opp.the7ups.malivinari.gui;

import java.io.PrintWriter;
import java.io.StringWriter;

import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import javafx.stage.FileChooser.ExtensionFilter;

public class GuiUtils {
	
	public static Alert exceptionAlert(String title, String header, String content, Exception e){
		StringWriter writer = new StringWriter();
		e.printStackTrace(new PrintWriter(writer));
		
		TextArea textArea = new TextArea(writer.toString());
		textArea.setEditable(false);
		textArea.setWrapText(true);
		
		Label label = new Label("Show this info to the developer:");
		
		GridPane.setVgrow(textArea, Priority.ALWAYS); // RIP Alan Rickman
		GridPane.setHgrow(textArea, Priority.ALWAYS);

		GridPane expContent = new GridPane();
		expContent.setMaxWidth(Double.MAX_VALUE);
		expContent.add(label, 0, 0);
		expContent.add(textArea, 0, 1);
		
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.getDialogPane().setExpandableContent(expContent);
		
		return alert;
	}
	
	public static Alert confirmAlert(String title, String header, String content){
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle(title);
		alert.setContentText(header);
		return alert;
	}
	
	public static Alert noConnectionAlert(){
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setContentText("Could not connect to server.");
		return alert;
	}
	
	public static Alert invalidFileAlert(){
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setContentText("Invalid file.");
		return alert;
	}
	
	public static Alert invalidSelectionAlert(){
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setContentText("This action requires at least one selected item.");
		return alert;
	}
	
	public static Alert errorAlert(String title, String text){
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(title);
		alert.setContentText(text);
		return alert;
	}
	
	public static FileChooser imageFileChooser(){
		FileChooser chooser = new FileChooser();
		chooser.setTitle("Image selection");
		ExtensionFilter imageFilter = new ExtensionFilter("Image files", "*.jpg", "*.png", "*.bmp");
		chooser.getExtensionFilters().add(imageFilter);
		chooser.setSelectedExtensionFilter(imageFilter);
		
		return chooser;
	}

	public static Dialog<ButtonType> infoAlert(String title, String text) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(title);
		alert.setContentText(text);
		return alert;
	}
}
