package hr.fer.zemris.opp.the7ups.malivinari.models;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Communication extends Model{

	private Set<Message> messages = new HashSet<Message>();
	private Date timeEnded;
	private CommunicationType type;
	private Group group;

	private Set<User> participants = new HashSet<User>();
	
	public Set<Message> getMessages() {
		return messages;
	}
	
	public void setMessages(Set<Message> messages) {
		this.messages = messages;
	}
	
	public Date getTimeEnded() {
		return timeEnded;
	}
	
	public void setTimeEnded(Date timeEnded) {
		this.timeEnded = timeEnded;
	}
	
	public CommunicationType getType() {
		return type;
	}
	
	public void setType(CommunicationType type) {
		this.type = type;
	}
	
	public Group getGroup() {
		return group;
	}
	
	public void setGroup(Group group) {
		this.group = group;
	}

	public Set<User> getParticipants() {
		return participants;
	}

	public void setParticipants(Set<User> participants) {
		this.participants = participants;
	}
	
}
