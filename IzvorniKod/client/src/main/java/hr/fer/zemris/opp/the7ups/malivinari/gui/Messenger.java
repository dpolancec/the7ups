package hr.fer.zemris.opp.the7ups.malivinari.gui;

import hr.fer.zemris.opp.the7ups.malivinari.models.Communication;
import hr.fer.zemris.opp.the7ups.malivinari.models.Message;
import hr.fer.zemris.opp.the7ups.malivinari.services.CommService;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

public class Messenger extends ContentBackgroundPane {

	/*
	 * Main pane (Border Pane):
	 * 	Center: chat pane
	 * 		Center receive pane
	 * 		Bottom send pane
	 * 			Center send	field
	 * 			Right send button
	 * 	Bottom info label
	 */
	
	private static final int MESSENGER_WIDTH = 1024;
	private static final int MESSENGER_HEIGHT = 760;
	
	private TextArea recieveArea = new TextArea();
	
	private TextField sendField = new TextField();
	private Label infoLabel = new SkinnableLabel("While communicating with other users you can not receive or send out communicatin requests"
			+ " to other users. Closing this window will end this communication.");
	private BorderPane chatPane = new BorderPane(); // receive and send panes go here;
	private Button sendButton = new Button("Send");
	private BorderPane sendPane = new BorderPane();
	
	private BorderPane mainPane = new BorderPane();
	
	public Messenger () {
		recieveArea.setEditable(false);
		sendPane.setCenter(sendField);
		sendPane.setRight(sendButton);
		BorderPane.setMargin(sendField, new Insets(GuiConstants.DEFAULT_V_GAP, GuiConstants.DEFAULT_H_GAP, GuiConstants.DEFAULT_V_GAP,
				GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(sendButton, new Insets(GuiConstants.DEFAULT_V_GAP, GuiConstants.DEFAULT_H_GAP, GuiConstants.DEFAULT_V_GAP,
				GuiConstants.DEFAULT_H_GAP));
		chatPane.setCenter(recieveArea);
		chatPane.setBottom(sendPane);
		
		mainPane.setCenter(chatPane);
		mainPane.setBottom(infoLabel);
		
		mainPane.setMaxWidth(MESSENGER_WIDTH);
		mainPane.setMaxHeight(MESSENGER_HEIGHT);
		
		this.addContent(mainPane);
	}

	public void appendMessage(String message){
		recieveArea.setText(recieveArea.getText() +  message + "\n");
	}
	
	public void setOnSend(EventHandler<ActionEvent> handler){
		sendButton.setOnAction((event) -> {
			if (getSendText().isEmpty()){
				event.consume();
				return;
			}
			
			handler.handle(event);
		});
		
	}
	
	public String getSendText(){
		return sendField.getText();
	}
	
	public void hideInput(){
		chatPane.getChildren().remove(sendPane);
	}
}
