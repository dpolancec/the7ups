package hr.fer.zemris.opp.the7ups.malivinari.gui;

public class GuiConstants {

	private GuiConstants(){}
	
	public static final int DEFAULT_V_GAP = 5;
	public static final int DEFAULT_H_GAP = 5;
}
