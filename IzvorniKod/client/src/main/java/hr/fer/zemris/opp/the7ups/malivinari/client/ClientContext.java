package hr.fer.zemris.opp.the7ups.malivinari.client;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Service;

import hr.fer.zemris.opp.the7ups.malivinari.models.Request;

@Service
public class ClientContext {

	public static final int AUTO_CANCEL_REQUEST_MILLIS = 30000;
	
	private String token;
	private String username;
	private Map<String, Request> openRequests = new HashMap<>();
	
	public synchronized void setToken(String token){
		this.token = token;
	}
	
	public synchronized String getToken() {
		return token;
	}

	public synchronized void addRequest(String eventId, Request request){
		openRequests.put(eventId, request);
	}
	
	public synchronized void removeRequest(String eventId){
		openRequests.remove(eventId);
	}
	
	public synchronized Request getRequest(String eventId){
		return openRequests.get(eventId);
	}

	public synchronized String getUsername() {
		return username;
	}

	public synchronized void setUsername(String username) {
		this.username = username;
	}
	
	
	
}
