package hr.fer.zemris.opp.the7ups.malivinari.models;

public class NamedModel extends Model {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
