package hr.fer.zemris.opp.the7ups.malivinari.services;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.VolatileImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import hr.fer.zemris.opp.the7ups.malivinari.ServerInfo;
import hr.fer.zemris.opp.the7ups.malivinari.responses.InputStreamResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.TokenResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.WineListResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.WineResponse;
import javafx.scene.image.Image;

@Service
public class WineService {

	public WineListResponse getWines(String username, String token){
		RestTemplate template = new RestTemplate();
		WineListResponse response = template.getForObject(ServerInfo.getUrl("wines/{username}?token={token}"), 
				WineListResponse.class, username, token);
		return response;
	}
	
	public WineResponse addWine(String name, String token, String imageFilePath){
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		params.add("token", token);
		params.add("name", name);
		RestTemplate template = new RestTemplate();
		WineResponse response;
		if (imageFilePath != null && !imageFilePath.isEmpty()){
			params.add("image", new FileSystemResource(imageFilePath));
			response = template.postForObject(ServerInfo.getUrl("add-wine"), params,
					WineResponse.class);
		} else {
			response = template.postForObject(ServerInfo.getUrl("add-wine-no-img"), params,
					WineResponse.class);
		}
		
		return response;
	}
	
	public WineResponse getWine(int id, String token){
		RestTemplate template = new RestTemplate();
		WineResponse response = template.getForObject(ServerInfo.getUrl("wine/{id}?token={token}"),
				WineResponse.class, Integer.toString(id), token);
		
		return response;
	}
	
	public WineResponse updateWine(int id, String name, String token, String imageFilePath){
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		params.add("token", token);
		params.add("id", Integer.toString(id));
		if (name != null){
			params.add("name", name);
		}
		if (imageFilePath != null){
			params.add("image", new FileSystemResource(imageFilePath));
		}
		RestTemplate template = new RestTemplate();
		WineResponse response = template.postForObject(ServerInfo.getUrl("update_wine"), params,
				WineResponse.class);
		return response;
	}
	
	public InputStreamResponse getWineImage(int id){
		RestTemplate template = new RestTemplate();
		// token response abuse
		TokenResponse response = template.getForObject(ServerInfo.getUrl("wine/{id}/image"),
				TokenResponse.class, Integer.toString(id));
		
		InputStreamResponse imgResponse = new InputStreamResponse();
		imgResponse.setStatus(response.getStatus());
		if (response.getData() != null){
			imgResponse.setData(new ByteArrayInputStream(Base64.getDecoder().decode(response.getData())));
		}
		

		return imgResponse;
	}
	
}
