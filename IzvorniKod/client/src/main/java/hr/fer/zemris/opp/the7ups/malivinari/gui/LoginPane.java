package hr.fer.zemris.opp.the7ups.malivinari.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

public class LoginPane extends ContentBackgroundPane {
	
	private static final int INPUT_PANE_WIDTH = 400;
	private static final int INPUT_PANE_HEIGHT = 400;

	private Label usernameLabel = new Label("Enter username:");
	private Label passwordLabel = new Label("Enter password:");
	
	private TextField usernameTextField;
	private TextField passwordTextField;
	
	private Button loginButton;
	
	private Alert wrongData = new Alert(AlertType.ERROR);
	
	private static final int INPUT_LENGTH = 160;
	private static final int INPUT_HEIGHT = 30;
	
	public LoginPane (){
		super();
		
		wrongData.setTitle("Error");
		wrongData.setHeaderText("Wrong username or password!");
		
		usernameLabel.setTextFill(Color.WHITE);
		passwordLabel.setTextFill(Color.WHITE);
		
		usernameTextField = new TextField();
		usernameTextField.setPrefWidth(INPUT_LENGTH);
		usernameTextField.setPrefHeight(INPUT_HEIGHT);
		
		passwordTextField = new PasswordField();
		passwordTextField.setPrefWidth(INPUT_LENGTH);
		passwordTextField.setPrefHeight(INPUT_HEIGHT);
		
		Image confirmImage = new Image(getClass().getResourceAsStream("/images/confirm.png"));
		loginButton = new Button();
		loginButton.setGraphic(new ImageView(confirmImage));
		loginButton.setText("Login");
		loginButton.setTooltip(new Tooltip("Log in to your account"));
		
		BorderPane usernamePane = new BorderPane();
		usernamePane.setLeft(usernameLabel);
		usernamePane.setRight(usernameTextField);
		BorderPane.setMargin(usernameLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(usernameTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		BorderPane passwordPane = new BorderPane();
		passwordPane.setLeft(passwordLabel);
		passwordPane.setRight(passwordTextField);
		BorderPane.setMargin(passwordLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(passwordTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		GridPane inputPane = new GridPane();
		inputPane.setVgap(GuiConstants.DEFAULT_V_GAP);
		inputPane.setHgap(GuiConstants.DEFAULT_H_GAP);
		
		inputPane.add(usernamePane, 0, 0);
		inputPane.add(passwordPane, 0, 2);
		inputPane.setAlignment(Pos.CENTER);
		
		BorderPane mainPane = new BorderPane();
//		StackPane.setAlignment(inputPane, Pos.CENTER);
//		StackPane.setAlignment(loginButton, Pos.BOTTOM_CENTER);
		
		mainPane.setCenter(inputPane);
		mainPane.setBottom(loginButton);
		mainPane.setPrefWidth(INPUT_PANE_WIDTH);
		mainPane.setMaxWidth(INPUT_PANE_WIDTH);
		mainPane.setMaxHeight(INPUT_PANE_HEIGHT);
		mainPane.setOpacity(1.0);
		
		BorderPane.setAlignment(loginButton, Pos.CENTER);
		BorderPane.setMargin(loginButton, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				3*GuiConstants.DEFAULT_V_GAP, 0));
		
		this.addContent(mainPane);
	}
	
	public void showWrongDataAlert(){
		wrongData.showAndWait();
	}
	
	public void setOnLogin(EventHandler<ActionEvent> handler){
		loginButton.setOnAction((event) -> {
			if (getUsername().isEmpty()){
				blankFieldAlert("Username").showAndWait();
				return;
			}
			
			if (getPassword().isEmpty()){
				blankFieldAlert("Password").showAndWait();
				return;
			}
			
			
			handler.handle(event);
			clearFields();
		});
	}
	
	public String getUsername(){
		return usernameTextField.getText().trim();
	}
	
	public String getPassword(){
		return passwordTextField.getText().trim();
	}
	
	private Alert blankFieldAlert(String fieldname){
		Alert blankField = new Alert(AlertType.ERROR);
		blankField.setTitle("Error");
		blankField.setHeaderText(fieldname  + " must not be blank.");
		
		return blankField;
	}
	
	public void clearFields(){
		usernameTextField.setText("");
		passwordTextField.setText("");
	}
	
}
