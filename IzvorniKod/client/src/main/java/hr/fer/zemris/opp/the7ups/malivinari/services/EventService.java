package hr.fer.zemris.opp.the7ups.malivinari.services;

import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import hr.fer.zemris.opp.the7ups.malivinari.ServerInfo;
import hr.fer.zemris.opp.the7ups.malivinari.responses.RequestResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.ServerEventResponse;

@Service
public class EventService {
	
	public ServerEventResponse collect(String token){
		RestTemplate template = new RestTemplate();
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("token", token);
		ServerEventResponse event = template.postForObject(ServerInfo.getUrl("collect"), params, ServerEventResponse.class);
		
		return event;
	}
	
	public ServerEventResponse listen(String token){
		RestTemplate template = new RestTemplate();
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("token", token);
		ServerEventResponse event = template.postForObject(ServerInfo.getUrl("listen"), params, ServerEventResponse.class);
		
		return event;
	}
	
	public RequestResponse accept(String token, String eventId){
		RestTemplate template = new RestTemplate();
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("token", token);
		params.add("id", eventId);
		RequestResponse request = template.postForObject(ServerInfo.getUrl("accept"), params, RequestResponse.class);
		System.out.println("Accept status " + request.getStatus());
		
		return request;
	}
	
	public RequestResponse decline(String token, String eventId){
		RestTemplate template = new RestTemplate();
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("token", token);
		params.add("id", eventId);
		RequestResponse request = template.postForObject(ServerInfo.getUrl("decline"), params, RequestResponse.class);
		
		return request;
	}
}
