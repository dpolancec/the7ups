package hr.fer.zemris.opp.the7ups.malivinari.gui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

public class AddGroupPane extends ContentBackgroundPane {

	private static final int INPUT_PANE_WIDTH = 400;
	private static final int INPUT_PANE_HEIGHT = 400;

	private Label groupNameLabel = new SkinnableLabel("Enter group name:");
	private Label invitedUsersLabel = new SkinnableLabel("Users to invite: ");
	
	private TextField groupNameTextField;
	
	private Button addButton;
	
	private Alert wrongData = new Alert(AlertType.ERROR);
	
	private static final int INPUT_LENGTH = 160;
	private static final int INPUT_HEIGHT = 30;
	
	private List<User> members;
	
	public AddGroupPane(Set<User> selectedItems) {
		setMembers(new ArrayList<>(selectedItems));
		
		wrongData.setTitle("Error");
		wrongData.setHeaderText("Group name must not be empty!");
		
		groupNameTextField = new TextField();
		groupNameTextField.setPrefWidth(INPUT_LENGTH);
		groupNameTextField.setPrefHeight(INPUT_HEIGHT);
		
		Image confirmImage = new Image(getClass().getResourceAsStream("/images/confirm.png"));
		addButton = new Button();
		addButton.setGraphic(new ImageView(confirmImage));
		addButton.setText("Confirm");
		addButton.setTooltip(new Tooltip("Invite listed users to join your group"));
		
		String invUsersText = "";
		Iterator<User> memIter = members.iterator();
		while (memIter.hasNext()){
			invUsersText += memIter.next().getUsername();
		}
		invitedUsersLabel.setText(invitedUsersLabel.getText() + invUsersText);
		
		BorderPane.setMargin(groupNameLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(groupNameTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(invitedUsersLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		BorderPane groupNamePane = new BorderPane();
		groupNamePane.setLeft(groupNameLabel);
		groupNamePane.setRight(groupNameTextField);
		BorderPane.setMargin(groupNameLabel, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(groupNameTextField, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				0, GuiConstants.DEFAULT_H_GAP));
		
		GridPane inputPane = new GridPane();
		inputPane.setVgap(GuiConstants.DEFAULT_V_GAP);
		inputPane.setHgap(GuiConstants.DEFAULT_H_GAP);
		
		inputPane.add(groupNamePane, 0, 0);
		invitedUsersLabel.setWrapText(true);
		inputPane.add(invitedUsersLabel, 0, 1);
		inputPane.setAlignment(Pos.CENTER);
		
		BorderPane mainPane = new BorderPane();
		
		mainPane.setCenter(inputPane);
		mainPane.setBottom(addButton);
		mainPane.setPrefWidth(INPUT_PANE_WIDTH);
		mainPane.setMaxWidth(INPUT_PANE_WIDTH);
		mainPane.setMaxHeight(INPUT_PANE_HEIGHT);
		mainPane.setOpacity(1.0);
		
		BorderPane.setAlignment(addButton, Pos.CENTER);
		BorderPane.setMargin(addButton, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 
				3*GuiConstants.DEFAULT_V_GAP, 0));
		
		this.addContent(mainPane);
	}

	public void setOnAdd(EventHandler<ActionEvent> handler) {
		addButton.setOnAction((event) -> {
			if (groupNameTextField.getText().isEmpty()){
				wrongData.showAndWait();
				return;
			}
			
			handler.handle(event);
		});
		
	}

	public String getName() {
		return groupNameTextField.getText();
	}

	public List<User> getMembers() {
		return members;
	}

	public void setMembers(List<User> members) {
		this.members = members;
	}

}
