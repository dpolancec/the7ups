package hr.fer.zemris.opp.the7ups.malivinari.services;

import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import hr.fer.zemris.opp.the7ups.malivinari.ServerInfo;
import hr.fer.zemris.opp.the7ups.malivinari.responses.BooleanResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.GeneralResponse;
import hr.fer.zemris.opp.the7ups.malivinari.responses.TokenResponse;

@Service
public class SessionService {

	public TokenResponse logIn(String username, String password){
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		
		params.add("username", username);
		params.add("password", password);
		
		RestTemplate template = new RestTemplate();
		TokenResponse response = template.postForObject(ServerInfo.getUrl("login"), params,
				TokenResponse.class);
		
		return response;
	}
	
	public GeneralResponse logOut(String token){
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		
		params.add("token", token);
		
		RestTemplate template = new RestTemplate();
		GeneralResponse response = template.postForObject(ServerInfo.getUrl("logout"), params,
				GeneralResponse.class);
		
		return response;
	}
	
	public BooleanResponse isLoggedIn(String username, String token){
		
		RestTemplate template = new RestTemplate();
		BooleanResponse response = template.getForObject(ServerInfo.getUrl("is-logged-in/{username}?token={token}"),
				BooleanResponse.class, username, token);
		
		return response;
	}
	
	public BooleanResponse isBusy(String username){
		
		RestTemplate template = new RestTemplate();
		BooleanResponse response = template.getForObject(ServerInfo.getUrl("is-busy/{username}"),
				BooleanResponse.class, username);
		
		return response;
	}
	
	public GeneralResponse see(String token){
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		
		params.add("token", token);
		
		RestTemplate template = new RestTemplate();
		GeneralResponse response = template.getForObject(ServerInfo.getUrl("see?token={token}"),
				GeneralResponse.class, token);
		
		return response;
	}
	
}
