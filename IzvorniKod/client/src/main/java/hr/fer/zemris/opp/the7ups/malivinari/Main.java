package hr.fer.zemris.opp.the7ups.malivinari;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.client.RestTemplate;

import hr.fer.zemris.opp.the7ups.malivinari.responses.UserResponse;
import javafx.application.Application;

public class Main {
	
	public static void main(String[] args) {
		Application.launch(ClientApp.class, args);
	}
	
}
