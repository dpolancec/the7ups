package hr.fer.zemris.opp.the7ups.malivinari.gui;

import java.io.ByteArrayInputStream;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;

public class Inspector extends ContentBackgroundPane {

	private FlowPane entriesPane = new FlowPane(Orientation.VERTICAL);
	private StackPane imagePane = new StackPane();
	private GridPane buttonPane = new GridPane();
	private BorderPane mainPane = new BorderPane();
	
	public static final double TITLE_SIZE = 16;
	public static final double MAX_VALUE_SIZE = 500; 
	
	public Inspector(){
		entriesPane.setVgap(GuiConstants.DEFAULT_V_GAP);
		BorderPane.setMargin(imagePane, new Insets(GuiConstants.DEFAULT_V_GAP, GuiConstants.DEFAULT_H_GAP,
				GuiConstants.DEFAULT_V_GAP, GuiConstants.DEFAULT_H_GAP));
		mainPane.setLeft(entriesPane);
		mainPane.setCenter(imagePane);
		mainPane.setBottom(buttonPane);
	}
	
	public void addEntry(String key, String value){
		entriesPane.getChildren().add(entryPane(key, value));
	}
	
	private BorderPane entryPane(String key, String value){
		BorderPane entryPane = new BorderPane();
		SkinnableLabel keyLabel = new SkinnableLabel(key + ":");
		BorderPane.setMargin(keyLabel, new Insets(0, GuiConstants.DEFAULT_H_GAP, 0, 0));
		SkinnableLabel valueLabel = new SkinnableLabel(value);
		valueLabel.setMaxWidth(MAX_VALUE_SIZE);
		valueLabel.setWrapText(true);
		BorderPane.setMargin(valueLabel, new Insets(0, GuiConstants.DEFAULT_H_GAP, 0, 0));
		
		entryPane.setLeft(keyLabel);
		entryPane.setRight(valueLabel);
		
		return entryPane;
	}
	
	public void addSeparator(){
		SkinnableLabel emptyLabel = new SkinnableLabel("");
		entriesPane.getChildren().add(emptyLabel);
	}
	
	public void addSectionTitle(String title){
		SkinnableLabel titleLabel = new SkinnableLabel(title);
		titleLabel.setFont(new Font(TITLE_SIZE));
		entriesPane.getChildren().add(titleLabel);
	}
	
	public void setImage(byte[] imageBytes){
		imagePane.getChildren().clear();
		Image image = new Image(new ByteArrayInputStream(imageBytes));
		imagePane.getChildren().add(new ImageView(image));
	}
	
	public void addButton(Button button, int colIndex, int rowIndex){
		buttonPane.add(button, colIndex, rowIndex);
	}
	
	public void reset(){
		entriesPane.getChildren().clear();
		imagePane.getChildren().clear();
		buttonPane.getChildren().clear();
	}
	
}
