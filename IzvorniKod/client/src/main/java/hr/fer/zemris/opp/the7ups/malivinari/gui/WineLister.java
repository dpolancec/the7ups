package hr.fer.zemris.opp.the7ups.malivinari.gui;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import hr.fer.zemris.opp.the7ups.malivinari.models.Wine;
import hr.fer.zemris.opp.the7ups.malivinari.responses.InputStreamResponse;
import hr.fer.zemris.opp.the7ups.malivinari.services.WineService;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

public class WineLister extends Lister<Wine> {

	private WineService wineService;
	
	@Override
	public String topText(Wine item) {
		return item.getOwner().getUsername();
	}

	@Override
	public String bottomText(Wine item) {
		return item.getName();
	}

	@Override
	public Image image(Wine item) {
		InputStreamResponse imgResponse = wineService.getWineImage(item.getId());
		if (imgResponse.getData() != null){
			return new Image(imgResponse.getData());
		} else {
			return new Image(getClass().getClassLoader().getResourceAsStream("images/noImage.png"));
		}
	}

	public synchronized WineService getWineService() {
		return wineService;
	}

	public synchronized void setWineService(WineService wineService) {
		this.wineService = wineService;
	}

}
