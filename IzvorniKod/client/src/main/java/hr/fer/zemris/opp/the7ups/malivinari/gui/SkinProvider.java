package hr.fer.zemris.opp.the7ups.malivinari.gui;

import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.paint.Color;

public interface SkinProvider {

	BackgroundImage getBackgroundImage();
	BackgroundFill getBackdrop();
	double getBackdropOpacity();
	Color getFontColor();
	
}
