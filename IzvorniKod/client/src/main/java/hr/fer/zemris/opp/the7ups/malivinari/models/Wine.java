package hr.fer.zemris.opp.the7ups.malivinari.models;

public class Wine extends NamedModel{

	public static final String Q_BY_USER_ID = "Wine.findByUserId";
	public static final String Q_PARAM_ID = "param_id";
	
	private User owner;
	private Image image;
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public Image getImage() {
		return image;
	}
	public void setImage(Image image) {
		this.image = image;
	}
	
}
