package hr.fer.zemris.opp.the7ups.malivinari.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

@Entity
@Table(name="users")
@NamedQueries({
	@NamedQuery(name=User.Q_USERNAME,query="select user from User as user where user.username=:" + User.Q_PARAM_USERNAME),
	@NamedQuery(name=User.Q_EMAIL,query="select user from User as user where user.username=:" + User.Q_PARAM_EMAIL),
	@NamedQuery(name=User.Q_ALL,query="select user from User as user")
})
public class User extends NamedModel{

	public static final String Q_USERNAME = "User.findByUsername";
	public static final String Q_PARAM_USERNAME = "username_param";
	public static final String Q_EMAIL = "User.findByEmail";
	public static final String Q_PARAM_EMAIL = "email_param";
	public static final String Q_ALL = "User.findAll";
	
	private String lastName;
	@Column(nullable = false, unique = true)
	private String username;
	@Column(nullable = false)
	private String passwordHash;
	@Column(nullable = false, unique = true)
	private String email;
	private String status;
	@OneToOne(fetch=FetchType.EAGER)
	private Image image;
	private String wineryName;
	private String wineryAddress;
	@ManyToMany(fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST})
	@JoinTable(
			   name = "user_blocks", 
			   joinColumns = @JoinColumn(name = "user_id"), 
			   inverseJoinColumns = @JoinColumn(name = "blocked_id")
			 )
	private Set<User> blockedUsers = new HashSet<User>();
	@ManyToMany(fetch=FetchType.LAZY, mappedBy="members", cascade = {CascadeType.PERSIST})
	private Set<Group> groups = new HashSet<Group>();
	@OneToMany(fetch=FetchType.LAZY, mappedBy="owner", cascade = {CascadeType.ALL})
	private Set<Wine> wines = new HashSet<Wine>();
	private boolean deleted = false;
	@ManyToMany(fetch=FetchType.LAZY, mappedBy="participants")
	private Set<Communication> communications = new HashSet<Communication>();
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPasswordHash() {
		return passwordHash;
	}
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Image getImage() {
		return image;
	}
	public void setImage(Image image) {
		this.image = image;
	}
	public String getWineryName() {
		return wineryName;
	}
	public void setWineryName(String wineryName) {
		this.wineryName = wineryName;
	}
	public String getWineryAddress() {
		return wineryAddress;
	}
	public void setWineryAddress(String wineryAddress) {
		this.wineryAddress = wineryAddress;
	}
	public Set<User> getBlockedUsers() {
		return blockedUsers;
	}
	public void setBlockedUsers(Set<User> blockedUsers) {
		this.blockedUsers = blockedUsers;
	}
	public Set<Group> getGroups() {
		return groups;
	}
	public void setGroups(Set<Group> groups) {
		this.groups = groups;
	}
	public Set<Wine> getWines() {
		return wines;
	}
	public void setWines(Set<Wine> wines) {
		this.wines = wines;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public Set<Communication> getCommunications() {
		return communications;
	}
	public void setCommunications(Set<Communication> communications) {
		this.communications = communications;
	}
	
}
