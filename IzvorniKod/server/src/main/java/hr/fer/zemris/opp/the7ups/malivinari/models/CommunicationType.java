package hr.fer.zemris.opp.the7ups.malivinari.models;

public enum CommunicationType {

	GROUP, PRIVATE
}
