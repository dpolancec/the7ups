package hr.fer.zemris.opp.the7ups.malivinari;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.context.ApplicationContext;

import hr.fer.zemris.opp.the7ups.malivinari.dao.jpa.HibernateDao;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;

public class BasicService {

	public HibernateDao dao;
	
	public BasicService() {
	}
	
	public void run(ApplicationContext context){
		//DAO dao = (DAO) context.getBean("dao");
		User djuro = dao.getUser("djuro1233");
		djuro.setCommunications(null);
		System.out.println(djuro.getCommunications());
	}
	
	public void run2(){
		//DAO dao = (DAO) context.getBean("dao");
		User user = new User();
		
		user.setName("Pero");
		user.setLastName("Perić");
		user.setUsername("perica12345");
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		m.reset();
		m.update("lozinka123".getBytes());
		byte[] digest = m.digest();
		String hash = new BigInteger(1,digest).toString(16);
		user.setPasswordHash(hash);
		user.setEmail("perica13@perica.hr");
		user.setWineryName("Peričina vinarija");
		user.setWineryAddress("Peričina 1");
		user.setStatus("Zdravo, ja sam Perica i trenutno sam sretan.");
		System.out.println(user.getLastName());
		
		dao.addUser(user);
		
		user = new User();
		
		user.setName("Đuro");
		user.setLastName("Đutić");
		user.setUsername("djuro1233");
		m.reset();
		m.update("lozinka1234".getBytes());
		digest = m.digest();
		hash = new BigInteger(1,digest).toString(16);
		user.setPasswordHash(hash);
		user.setEmail("đuro1@perica.hr");
		user.setWineryName("Đurina vinarija");
		user.setWineryAddress("Đurina 1");
		user.setStatus("Zdravo, ja sam Đuro i trenutno sam sretan.");
		dao.addUser(user);
		
		User pero = dao.getUser("perica12345");
		User djuro = dao.getUser("djuro1233");
		
		dao.blockUser(pero.getId(), djuro.getId());
		
		//System.out.println(dao.getBlockedUsers(pero.getId()).get(0).getUsername());
	}

}
