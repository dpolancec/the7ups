package hr.fer.zemris.opp.the7ups.malivinari.models;

import java.sql.Blob;
import java.sql.SQLException;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="images")
public class Image extends Model{

	@OneToOne(mappedBy = "image", fetch=FetchType.EAGER, cascade = {CascadeType.ALL})
	private User owner;
	@OneToOne(mappedBy = "image", fetch=FetchType.EAGER, cascade = {CascadeType.ALL})
	private Wine wine;
	@Column(nullable = false)
	private Blob data;
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public Blob getData() {
		return data;
	}
	public void setData(Blob data) {
		this.data = data;
	}
	
	public byte[] getImageBytes() throws SQLException{
		if (data == null){
			return null;
		}
		return data.getBytes(1, (int)data.length());
	}
	
}
