package hr.fer.zemris.opp.the7ups.malivinari.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.server.Session;
import hr.fer.zemris.opp.the7ups.malivinari.services.SessionService;
import hr.fer.zemris.opp.the7ups.malivinari.services.UserService;
import hr.fer.zemris.opp.the7ups.malivinari.utils.HashUtils;
import hr.fer.zemris.opp.the7ups.malivinari.utils.ResponseUtils;

import static hr.fer.zemris.opp.the7ups.malivinari.rules.ParamRules.*;

import java.io.IOException;
import java.util.List;
import java.util.Set;

@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private SessionService sessionService;

	@RequestMapping(value = "/register", method = RequestMethod.POST)
    public Response register(
    		@RequestParam(value="name", defaultValue="") String name,
    		@RequestParam(value="username") String username,
    		@RequestParam(value="password") String password,
    		@RequestParam(value="email") String email,
    		@RequestParam(value="lastName", defaultValue="") String lastName
    		) {
		
		if (!checkUsername(username)){
			return new Response(null, Status.BAD_USERNAME);
		}
		
		if (!checkPassword(password)){
			return new Response(null, Status.BAD_PASSWORD);
		}
		
		if (!checkEmail(email)){
			return new Response(null, Status.BAD_EMAIL);
		}
		
		if (userService.usernameExists(username)){
			return new Response(null, Status.USERNAME_TAKEN);
		}
		
		if (userService.emailExists(email)){
			return new Response(null, Status.EMAIL_TAKEN);
		}
		
		System.out.println("Received data: " + "username: " + username +", password: " + password + 
				", name: " + name + ", lastName: " + lastName + ", email: " + email);
		User user = userService.registerUser(name, lastName, username, password, email);
		
		return new Response(ResponseUtils.privateResponse(user), Status.OK);
    }
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
    public Response update(
    		@RequestParam(value="name", defaultValue = "") String name,
    		@RequestParam(value="password", defaultValue = "") String password,
    		@RequestParam(value="password", defaultValue = "") String oldPassword,
    		@RequestParam(value="email", defaultValue = "") String email,
    		@RequestParam(value="lastName", defaultValue = "") String lastName,
    		@RequestParam(value="wineryName", defaultValue = "") String wineryName,
    		@RequestParam(value="wineryAddress", defaultValue = "") String wineryAddress,
    		@RequestParam(value="status", defaultValue = "") String status,
    		@RequestParam(value="image") MultipartFile image,
    		@RequestParam(value="token") String token
    		) {
		
		Session session = sessionService.getSession(token);
		
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		
		User user = session.getUser();
		
		if (!password.isEmpty()){
			if (!HashUtils.checkHash(oldPassword, user.getPasswordHash())){
				return new Response(null, Status.FORBIDDEN);
			}
			
			if (!checkPassword(password)){
				return new Response(null, Status.BAD_PASSWORD);
			}
			
			user.setPasswordHash(HashUtils.hashString(password));
		}
		
		
		
		if (!email.isEmpty()){
			if (!checkEmail(email)){
				return new Response(null, Status.BAD_EMAIL);
			}
			
			if (userService.emailExists(email)){
				return new Response(null, Status.EMAIL_TAKEN);
			}
			
			user.setEmail(email);
		}
		
		if (!name.isEmpty()){
			user.setName(name);
		}
		
		if (!lastName.isEmpty()){
			user.setLastName(lastName);
		}
		
		if (!wineryName.isEmpty()){
			user.setWineryName(wineryName);
		}
		
		if (!wineryAddress.isEmpty()){
			user.setWineryAddress(wineryAddress);
		}
		
		if (!status.isEmpty()){
			user.setStatus(status);
		}
		
		byte[] imageBytes = null;
		if (image != null){
			try {
				imageBytes = image.getBytes();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		userService.update(user, imageBytes);
		return new Response(ResponseUtils.privateResponse(user), Status.OK);
    }
	
	@RequestMapping(value = "/user-username/{username}", method = RequestMethod.GET)
    public Response getUser(
    		@PathVariable(value="username") String username,
    		@RequestParam(value="token", defaultValue="") String token
    		) {
		
		User user = userService.getUser(username);
		return getResponse(user, token);
    }
	
	@RequestMapping(value = "/user-id/{id}", method = RequestMethod.GET)
    public Response getUser(
    		@PathVariable(value="username") int id,
    		@RequestParam(value="token", defaultValue="") String token
    		) {
		
		User user = userService.getUser(id);
		return getResponse(user, token);
    }
	
	@RequestMapping(value = "/user-email/{email}", method = RequestMethod.GET)
    public Response getUserByEmail(
    		@PathVariable(value="username") String email,
    		@RequestParam(value="token", defaultValue="") String token
    		) {
		
		User user = userService.getUserByEmail(email);
		return getResponse(user, token);
    }
	
	@RequestMapping(value = "/users/", method = RequestMethod.GET)
    public Response getAllUsers(
    		@RequestParam(value="token", defaultValue="") String token
    		) {
		
		User loggedInUser = null;
		Session session = sessionService.getSession(token);
		if (session != null){
			loggedInUser = session.getUser();
		}
		
		List<User> users = userService.getAllUsers();
		for (User user: users){
			if (user.equals(loggedInUser)){
				ResponseUtils.privateResponse(user);
			} else {
				ResponseUtils.publicResponse(user);
			}
		}
		return new Response(users, Status.OK);
    }
	
	@RequestMapping(value = "/users-blocked/", method = RequestMethod.GET)
    public Response getBlockedUsers(
    		@RequestParam(value="token", defaultValue="") String token
    		) {
		
		User loggedInUser = null;
		Session session = sessionService.getSession(token);
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		
		loggedInUser = session.getUser();
		Set<User> users = userService.getBlockedUsers(loggedInUser);
		for (User user: users){
			if (user.equals(loggedInUser)){
				ResponseUtils.privateResponse(user);
			} else {
				ResponseUtils.publicResponse(user);
			}
		}
		return new Response(users, Status.OK);
    }
	
	@RequestMapping(value = "/block", method = RequestMethod.POST)
    public Response block(
    		@RequestParam(value="token", defaultValue="") String token,
    		@RequestParam(value="user", defaultValue="") String user
    		) {
		
		Session session = sessionService.getSession(token);
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		
		User blocker = session.getUser();
		User blocked = userService.getUser(user);
		
		if (blocked == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		userService.blockUser(blocker.getId(), blocked.getId());
		
		return new Response(null, Status.OK);
    }
	
	@RequestMapping(value = "/unblock", method = RequestMethod.POST)
    public Response unblock(
    		@RequestParam(value="token", defaultValue="") String token,
    		@RequestParam(value="user", defaultValue="") String user
    		) {
		
		Session session = sessionService.getSession(token);
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		
		User blocker = session.getUser();
		User blocked = userService.getUser(user);
		
		if (blocked == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		userService.unblockUser(blocker.getId(), blocked.getId());
		
		return new Response(null, Status.OK);
    }
	
	private Response getResponse(User user, String token){
		if (user == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		Session session = sessionService.getSession(token);
		boolean isPublic = session == null || !session.getUser().getUsername().equals(user.getUsername());
		
		return new Response(ResponseUtils.userResponse(user, isPublic), Status.OK);
	}
	
}
