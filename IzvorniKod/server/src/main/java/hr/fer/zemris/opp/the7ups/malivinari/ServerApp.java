package hr.fer.zemris.opp.the7ups.malivinari;

import org.apache.coyote.AbstractProtocol;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableAutoConfiguration(exclude = {
        org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration.class,
		org.springframework.boot.autoconfigure.security.SecurityFilterAutoConfiguration.class})
public class ServerApp {

	private static final int CONNECTION_TIMEOUT = -1;
	
	@Bean
	public EmbeddedServletContainerFactory servletContainerFactory() {
	    TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();

	    factory.addConnectorCustomizers(connector -> 
	            ((AbstractProtocol) connector.getProtocolHandler()).setConnectionTimeout(CONNECTION_TIMEOUT));

	    // configure some more properties

	    return factory;
	}
}
