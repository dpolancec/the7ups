package hr.fer.zemris.opp.the7ups.malivinari.services;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.fer.zemris.opp.the7ups.malivinari.dao.jpa.HibernateDao;
import hr.fer.zemris.opp.the7ups.malivinari.models.Communication;
import hr.fer.zemris.opp.the7ups.malivinari.models.CommunicationType;
import hr.fer.zemris.opp.the7ups.malivinari.models.Group;
import hr.fer.zemris.opp.the7ups.malivinari.models.Message;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.server.events.EventTypes;
import hr.fer.zemris.opp.the7ups.malivinari.server.events.Request;
import hr.fer.zemris.opp.the7ups.malivinari.server.events.ServerEvent;
import hr.fer.zemris.opp.the7ups.malivinari.server.events.UserInitiatedEvent;

@Service
@Transactional
public class CommService {

	@Autowired
	private HibernateDao dao;
	@Autowired
	private GroupService groupService;
	@Autowired
	private SessionService sessionService;
	@Autowired
	private EventService eventService;
	
	private Set<Communication> pausedComms = new HashSet<>();
	private Set<Communication> activeComms = new HashSet<>();
	
	public Communication getComm(int commId){
		return dao.getCommunication(commId);
	}
	
	public List<Communication> getComms(int userId){
		return dao.getCommunications(userId);
	}
	
	public int startPrivateComm(User firstUser, User secondUser){
		Communication communication = new Communication();
		Set<User> participants = new HashSet<>();
		participants.add(firstUser);
		participants.add(secondUser);
		communication.setParticipants(participants);
		communication.setType(CommunicationType.PRIVATE);
		
		for (User participant: participants){
			if (sessionService.isLoggedIn(participant.getUsername())){
				sessionService.setBusy(participant.getUsername(), true);
			}
		}
		
		int commId = dao.startCommunication(communication);
		activeComms.add(communication);
		return commId;
	}
	
	public void startGroupComm(int groupId){
		Group group = groupService.getGroup(groupId);
		Communication communication = new Communication();
		communication.setGroup(group);
		communication.setParticipants(groupService.getGroupMembers(groupId));
		communication.setType(CommunicationType.GROUP);
		
		dao.startCommunication(communication);
		activeComms.add(communication);
	}
	
	public List<User> getParticipants(int commId){
		return dao.getParticipants(commId);
	}
	
	public void appendCommunication(int commId, String sender, String messageText){
		Message message = new Message();
		String messageBody = sender + ": " + messageText;
		message.setBody(messageBody);
		List<User> participants = getParticipants(commId); 
		Set<String> toNotify = new HashSet<>();
		for (User user: participants){
			toNotify.add(user.getUsername());
		}
		
		dao.appendCommunication(commId, message);
		ServerEvent event = new ServerEvent(toNotify, EventTypes.COMM_APPEND, false);
		event.setAdditionalInfo(commId + ";" + messageBody);
		eventService.publishEvent(event, false);
	}
	
	public void endCommunication(int commId){
		List<User> participants = getParticipants(commId);
		Set<String> toNotify = new HashSet<>();
		for (User participant: participants){
			if (sessionService.isLoggedIn(participant.getUsername())){
				sessionService.setBusy(participant.getUsername(), false);
			}
			
			toNotify.add(participant.getUsername());
		}
		dao.endCommunication(commId);
		ServerEvent event = new ServerEvent(toNotify, EventTypes.COMM_END, false);
		event.setAdditionalInfo(Integer.toString(commId));
		eventService.publishEvent(event, false);
		activeComms.remove(getComm(commId));
	}
	
	public List<Message> getMessages(int commId){
		return dao.getMessages(commId);
	}
	
	public Request createCommCreateRequest(User initiator, User invited){
		Set<String> invitedUsers = new HashSet<>();
		invitedUsers.add(invited.getUsername());
		invitedUsers.add(initiator.getUsername());
		Request request = new Request(new UserInitiatedEvent(invitedUsers,
				EventTypes.COMM_REQUEST, true, initiator.getUsername()), () -> {
					int commId = startPrivateComm(initiator, invited);
					// order client to show the messenger
					ServerEvent showMessenger = new ServerEvent(invitedUsers, EventTypes.COMM_SHOW, false);
					showMessenger.setAdditionalInfo(Integer.toString(commId));
					eventService.publishEvent(showMessenger, true);
		});
		
		return request;
	}
	
	public void pauseComm(int commId){
		pausedComms.add(getComm(commId));
	}
	
	public void resumeComm(int commId){
		pausedComms.remove(getComm(commId));
	}
	
	public boolean isPausedComm(int commId){
		return pausedComms.contains(getComm(commId));
	}
	
}
