package hr.fer.zemris.opp.the7ups.malivinari.services;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.fer.zemris.opp.the7ups.malivinari.dao.jpa.HibernateDao;
import hr.fer.zemris.opp.the7ups.malivinari.models.Image;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.utils.HashUtils;

@Transactional
@Service
public class UserService {

	@Autowired
	public HibernateDao dao;
	
	public UserService() {}
	
	public User registerUser(String name, String lastName, String username, String password, String email){
		User user = new User();
		user.setName(name);
		user.setLastName(lastName);
		user.setEmail(email);
		user.setPasswordHash(HashUtils.hashString(password));
		user.setUsername(username);
		
		System.out.println("pasword hash: " + user.getPasswordHash());
		
		dao.addUser(user);
		
		return user;
	}
	
	public boolean usernameExists(String username){
		if (dao.getUser(username) != null){
			return true;
		}
		
		return false;
	}
	
	public boolean emailExists(String email){
		if (getUserByEmail(email) != null){
			return true;
		}
		
		return false;
	}
	
	public User getUser(String username){
		return dao.getUser(username);
	}
	
	public User getUser(int id){
		return dao.getUser(id);
	}
	
	public User getUserByEmail(String email){
		return dao.getUserByEmail(email);
	}
	
	public User update(User user, byte[] imageBytes){
		Image image = dao.addImage(imageBytes);
		user.setImage(image);
		dao.updateUser(user);
		return user;
	}

	public void removeUser(User user){
		dao.removeUser(user.getId());
	}
	
	public void removeUser(int userId){
		dao.removeUser(userId);
	}
	
	public Set<User> getBlockedUsers(User user){
		if (user == null){
			return null;
		}
		return dao.getBlockedUsers(user.getId());
	}
	
	public Set<User> getBlockedUsers(int userId){
		return getBlockedUsers(dao.getUser(userId));
	}
	
	public List<User> getAllUsers(){
		return dao.getAllUsers();
	}
	
	public void blockUser(int blockerId, int blockedId){
		dao.blockUser(blockerId, blockedId);
	}
	
	public void unblockUser(int blockerId, int blockedId){
		dao.unblockUser(blockerId, blockedId);
	}

}
