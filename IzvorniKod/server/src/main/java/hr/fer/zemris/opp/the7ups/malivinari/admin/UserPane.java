package hr.fer.zemris.opp.the7ups.malivinari.admin;

import java.util.Collection;
import java.util.stream.Collectors;

import hr.fer.zemris.opp.the7ups.malivinari.gui.UserLister;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import javafx.scene.Node;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;

public class UserPane extends BorderPane{

	private ToolBar toolbar;
	private UserLister lister;
	
	public UserPane(Collection<User> users, Node ... toolbarItems) {
		super();
		
		toolbar = new ToolBar(toolbarItems);
		this.setTop(toolbar);
		
		lister = new UserLister();
		refresh(users);
		this.setCenter(lister);
	}
	
	public User getSelectedUser(){
		return lister.getSelectionModel().getSelectedItem();
	}
	
	public void refresh (Collection<User> users){
		lister.setItems(users
				.stream()
				.filter((user) -> !user.isDeleted())
				.collect(Collectors.toList()));
	}
	
}
