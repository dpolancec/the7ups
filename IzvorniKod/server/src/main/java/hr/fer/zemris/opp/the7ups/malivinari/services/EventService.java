package hr.fer.zemris.opp.the7ups.malivinari.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.async.DeferredResult;

import hr.fer.zemris.opp.the7ups.malivinari.server.ServerContext;
import hr.fer.zemris.opp.the7ups.malivinari.server.Session;
import hr.fer.zemris.opp.the7ups.malivinari.server.events.EventListener;
import hr.fer.zemris.opp.the7ups.malivinari.server.events.FinalizedRequestEvent;
import hr.fer.zemris.opp.the7ups.malivinari.server.events.Request;
import hr.fer.zemris.opp.the7ups.malivinari.server.events.ServerEvent;

@Service
@Transactional
public class EventService {

	@Autowired
	private SessionService sessionService;
	
	private Map<String, DeferredResult<ServerEvent>> listeners = new HashMap<>();
	private Map<String, List<Request>> activeRequests = new HashMap<>();
	private Map<String, List<ServerEvent>> pendingEvents = new HashMap<>();
	
	public synchronized boolean publishEvent(ServerEvent event, boolean requireLoggedIn){
		for (String string: event.getRelevantUsers()){
			if (!sessionService.isLoggedIn(string) && requireLoggedIn){
				return false;
			}
		}
		
		for (String string: event.getRelevantUsers()){
			if (!sessionService.isLoggedIn(string)){
				continue;
			}
			if (listeners.containsKey(string)){
				listeners.get(string).setResult(event);
				listeners.remove(string);
			} else {
				List<ServerEvent> myPendingEvents = pendingEvents.get(string);
				if (myPendingEvents == null){
					myPendingEvents = new ArrayList<>();
					pendingEvents.put(string, myPendingEvents);
				}
				
				System.out.println(event.getEventId());
				myPendingEvents.add(event);
			}
		}
		return true;
	}
	
	public synchronized void finalizeRequest(Request request){
		Set<String> usersToNotify = new HashSet<>();
		for (String username: request.getEvent().getRelevantUsers()){
			if (sessionService.isLoggedIn(username) && !sessionService.isBusy(username)){
				usersToNotify.add(username);
				getRequests(username).remove(request);
			}
		}
		
		System.out.println("Finalize request: " + request.getEvent().getEventId());
		publishEvent(new FinalizedRequestEvent(usersToNotify, request), false);
	}
	
	public synchronized Request acceptRequest(String username, String eventId){
		Request request = getRequest(username, eventId);
		System.out.println("Event id:" + eventId);
		System.out.println("This shouldn't be null: " + request);
		if (request != null){
			request.setNumAccepts(request.getNumAccepts() + 1);
			System.out.println(request.getNumAccepts());
			if (request.isAccepted()){
				request.getOnAccepted().run();
				finalizeRequest(request);
			}
		}
		
		return request;
	}
	
	public synchronized Request declineRequest(String username, String eventId){
		Request request = getRequest(username, eventId);
		if (request != null){
			finalizeRequest(request);
		}
		
		return request;
	}
	
	public synchronized List<Request> getRequests(String username){
		if (activeRequests.get(username) == null){
			return new ArrayList<>();
		}
		
		return activeRequests.get(username);
	}
	
	public synchronized Request getRequest(String username, String eventId){
		if (activeRequests.get(username) == null){
			return null;
		}
		
		for (Request request: activeRequests.get(username)){
			if (request.getEvent().getEventId().equals(eventId)){
				return request;
			}
		}
		
		return null;
	}
	
	public synchronized boolean publishRequest(Request request, boolean requireLoggedIn){
		if (request == null){
			return false;
		}
		
		for (String username: request.getEvent().getRelevantUsers()){
			if (sessionService.isLoggedIn(username)){
				List<Request> myActiveRequests = activeRequests.get(username);
				if (myActiveRequests == null){
					myActiveRequests = new ArrayList<>();
					activeRequests.put(username, myActiveRequests);
				}
				myActiveRequests.add(request);
			}
		}
		
		return publishEvent(request.getEvent(), requireLoggedIn);
	}
	
	public synchronized List<ServerEvent> collectEvents(String username){
		return pendingEvents.get(username);
	}
	
	public synchronized void register(String username, DeferredResult<ServerEvent> deferred){
		List<ServerEvent> myPendingEvents = pendingEvents.get(username);
		if (myPendingEvents == null || myPendingEvents.isEmpty()){
			listeners.put(username, deferred);
			return;
		}
		deferred.setResult(myPendingEvents.remove(0));
	}
	
	public synchronized void unregister(String username){
		listeners.remove(username);
	}
	
	public synchronized void clearAll(String username){
		List<Request> toRemove = new ArrayList<>();
		if (activeRequests.get(username) != null){
			for (Request request: activeRequests.get(username)){
				toRemove.add(request);
			}
			
			for (Request request: toRemove){
				declineRequest(username, request.getEvent().getEventId());
			}
		}
		
		activeRequests.remove(username);
		pendingEvents.remove(username);
		unregister(username);
	}

}
