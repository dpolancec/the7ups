package hr.fer.zemris.opp.the7ups.malivinari.utils;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import hr.fer.zemris.opp.the7ups.malivinari.models.User;

public class ExtractUtils {

	private ExtractUtils() {}

	public static Set<String> extractUsernames(Collection<User> users){
		Set<String> usernames = new HashSet<>();
		for (User user: users){
			usernames.add(user.getUsername());
		}
		
		return usernames;
	}
	
}
