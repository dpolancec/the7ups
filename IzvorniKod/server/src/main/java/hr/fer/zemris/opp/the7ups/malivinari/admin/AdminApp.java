package hr.fer.zemris.opp.the7ups.malivinari.admin;

import java.util.Collection;
import java.util.Optional;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import hr.fer.zemris.opp.the7ups.malivinari.config.DataAccessConfig;
import hr.fer.zemris.opp.the7ups.malivinari.gui.GuiUtils;
import hr.fer.zemris.opp.the7ups.malivinari.gui.RegisterPane;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.services.UserService;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tooltip;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

@Configuration
@ComponentScan
public class AdminApp extends Application{

	private static final int DEFAULT_CP_WIDTH = 480;
	private static final int DEFAULT_CP_HEIGHT = 150;
	
	private static final int DEFAULT_UP_WIDTH = 1280;
	private static final int DEFAULT_UP_HEIGHT = 760;
	
	private static final int DEFAULT_RP_WIDTH = 640;
	private static final int DEFAULT_RP_HEIGHT = 320;
	
	private ServerControlPane controlPane;
	private ApplicationContext myContext;
	
	private UserService userService;
	
	public AdminApp() {}

	@Override
	public void start(Stage primaryStage) throws Exception {
		myContext = new AnnotationConfigApplicationContext(AdminApp.class, DataAccessConfig.class);
		userService = myContext.getBean(UserService.class);
		
		primaryStage.setTitle("Server control panel");
		
		controlPane = myContext.getBean(ServerControlPane.class);
		controlPane.setVisible(true);
		
		Scene scene = new Scene(controlPane, DEFAULT_CP_WIDTH, DEFAULT_CP_HEIGHT);
		scene.getStylesheets().add("stylesheets.css");
		primaryStage.setOnCloseRequest((event) -> {
			ApplicationContext context = controlPane.getServerAppContext();
			if (context != null){
				controlPane.stopServer();
			}
			
			Platform.exit();
		});
		primaryStage.setScene(scene);
		primaryStage.show();
		
		Stage userStage = new Stage();
		userStage.setTitle("Registered users");
		
		// button definitions
		
		Image imageRefresh = new Image(getClass().getResourceAsStream("/images/download.png"));
		Button refreshButton = new Button();
		refreshButton.setGraphic(new ImageView(imageRefresh));
		refreshButton.setTooltip(new Tooltip("Get new data"));
		
		Image imageAddUser = new Image(getClass().getResourceAsStream("/images/add.png"));
		Button addUserButton = new Button();
		addUserButton.setGraphic(new ImageView(imageAddUser));
		addUserButton.setTooltip(new Tooltip("Add new user"));
		
		Image imageRemoveUser = new Image(getClass().getResourceAsStream("/images/cancel.png"));
		Button removeUserButton = new Button();
		removeUserButton.setGraphic(new ImageView(imageRemoveUser));
		removeUserButton.setTooltip(new Tooltip("Delete user"));
		
		// userPane definition and initialization
		
		Collection<User> users = userService.getAllUsers();
		UserPane userPane = new UserPane(users, refreshButton, addUserButton, removeUserButton);
		userPane.setPrefHeight(DEFAULT_UP_HEIGHT);
		userPane.setPrefWidth(DEFAULT_UP_WIDTH);
		
		// button actions
		
		refreshButton.setOnAction((event) -> {
			userPane.refresh(userService
					.getAllUsers());
		});
		
		addUserButton.setOnAction((event) -> {
		
			Stage registerStage = new Stage();
			RegisterPane registerPane = new RegisterPane();
			registerPane.setOnRegister((regEvent) -> {
				try{
					userService.registerUser(null, null, registerPane.getUsername(), 
							registerPane.getPassword(), registerPane.getEmail());
					userPane.refresh(userService.getAllUsers());
				} catch (Exception e){
					GuiUtils.exceptionAlert("Error", "User registration error", 
							"An error occurred during registration.", e);
				}
				registerStage.hide();
			});
			Scene regScene = new Scene(registerPane, DEFAULT_RP_WIDTH, DEFAULT_RP_HEIGHT);
			registerStage.setScene(regScene);
			registerStage.setTitle("Register");
			registerStage.show();
		});
		
		removeUserButton.setOnAction((event) -> {
			
			User selectedUser = userPane.getSelectedUser();
			if (selectedUser == null){
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error");
				alert.setContentText("No users selected");
				alert.showAndWait();
				return;
			}
			
			Optional<ButtonType> result = GuiUtils.confirmAlert("Deleting user", "Are you sure you"
					+ " want to delete user " + selectedUser.getUsername() + " ?", null).showAndWait();
			if (result.get() == ButtonType.OK){
				userService.removeUser(selectedUser);
				userPane.refresh(userService.getAllUsers());
			}
		});
		
		userStage.setScene(new Scene(userPane, DEFAULT_UP_WIDTH, DEFAULT_UP_HEIGHT));
		userStage.show();
	}

}
