package hr.fer.zemris.opp.the7ups.malivinari.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.fer.zemris.opp.the7ups.malivinari.dao.jpa.HibernateDao;
import hr.fer.zemris.opp.the7ups.malivinari.models.Image;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.models.Wine;

@Service
@Transactional
public class WineService {

	@Autowired
	public HibernateDao dao;
	
	public WineService() {}

	public List<Wine> getWines(String username){
		
		User owner = dao.getUser(username);
		
		if (owner == null){
			return null;
		}
		
		List<Wine> wines = new ArrayList<Wine>(owner.getWines());
		// order from newest to oldest
		wines.sort((first, second) -> -Long.compare(first.getTimeCreated().getTime(), second.getTimeCreated().getTime()));
		
		return wines;
	}
	
	public Wine addWine(String name, String ownerUsername, byte[] imageBytes){
		
		User owner = dao.getUser(ownerUsername);
		
		if (owner == null){
			return null;
		}
		
		return dao.addWine(owner, name, imageBytes);
	}
	
	public Wine getWine(int id){
		return dao.getWine(id);
	}
	
	public Wine updateWine(Wine wine, byte[] imageBytes){
		Image image = dao.addImage(imageBytes);
		wine.setImage(image);
		dao.updateWine(wine);
		return wine;
	}
	
}
