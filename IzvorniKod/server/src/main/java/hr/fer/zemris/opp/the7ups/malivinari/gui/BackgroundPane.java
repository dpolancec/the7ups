package hr.fer.zemris.opp.the7ups.malivinari.gui;

import javafx.scene.layout.Background;
import javafx.scene.layout.StackPane;

public class BackgroundPane extends StackPane implements Skinnable {

	public BackgroundPane (){
		this.setBackground(new Background(SkinContext.getSkinProvider().getBackgroundImage()));
	}
	
	@Override
	public void notifyProviderChanged(SkinProvider newProvider) {
		this.setBackground(new Background(newProvider.getBackgroundImage()));
	}

}
