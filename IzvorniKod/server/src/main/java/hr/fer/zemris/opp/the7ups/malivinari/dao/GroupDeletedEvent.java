package hr.fer.zemris.opp.the7ups.malivinari.dao;

import java.util.Set;

import hr.fer.zemris.opp.the7ups.malivinari.models.Group;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;

public class GroupDeletedEvent implements DAOEvent {

	private Set<User> members;
	private Group group;
	
	public GroupDeletedEvent(Group group) {
		super();
		this.group = group;
		this.members = group.getMembers();
	}

	public Set<User> getMembers() {
		return members;
	}

	public Group getGroup() {
		return group;
	}
	
}
