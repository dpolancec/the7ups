package hr.fer.zemris.opp.the7ups.malivinari;

import hr.fer.zemris.opp.the7ups.malivinari.admin.AdminApp;
import javafx.application.Application;


public class Main {
	
	public static void main(String[] args) throws InterruptedException {
		Application.launch(AdminApp.class, args);
	}

}
