package hr.fer.zemris.opp.the7ups.malivinari.dao.exceptions;

import hr.fer.zemris.opp.the7ups.malivinari.dao.DAO;

public class WineNotFoundException extends DAOException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WineNotFoundException(DAO source) {
		super(source);
	}

	public WineNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace,
			DAO source) {
		super(message, cause, enableSuppression, writableStackTrace, source);
	}

	public WineNotFoundException(String message, Throwable cause, DAO source) {
		super(message, cause, source);
	}

	public WineNotFoundException(String message, DAO source) {
		super(message, source);
	}

	public WineNotFoundException(Throwable cause, DAO source) {
		super(cause, source);
	}

}
