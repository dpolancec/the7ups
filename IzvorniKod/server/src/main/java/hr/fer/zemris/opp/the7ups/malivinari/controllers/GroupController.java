package hr.fer.zemris.opp.the7ups.malivinari.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.zemris.opp.the7ups.malivinari.models.Group;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.server.Session;
import hr.fer.zemris.opp.the7ups.malivinari.server.events.Request;
import hr.fer.zemris.opp.the7ups.malivinari.services.EventService;
import hr.fer.zemris.opp.the7ups.malivinari.services.GroupService;
import hr.fer.zemris.opp.the7ups.malivinari.services.SessionService;
import hr.fer.zemris.opp.the7ups.malivinari.services.UserService;
import hr.fer.zemris.opp.the7ups.malivinari.utils.ResponseUtils;

@RestController
public class GroupController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private GroupService groupService;
	@Autowired
	private SessionService sessionService;
	@Autowired
	private EventService eventService;
	 
	@RequestMapping(value = "/group/{id}", method = RequestMethod.GET)
    public Response getGroup(
    		@PathVariable(value="id") int id,
    		@RequestParam(value="token") String token
    		) {
		
		Group group = groupService.getGroup(id);
		
		if (group == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		Set<User> members = groupService.getGroupMembers(id);
		User user = null;
		Session session = sessionService.getSession(token);
		if (session != null){
			user = session.getUser();
		}
		
		return new Response(ResponseUtils.groupResponse(group, members, user), Status.OK);
    }
	
	@RequestMapping(value = "/groups/{username}", method = RequestMethod.GET)
    public Response getGroups(
    		@PathVariable(value="username") String username,
    		@RequestParam(value="token") String token
    		) {
		
		Set<Group> groups = groupService.getGroups(username);
		if (groups == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		User user = null;
		Session session = sessionService.getSession(token);
		if (session != null){
			user = session.getUser();
		}
		
		for (Group group: groups){
			Set<User> members = groupService.getGroupMembers(group.getId());
			ResponseUtils.groupResponse(group, members, user);
		}
		
		return new Response(groups, Status.OK);
    }
	
	@RequestMapping(value = "/members/{id}", method = RequestMethod.GET)
    public Response getMembers(
    		@PathVariable(value="id") int id,
    		@RequestParam(value="token", defaultValue="") String token
    		) {
		
		Group group = groupService.getGroup(id);
		
		if (group == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		User user = null;
		Session session = sessionService.getSession(token);
		if (session != null){
			user = session.getUser();
		}
		
		Set<User> members = groupService.getGroupMembers(id);
		ResponseUtils.userCollectionResponse(members, user);
		
		return new Response(members, Status.OK);
    }
	
	@RequestMapping(value = "/group/request-create", method = RequestMethod.POST)
    public Response requestCreateGroup(
    		@RequestParam(value="name", defaultValue = "") String name,
    		@RequestParam(value="user[]") String[] user,
    		@RequestParam(value="token") String token
    		) {
		
		System.out.println("POST /group/request-create");
		
		if (name.isEmpty()){
			return new Response(null, Status.BAD_GROUP_DATA);
		}
		
		User initiator = null;
		Session session = sessionService.getSession(token);
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		initiator = session.getUser();
		Set<User> invitedUsers = new HashSet<>();
		System.out.println(Arrays.toString(user));
		for (String username: user){
			User invitedUser = userService.getUser(username);
			if (invitedUser == null || !sessionService.isLoggedIn(username)){
				return new Response(null, Status.NOT_FOUND);
			}
			invitedUsers.add(invitedUser);
		}
		invitedUsers.add(initiator);
		
		for (User invitedUser: invitedUsers){
			Set<User> blockedUsers = userService.getBlockedUsers(invitedUser);
			blockedUsers.retainAll(invitedUsers);
			if (!blockedUsers.isEmpty()){
				return new Response(null, Status.BAD_REQUEST);
			}
		}
		
		Request request = groupService.createGroupCreateRequest(invitedUsers, name, initiator);
		
		if (eventService.publishRequest(request, true)){
			return new Response(request, Status.OK);
		}
		
		return new Response(null, Status.NOT_FOUND);
    }
	
	@RequestMapping(value = "/group/request-disband", method = RequestMethod.POST)
    public Response requestDisbandGroup(
    		@RequestParam(value="id") int groupId,
    		@RequestParam(value="token") String token
    		) {
		
		Group group = groupService.getGroup(groupId);
		if (group == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		Session session = sessionService.getSession(token);
		
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		
		User initiator = session.getUser();
		Set<User> members = groupService.getGroupMembers(groupId);
		if (!members.contains(initiator)){
			return new Response(null, Status.FORBIDDEN);
		}
		
		Request request = groupService.createGroupDisbandRequest(groupId, initiator);
		
		if (eventService.publishRequest(request, true)){
			return new Response(request, Status.OK);
		}
		
		return new Response(null, Status.BAD_REQUEST);
    }
	
	@RequestMapping(value = "/group-leave", method = RequestMethod.POST)
    public Response leaveGroup(
    		@RequestParam(value="id") int id,
    		@RequestParam(value="token") String token
    		) {
		
		Group group = groupService.getGroup(id);
		
		if (group == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		User user = null;
		Session session = sessionService.getSession(token);
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		user = session.getUser();
		
		Set<User> members = groupService.getGroupMembers(id);
		if (!members.contains(user)){
			return new Response(null, Status.BAD_GROUP_DATA);
		}
		groupService.leaveGroup(id, user.getUsername());
		
		return new Response(null, Status.OK);
    }
	
}
