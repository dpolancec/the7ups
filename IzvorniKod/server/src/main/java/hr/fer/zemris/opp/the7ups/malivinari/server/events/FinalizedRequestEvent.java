package hr.fer.zemris.opp.the7ups.malivinari.server.events;

import java.util.Set;

public class FinalizedRequestEvent extends ServerEvent {

	private String requestEventId;
	private int numAccepts;
	
	public FinalizedRequestEvent(Set<String> relevantUsers, Request request) {
		super(relevantUsers, request.isAccepted() ?  EventTypes.SUCCESS : EventTypes.REJECT, false);
		this.setRequestEventId(request.getEvent().getEventId());
		this.setNumAccepts(request.getNumAccepts());
	}

	public int getNumAccepts() {
		return numAccepts;
	}

	public void setNumAccepts(int numAccepts) {
		this.numAccepts = numAccepts;
	}

	public String getRequestEventId() {
		return requestEventId;
	}

	public void setRequestEventId(String requestEventId) {
		this.requestEventId = requestEventId;
	}

}
