package hr.fer.zemris.opp.the7ups.malivinari.controllers;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import hr.fer.zemris.opp.the7ups.malivinari.models.Wine;
import hr.fer.zemris.opp.the7ups.malivinari.server.Session;
import hr.fer.zemris.opp.the7ups.malivinari.services.SessionService;
import hr.fer.zemris.opp.the7ups.malivinari.services.WineService;
import hr.fer.zemris.opp.the7ups.malivinari.utils.ResponseUtils;

@RestController
public class WineController {
	
	@Autowired
	private WineService wineService;
	
	@Autowired SessionService sessionService; 

	@RequestMapping(value = "/wines/{username}", method = RequestMethod.GET)
    public Response getWines(
    		@PathVariable(value = "username") String username,
    		@RequestParam(value = "token") String token
    		){
		
		List<Wine> wines = wineService.getWines(username);
		
		if (wines == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		Session session = sessionService.getSession(token);
		
		for (Wine wine: wines){
			boolean isPublic = !(session != null && wine.getOwner().equals(session.getUser()));
			ResponseUtils.wineResponse(wine, isPublic);
		}
		
		return new Response(wines, Status.OK);
	}
	
	@RequestMapping(value = "/add-wine", method = RequestMethod.POST)
    public Response addWine(
    		@RequestParam(value="name") String name,
    		@RequestParam(value="image") MultipartFile image,
    		@RequestParam(value = "token") String token
    		){
		
		byte[] imageBytes = null;
		if (image != null){
			try {
				imageBytes = image.getBytes();
			} catch (IOException e) {} // ignorable
		}
		
		Session session = sessionService.getSession(token);
		
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		
		Wine wine = wineService.addWine(name, session.getUser().getUsername(), imageBytes);
		return new Response(ResponseUtils.wineResponse(wine, false), Status.OK);
	}
	
	@RequestMapping(value = "/add-wine-no-img", method = RequestMethod.POST)
    public Response addWine(
    		@RequestParam(value="name") String name,
    		@RequestParam(value = "token") String token
    		){
		
		byte[] imageBytes = null;
		
		Session session = sessionService.getSession(token);
		
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		
		Wine wine = wineService.addWine(name, session.getUser().getUsername(), imageBytes);
		return new Response(ResponseUtils.wineResponse(wine, false), Status.OK);
	}
	
	@RequestMapping(value = "/wine/{id}", method = RequestMethod.GET)
    public Response getWine(
    		@PathVariable(value="id") int id,
    		@RequestParam(value = "token", defaultValue = "") String token
    		){
		
		boolean isPublic = true;
		Wine wine = wineService.getWine(id);
		
		if (wine == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		Session session = sessionService.getSession(token);
		
		if (session != null && session.getUser().equals(wine.getOwner())){
			isPublic = false;
		}
		
		if (session != null){
			session.see();
		}
		
		return new Response(ResponseUtils.wineResponse(wine, isPublic), Status.OK);
	}
	
	@RequestMapping(value = "/wine/{id}/image", method = RequestMethod.GET)
    public Response getWineImage(
    		@PathVariable(value="id") int id
    		) throws SQLException{
		
		Wine wine = wineService.getWine(id);
		
		if (wine == null || wine.getImage() == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		System.out.println(wine.getImage().getId());
		System.out.println(wine.getImage().getData());
		
		Blob blob = wine.getImage().getData();
		return new Response(blob.getBytes(1, (int) blob.length()), Status.OK);
	}
	
	@RequestMapping(value = "/update-wine", method = RequestMethod.POST)
    public Response updateWine(
    		@RequestParam(value="id") int id,
    		@RequestParam(value = "name", defaultValue = "") String name,
    		@RequestParam(value = "image", defaultValue = "") MultipartFile image,
    		@RequestParam(value = "token", defaultValue = "") String token
    		) throws SQLException, IOException{
		
		Session session = sessionService.getSession(token);
		Wine wine = wineService.getWine(id);
		
		if (wine == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		if (session == null || !session.getUser().equals(wine.getOwner())){
			return new Response(null, Status.FORBIDDEN);
		}
		
		if (!name.isEmpty()){
			wine.setName(name);
		}
		
		byte[] imageBytes = null;
		if (image != null){
			imageBytes = image.getBytes();
		}
		
		return new Response(ResponseUtils.wineResponse(wineService.updateWine(wine, imageBytes), false), Status.OK);
	}
	
}
