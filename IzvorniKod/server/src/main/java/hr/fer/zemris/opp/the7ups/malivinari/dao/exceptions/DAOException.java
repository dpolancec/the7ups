package hr.fer.zemris.opp.the7ups.malivinari.dao.exceptions;

import hr.fer.zemris.opp.the7ups.malivinari.dao.DAO;

public class DAOException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private DAO source;
	
	public DAOException(DAO source) {
		super();
		this.source = source;
	}

	public DAOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace,
			DAO source) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.source = source;
	}

	public DAOException(String message, Throwable cause, DAO source) {
		super(message, cause);
		this.source = source;
	}

	public DAOException(String message, DAO source) {
		super(message);
		this.source = source;
	}

	public DAOException(Throwable cause, DAO source) {
		super(cause);
		this.source = source;
	}

	@Override
	public String toString() {
		return super.toString() + " source DAO: " + source;
	}
	
	
}
