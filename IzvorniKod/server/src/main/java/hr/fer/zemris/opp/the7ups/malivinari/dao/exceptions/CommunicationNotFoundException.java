package hr.fer.zemris.opp.the7ups.malivinari.dao.exceptions;

import hr.fer.zemris.opp.the7ups.malivinari.dao.DAO;

public class CommunicationNotFoundException extends DAOException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CommunicationNotFoundException(DAO source) {
		super(source);
	}

	public CommunicationNotFoundException(String message, DAO source) {
		super(message, source);
	}

	public CommunicationNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace, DAO source) {
		super(message, cause, enableSuppression, writableStackTrace, source);
	}

	public CommunicationNotFoundException(String message, Throwable cause, DAO source) {
		super(message, cause, source);
	}

	public CommunicationNotFoundException(Throwable cause, DAO source) {
		super(cause, source);
	}

}
