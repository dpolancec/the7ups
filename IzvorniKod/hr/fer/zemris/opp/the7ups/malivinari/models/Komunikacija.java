package hr.fer.zemris.opp.the7ups.malivinari.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public abstract class Komunikacija {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private Date vrijemePocetka;
	private Date vrijemeKraja;
	private List<Poruka> poruke = new ArrayList<>();
	private Boolean aktivna;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getVrijemePocetka() {
		return vrijemePocetka;
	}
	public void setVrijemePocetka(Date vrijemePocetka) {
		this.vrijemePocetka = vrijemePocetka;
	}
	public Date getVrijemeKraja() {
		return vrijemeKraja;
	}
	public void setVrijemeKraja(Date vrijemeKraja) {
		this.vrijemeKraja = vrijemeKraja;
	}
	public List<Poruka> getPoruke() {
		return poruke;
	}
	public void setPoruke(List<Poruka> poruke) {
		this.poruke = poruke;
	}
	public Boolean getAktivna() {
		return aktivna;
	}
	public void setAktivna(Boolean aktivna) {
		this.aktivna = aktivna;
	}
	
}
