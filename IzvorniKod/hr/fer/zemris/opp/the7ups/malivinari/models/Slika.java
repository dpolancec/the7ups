package hr.fer.zemris.opp.the7ups.malivinari.models;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Slika {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private Blob podaci;
	private Date datumNastajanja;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Blob getPodaci() {
		return podaci;
	}
	public void setPodaci(Blob podaci) {
		this.podaci = podaci;
	}
	public Date getDatumNastajanja() {
		return datumNastajanja;
	}
	public void setDatumNastajanja(Date datumNastajanja) {
		this.datumNastajanja = datumNastajanja;
	}	
}
