package hr.fer.zemris.opp.the7ups.malivinari.models;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Vino {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private Korisnik vinar;
	private Slika slika;
	private String ime;
	private Date vrijemeDodavanja;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Korisnik getVinar() {
		return vinar;
	}
	public void setVinar(Korisnik vinar) {
		this.vinar = vinar;
	}
	public Slika getSlika() {
		return slika;
	}
	public void setSlika(Slika slika) {
		this.slika = slika;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public Date getVrijemeDodavanja() {
		return vrijemeDodavanja;
	}
	public void setVrijemeDodavanja(Date vrijemeDodavanja) {
		this.vrijemeDodavanja = vrijemeDodavanja;
	}
	
	
	
}
