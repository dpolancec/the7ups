package hr.fer.zemris.opp.the7ups.malivinari.models;

public class PrivatnaKomunikacija extends Komunikacija {

	private Korisnik prviKorisnik;
	private Korisnik drugiKorisnik;
	
	public Korisnik getPrviKorisnik() {
		return prviKorisnik;
	}
	public void setPrviKorisnik(Korisnik prviKorisnik) {
		this.prviKorisnik = prviKorisnik;
	}
	public Korisnik getDrugiKorisnik() {
		return drugiKorisnik;
	}
	public void setDrugiKorisnik(Korisnik drugiKorisnik) {
		this.drugiKorisnik = drugiKorisnik;
	}
	
	
}
