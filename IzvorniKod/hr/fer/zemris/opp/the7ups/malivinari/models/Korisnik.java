package hr.fer.zemris.opp.the7ups.malivinari.models;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Korisnik {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private String ime;
	private String prezime;
	private String korisnickoIme;
	private String rasprsenjeLozinke;
	private String email;
	private String status;
	private Date datumRegistracije;
	private Slika slikaProfila;
	private List<Korisnik> listaBlokiranih;
	private List<Grupa> listaGrupa;
	private List<Komunikacija> komunikacije;
	private List<Vino> listaVina;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getKorisnickoIme() {
		return korisnickoIme;
	}
	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRasprsenjeLozinke() {
		return rasprsenjeLozinke;
	}
	public void setRasprsenjeLozinke(String rasprsenjeLozinke) {
		this.rasprsenjeLozinke = rasprsenjeLozinke;
	}
	public Date getDatumRegistracije() {
		return datumRegistracije;
	}
	public void setDatumRegistracije(Date datumRegistracije) {
		this.datumRegistracije = datumRegistracije;
	}
	public Slika getSlikaProfila() {
		return slikaProfila;
	}
	public void setSlikaProfila(Slika slikaProfila) {
		this.slikaProfila = slikaProfila;
	}
	public List<Korisnik> getListaBlokiranih() {
		return listaBlokiranih;
	}
	public void setListaBlokiranih(List<Korisnik> listaBlokiranih) {
		this.listaBlokiranih = listaBlokiranih;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<Grupa> getListaGrupa() {
		return listaGrupa;
	}
	public void setListaGrupa(List<Grupa> listaGrupa) {
		this.listaGrupa = listaGrupa;
	}
	public List<Komunikacija> getKomunikacije() {
		return komunikacije;
	}
	public void setKomunikacije(List<Komunikacija> komunikacije) {
		this.komunikacije = komunikacije;
	}
	public List<Vino> getListaVina() {
		return listaVina;
	}
	public void setListaVina(List<Vino> listaVina) {
		this.listaVina = listaVina;
	}
	
	
	
}
