package hr.fer.zemris.opp.the7ups.malivinari.models;

public class GrupnaKomunikacija extends Komunikacija {

	private Grupa grupa;

	public Grupa getGrupa() {
		return grupa;
	}

	public void setGrupa(Grupa grupa) {
		this.grupa = grupa;
	}
}
