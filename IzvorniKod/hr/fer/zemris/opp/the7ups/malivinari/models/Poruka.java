package hr.fer.zemris.opp.the7ups.malivinari.models;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Poruka {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private String tekst;
	private Date datumStvaranja;
	private Komunikacija komunikacija;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTekst() {
		return tekst;
	}
	public void setTekst(String tekst) {
		this.tekst = tekst;
	}
	public Date getDatumStvaranja() {
		return datumStvaranja;
	}
	public void setDatumStvaranja(Date datumStvaranja) {
		this.datumStvaranja = datumStvaranja;
	}
	public Komunikacija getKomunikacija() {
		return komunikacija;
	}
	public void setKomunikacija(Komunikacija komunikacija) {
		this.komunikacija = komunikacija;
	}
}
