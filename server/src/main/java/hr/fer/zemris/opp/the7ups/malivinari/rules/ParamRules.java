package hr.fer.zemris.opp.the7ups.malivinari.rules;

public class ParamRules {
	
	public static final int MIN_USERNAME_SIZE = 6;
	public static final int MIN_PASS_SIZE = 8;
	public static final int MAX_USERNAME_SIZE = 24;
	public static final int MAX_PASS_SIZE = 32;
	
	private ParamRules() {}
	
	public static boolean checkUsername(String username){
		if (username != null &&username.length() >= MIN_USERNAME_SIZE && username.length() <= MAX_USERNAME_SIZE){
			return true;
		}
		
		return false;
	}
	
	public static boolean checkPassword(String password){
		if (password != null && password.length() >= MIN_PASS_SIZE && password.length() <= MAX_PASS_SIZE){
			return true;
		}
		
		return false;
	}
	
	public static boolean checkEmail(String email){
		if (email.contains("@")){
			return true;
		}
		
		return false;
	}
	
}
