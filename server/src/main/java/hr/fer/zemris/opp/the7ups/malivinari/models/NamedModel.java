package hr.fer.zemris.opp.the7ups.malivinari.models;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class NamedModel extends Model {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
