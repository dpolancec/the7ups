package hr.fer.zemris.opp.the7ups.malivinari.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.fer.zemris.opp.the7ups.malivinari.dao.jpa.HibernateDao;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.server.ServerContext;
import hr.fer.zemris.opp.the7ups.malivinari.server.Session;
import hr.fer.zemris.opp.the7ups.malivinari.utils.HashUtils;

@Service
@Transactional
public class SessionService {
	
	@Autowired
	public HibernateDao dao;
	
	@Autowired
	public ServerContext serverContext;

	public SessionService() {}

	public Session logIn(String username, String password){
		User user = dao.getUser(username);
		
		if (user == null || !HashUtils.checkHash(password, user.getPasswordHash()) || user.isDeleted()){
			return null;
		}
		
		return serverContext.createSession(user);
	}
	
	public void logOut(String token){
		serverContext.removeSession(getSession(token));
	}
	
	public Session getSession(String token){
		return serverContext.getAndSeeSession(token);
	}
	
	public boolean isLoggedIn(String username){
		return serverContext.isLoggedIn(username);
	}
	
	public boolean isBusy(String username){
		return serverContext.isBusy(username);
	}
	
	public int getSessionCount(){
		return serverContext.getSessionCount();
	}
	
	public int getMaxSessions(){
		return serverContext.getMaxSessions();
	}
	
	public void setBusy(String username, boolean busy){
		serverContext.setBusy(username, busy);
	}
	
}
