package hr.fer.zemris.opp.the7ups.malivinari.dao.exceptions;

import hr.fer.zemris.opp.the7ups.malivinari.dao.DAO;

public class GroupNotFoundException extends DAOException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GroupNotFoundException(DAO source) {
		super(source);
	}

	public GroupNotFoundException(String message, DAO source) {
		super(message, source);
	}

	public GroupNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace, DAO source) {
		super(message, cause, enableSuppression, writableStackTrace, source);
	}

	public GroupNotFoundException(String message, Throwable cause, DAO source) {
		super(message, cause, source);
	}

	public GroupNotFoundException(Throwable cause, DAO source) {
		super(cause, source);
	}
	
}
