package hr.fer.zemris.opp.the7ups.malivinari.server.events;

import java.util.Set;

public class UserInitiatedEvent extends ServerEvent {

	private String initiatorUsername;
	
	public UserInitiatedEvent(Set<String> relevantUsers, String type, boolean isRequest, String initiatorUsername) {
		super(relevantUsers, type, isRequest);
		this.initiatorUsername = initiatorUsername;
	}

	public String getInitiatorUsername() {
		return initiatorUsername;
	}

	public void setInitiatorUsername(String initiatorUsername) {
		this.initiatorUsername = initiatorUsername;
	}

}
