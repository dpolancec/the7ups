package hr.fer.zemris.opp.the7ups.malivinari.gui;

import java.util.Collection;

import hr.fer.zemris.opp.the7ups.malivinari.models.Model;
import hr.fer.zemris.opp.the7ups.malivinari.models.NamedModel;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;

public class Lister<T> extends TableView<T>{

	public Lister() {
		super();
	}
	
	public void setItems(Collection<T> items){
		if (items.isEmpty()){
			return;
		}
		
		ObservableList<T> itemList = FXCollections.observableArrayList(items);
		T object = itemList.get(0);
		if (object instanceof Model){
			if (object instanceof NamedModel){
				if (object instanceof User){
					itemList.sort((first, second) -> {
						User userFirst = (User) first;
						User userSecond = (User) second;
						
						return userFirst.getUsername().compareTo(userSecond.getUsername());
					});
				} else {
					itemList.sort((first, second) -> {
						NamedModel namedFirst = (NamedModel) first;
						NamedModel namedSecond = (NamedModel) second;
						
						return namedFirst.getName().compareTo(namedSecond.getName());
					});
				}
			} else {
				itemList.sort((first, second) -> {
					Model modelFirst = (Model) first;
					Model modelSecond = (Model) second;
					
					return -modelFirst.getTimeCreated().compareTo(modelSecond.getTimeCreated());
				});
			}
		}
		
		this.setItems(itemList);
	}

}
