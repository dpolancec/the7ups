package hr.fer.zemris.opp.the7ups.malivinari.server.events;

public class EventTypes {

	public static final String GROUP_REQUEST = "groupRequest";
	public static final String COMM_REQUEST = "commRequest";
	public static final String GROUP_DISBAND = "groupDisband";
	public static final String COMM_DISBAND = "commDisband";
	public static final String GROUP_DISBAND_REQUEST = "groupDisbandRequest";
	public static final String SERVER_SHUTDOWN_REQUEST = "serverShutdownRequest";
	public static final String REJECT = "reject";
	public static final String SUCCESS = "success";
	public static final String COMM_SHOW = "comm-show";
	public static final String COMM_APPEND = "comm-append";
	public static final String COMM_END = "comm-end";
	
}
