package hr.fer.zemris.opp.the7ups.malivinari.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.server.events.EventListener;
import hr.fer.zemris.opp.the7ups.malivinari.server.events.FinalizedRequestEvent;
import hr.fer.zemris.opp.the7ups.malivinari.server.events.Request;
import hr.fer.zemris.opp.the7ups.malivinari.server.events.ServerEvent;
import hr.fer.zemris.opp.the7ups.malivinari.services.EventService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
@PropertySource("classpath:session.properties")
public class ServerContext implements EnvironmentAware{
	
	@Autowired
	private EventService eventService;
	
	private Environment env;
	
	private BiMap<String, Session> sessions = HashBiMap.create();
	private int maxSessions;
	//private BiMap<Session, EventListener> eventListeners = HashBiMap.create();
	private Map<Session, List<ServerEvent>> pendingEvents = new HashMap<>();
	private Map<Session, Set<Request>> activeRequests = new HashMap<>();
	
	private final int DEFAULT_SESSION_LIFETIME_MILIS = 10_000;
	private final int TOKEN_COLLECTOR_SLEEP_TIME = 1_000;
	
	private int sessionLifetime = DEFAULT_SESSION_LIFETIME_MILIS;
	private final Thread tokenCollector = new Thread(() -> {
		while(true){
			try{
				Thread.sleep(TOKEN_COLLECTOR_SLEEP_TIME);
			}catch (Exception e){
				e.printStackTrace();
				continue;
			}
			
			checkSessions();
		}
	});
	
	{
		tokenCollector.setDaemon(true);
		tokenCollector.start();
	}
	
	public ServerContext(){
	}
	
	public synchronized int getSessionCount(){
		return sessions.keySet().size();
	}

	public synchronized Session createSession(User user){
		if (getSessionCount() >= maxSessions){
			return null;
		}
		Session session = initSession(user);
		sessions.put(user.getUsername(), session);
		pendingEvents.put(session, new LinkedList<>());
		activeRequests.put(session, new HashSet<>());
		return session;
	}
	
	public synchronized Session getAndSeeSession(String token){
		Session session = sessions.get(sessions.inverse().get(initDummySession(token)));
		if (session != null){
			session.see();
		}
		
		return session;
	}
	
	public synchronized void removeSession(Session session){
		sessions.remove(session.getUser().getUsername());
		pendingEvents.remove(session);
		for (Request request: activeRequests.remove(session)){
			eventService.finalizeRequest(request);
		}
	}
	
	
	
	public synchronized boolean isLoggedIn(String username){
		return sessions.get(username) != null;
	}
	
	public synchronized boolean isBusy(String username){
		return sessions.get(username).isBusy();
	}
	
	public synchronized void setBusy(String username, boolean busy){
		sessions.get(username).setBusy(busy);;
	}
	
	private Session initSession(User user){
		Session session = new Session(user);
		return session;
	}
	
	public Session initDummySession(String token){
		Session session = new Session(token);
		return session;
	}
	
	private final synchronized void checkSessions(){
		List<Session> removeList = new ArrayList<>();
		for (Session session: sessions.values()){
			if (new Date().getTime() - session.getLastSeen().getTime() > sessionLifetime){
				removeList.add(session);
			}
		}
		
		for (Session session: removeList){
			removeSession(session);
		}
	}

	public synchronized int getSessionLifetime() {
		return sessionLifetime;
	}

	public synchronized void setSessionLifetime(int sessionLifetime) {
		this.sessionLifetime = sessionLifetime;
	}
	
	public synchronized boolean publishEvent(ServerEvent event, boolean requireLoggedIn){
		if (event == null){
			return false;
		}
		
		for (String username: event.getRelevantUsers()){
			Session session = sessions.get(username);
			if (session == null && requireLoggedIn){
				return false;
			}
		}
		
		for (String username: event.getRelevantUsers()){
			Session session = sessions.get(username);
			if (session != null){
//				EventListener listener = eventListeners.get(session);
//				if (listener == null){
				List<ServerEvent> eventQueue = pendingEvents.get(session);
				eventQueue.add(event);
//				} else {
//					listener.setResult(event);
//				}
			}
		}
		
		return true;
	}
	
	public synchronized boolean publishRequest(Request request, boolean requireLoggedIn){
		if (request == null){
			return false;
		}
		
		for (String username: request.getEvent().getRelevantUsers()){
			Session otherSession = this.sessions.get(username);
			if (otherSession != null){
				activeRequests.get(otherSession).add(request);
			}
		}
		
		return publishEvent(request.getEvent(), requireLoggedIn);
	}
	
//	public synchronized void addListener(Session session, EventListener listener){
//		List<ServerEvent> eventQueue = pendingEvents.get(session);
//		if (!eventQueue.isEmpty()){
//			listener.setResult(eventQueue.remove(0));
//			return;
//		}
//		
//		eventListeners.put(session, listener);
//	}
//	
//	public synchronized void removeListener(EventListener listener){
//		eventListeners.inverse().remove(listener);
//	}

	@Override
	public void setEnvironment(Environment arg0) {
		this.env = arg0;
		maxSessions = Integer.parseInt(env.getProperty("session.max"));
	}

	public int getMaxSessions() {
		return maxSessions;
	}
	
	public synchronized List<ServerEvent> collectEvents(Session session){
		List<ServerEvent> events = pendingEvents.get(session);
		events.clear();
		return events;
	}

}
