package hr.fer.zemris.opp.the7ups.malivinari.gui;

import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class UserLister extends Lister<User> {

	@SuppressWarnings("unchecked")
	public UserLister(){
		super();
		
		TableColumn<User,String> usernameCol = new TableColumn<>("Username");
		usernameCol.setCellValueFactory(new PropertyValueFactory<>("username"));
		TableColumn<User,String> firstNameCol = new TableColumn<>("First Name");
		firstNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
		TableColumn<User,String> lastNameCol = new TableColumn<>("Last Name");
		lastNameCol.setCellValueFactory(new PropertyValueFactory<>("lastName"));
		
		this.setStyle("-fx-background-color: rgba(0, 0, 0, 0)");
		this.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		this.getColumns().setAll(usernameCol, firstNameCol, lastNameCol);
	}
	
}
