package hr.fer.zemris.opp.the7ups.malivinari.gui;

import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.opp.the7ups.malivinari.gui.skins.DefaultSkinProvider;

public class SkinContext {

	private static SkinProvider currentSkinProvider = new DefaultSkinProvider();
	private static List<Skinnable> skinnables = new ArrayList<>();
	
	public static synchronized void add(Skinnable skinnable){
		skinnables.add(skinnable);
	}
	
	public static synchronized void addAll(Skinnable ... skinnable){
		skinnables.addAll(skinnables);
	}
	
	public static synchronized SkinProvider getSkinProvider(){
		return currentSkinProvider;
	}
	
	public static synchronized void setSkinProvider(SkinProvider newProvider){
		currentSkinProvider = newProvider;
		for (Skinnable skinnable: skinnables){
			skinnable.notifyProviderChanged(newProvider);
		}
	}
	
}
