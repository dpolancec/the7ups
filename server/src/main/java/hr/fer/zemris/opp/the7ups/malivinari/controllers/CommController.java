package hr.fer.zemris.opp.the7ups.malivinari.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.zemris.opp.the7ups.malivinari.models.Communication;
import hr.fer.zemris.opp.the7ups.malivinari.models.Group;
import hr.fer.zemris.opp.the7ups.malivinari.models.Message;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.server.Session;
import hr.fer.zemris.opp.the7ups.malivinari.server.events.Request;
import hr.fer.zemris.opp.the7ups.malivinari.services.CommService;
import hr.fer.zemris.opp.the7ups.malivinari.services.EventService;
import hr.fer.zemris.opp.the7ups.malivinari.services.GroupService;
import hr.fer.zemris.opp.the7ups.malivinari.services.SessionService;
import hr.fer.zemris.opp.the7ups.malivinari.services.UserService;
import hr.fer.zemris.opp.the7ups.malivinari.utils.ResponseUtils;

@RestController
public class CommController {

	@Autowired
	private UserService userService;
	@Autowired
	private GroupService groupService;
	@Autowired
	private SessionService sessionService;
	@Autowired
	private CommService commService;
	@Autowired
	private EventService eventService;
	 
	@RequestMapping(value = "/comm/{id}", method = RequestMethod.GET)
    public Response getComm(
    		@PathVariable(value="id") int id,
    		@RequestParam(value="token", defaultValue="") String token
    		) {
		
		Communication comm = commService.getComm(id);
		System.out.println(comm);
		if (comm == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		List<User> participants = commService.getParticipants(id);
		Session session = sessionService.getSession(token);
		if (session == null || !participants.contains(session.getUser())){
			return new Response(null, Status.FORBIDDEN);
		}
		
		return new Response(ResponseUtils.commResponse(comm, participants, session.getUser()), Status.OK);
    }
	
	@RequestMapping(value = "/comms", method = RequestMethod.GET)
    public Response getComms(
    		@RequestParam(value="token", defaultValue="") String token
    		) {
		
		Session session = sessionService.getSession(token);
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		User user = session.getUser();
		List<Communication> comms = commService.getComms(user.getId());
		for (Communication comm: comms){
			List<User> participants = commService.getParticipants(comm.getId());
			ResponseUtils.commResponse(comm, participants, user);
		}
		
		return new Response(comms, Status.OK);
    }
	
	@RequestMapping(value = "/participants/{id}", method = RequestMethod.GET)
    public Response getParticipants(
    		@PathVariable(value="id") int id,
    		@RequestParam(value="token", defaultValue="") String token
    		) {
		
		Communication comm = commService.getComm(id);
		
		if (comm == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		Session session = sessionService.getSession(token);
		List<User> participants = commService.getParticipants(id);
		if (session == null || !participants.contains(session.getUser())){
			return new Response(null, Status.FORBIDDEN);
		}
		
		return new Response(ResponseUtils.userCollectionResponse(participants, session.getUser()), Status.OK);
    }
	
	@RequestMapping(value = "/messages/{id}", method = RequestMethod.GET)
    public Response getMessages(
    		@PathVariable(value="id") int id,
    		@RequestParam(value="token", defaultValue="") String token
    		) {
		
		Communication comm = commService.getComm(id);
		
		if (comm == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		List<User> participants = commService.getParticipants(id);
		Session session = sessionService.getSession(token);
		if (session == null || !participants.contains(session.getUser())){
			return new Response(null, Status.FORBIDDEN);
		}
		

		List<Message> messages = commService.getMessages(comm.getId());
		for (Message message: messages){
			message.setCommunication(null);
		}
		
		return new Response(messages, Status.OK);
    }
	
	@RequestMapping(value = "/private-comm-request-create", method = RequestMethod.POST)
    public Response requestCreatePrivateComm(
    		@RequestParam(value="user", defaultValue="") String user,
    		@RequestParam(value="token", defaultValue="") String token
    		) {
		
		System.out.println("POST /private-comm-request-create");
		
		User initiator = null;
		Session session = sessionService.getSession(token);
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		initiator = session.getUser();
		
		User invitedUser = userService.getUser(user);
		if (invitedUser == null){
			return new Response(null, Status.NOT_FOUND);
		}
		Set<User> blockedUsers = userService.getBlockedUsers(invitedUser);
		Set<User> myBlockedUsers = userService.getBlockedUsers(initiator);
		
		if (myBlockedUsers.contains(invitedUser) || blockedUsers.contains(initiator) || 
				!sessionService.isLoggedIn(invitedUser.getUsername())){
			return new Response(null, Status.NOT_FOUND);
		}
		
		if (sessionService.isBusy(user)){
			return new Response(null, Status.BUSY);
		}
		
		Request request = commService.createCommCreateRequest(initiator, invitedUser);
		
		if (eventService.publishRequest(request, true)){
			return new Response(request, Status.OK);
		}
		
		return new Response(null, Status.NOT_FOUND);
    }
	
	@RequestMapping(value = "/comm-group-request-create", method = RequestMethod.POST)
    public Response requestCreateGroupComm(
    		@RequestParam(value="id", defaultValue="") int groupId,
    		@RequestParam(value="token", defaultValue="") String token
    		) {
		
		System.out.println("POST /comm-group-request-create");
		
		User initiator = null;
		Session session = sessionService.getSession(token);
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		initiator = session.getUser();
		
		Group group = groupService.getGroup(groupId);
		if (group == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		Set<User> members = new HashSet<>(groupService.getGroupMembers(groupId));
		if (!members.contains(initiator)){
			return new Response(null, Status.FORBIDDEN);
		}
		System.out.println(members);
		Request request = commService.createCommCreateRequest(initiator, group.getName(), members);
		
		if (eventService.publishRequest(request, true)){
			return new Response(request, Status.OK);
		}
		
		return new Response(null, Status.NOT_FOUND);
    }
	
	@RequestMapping(value = "/comm-leave", method = RequestMethod.POST)
    public Response leaveComm(
    		@RequestParam(value="id") int id,
    		@RequestParam(value="token") String token
    		) {
		
		Communication comm = commService.getComm(id);
		
		if (comm == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		Session session = sessionService.getSession(token);
		if (session == null || !commService.getParticipants(id).contains(session.getUser())){
			return new Response(null, Status.FORBIDDEN);
		}
		
		commService.endCommunication(id);
		return new Response(null, Status.OK);
    }
	
	@RequestMapping(value = "/comm-append", method = RequestMethod.POST)
    public Response sendMessage(
    		@RequestParam(value="id") int id,
    		@RequestParam(value="token") String token,
    		@RequestParam(value="text") String text
    		) {
		
		Communication comm = commService.getComm(id);
		System.out.println(comm);
		if (comm == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		Session session = sessionService.getSession(token);
		if (session == null || !commService.getParticipants(id).contains(session.getUser())){
			return new Response(null, Status.FORBIDDEN);
		}
		System.out.println("About to append");
		
		commService.appendCommunication(id, session.getUser().getUsername(), text);
		return new Response(null, Status.OK);
    }
	
}
