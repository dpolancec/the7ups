package hr.fer.zemris.opp.the7ups.malivinari.utils;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import hr.fer.zemris.opp.the7ups.malivinari.models.Communication;
import hr.fer.zemris.opp.the7ups.malivinari.models.Group;
import hr.fer.zemris.opp.the7ups.malivinari.models.Image;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.models.Wine;

public class ResponseUtils {

	private ResponseUtils() {}

	public static User publicResponse(User user){
		user = privateResponse(user);
		user.setEmail(null);
		user.setTimeUpdated(null);
		
		return user;
	}
	
	public static User privateResponse(User user){
		user.setBlockedUsers(null);
		user.setCommunications(null);
		user.setGroups(null);
		user.setWines(null);
		user.setPasswordHash(null);
		
		Image image = user.getImage();
		if (image != null){
			image.setData(null);
			image.setOwner(null);
		}
		
		return user;
	}
	
	public static User userResponse(User user, boolean isPublic){
		if (isPublic){
			return publicResponse(user);
		} else {
			return privateResponse(user);
		}
	}
	
	public static Collection<User> userCollectionResponse (Collection<User> users, User loggedInUser){
		for (User user: users){
			if (user.equals(loggedInUser)){
				privateResponse(user);
			} else {
				publicResponse(user);
			}
		}
		
		return users;
	}
	
	public static Wine wineResponse(Wine wine, boolean isPublic){
		if (isPublic){
			publicResponse(wine.getOwner());
		} else {
			privateResponse(wine.getOwner());
		}
		
		Image image = wine.getImage();
		
		if (image != null){
			image.setData(null);
		}
		
		return wine;
	}
	
	public static Group groupResponse(Group group, Set<User> members, User loggedInUser){
		group.setMembers(members);
		userCollectionResponse(group.getMembers(), loggedInUser);
		
		return group;
	}

	public static Communication commResponse(Communication comm, List<User> participants, User user) {
		comm.setParticipants(new HashSet<>(participants));
		userCollectionResponse(comm.getParticipants(), user);
		comm.setMessages(null);
		
		return comm;
	}
}
