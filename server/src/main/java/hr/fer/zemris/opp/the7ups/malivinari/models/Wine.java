package hr.fer.zemris.opp.the7ups.malivinari.models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="wines")
public class Wine extends NamedModel{
	
	@ManyToOne(fetch=FetchType.EAGER)
	private User owner;
	@OneToOne(fetch=FetchType.EAGER)
	private Image image;
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public Image getImage() {
		return image;
	}
	public void setImage(Image image) {
		this.image = image;
	}
	
}
