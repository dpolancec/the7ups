package hr.fer.zemris.opp.the7ups.malivinari.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="messages")
public class Message extends Model{

	@Column(nullable = false)
	private String body;
	@ManyToOne(fetch=FetchType.EAGER)
	private Communication communication;
	
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public Communication getCommunication() {
		return communication;
	}
	public void setCommunication(Communication communication) {
		this.communication = communication;
	}
	
}
