package hr.fer.zemris.opp.the7ups.malivinari.gui;

import java.io.PrintWriter;
import java.io.StringWriter;

import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public class GuiUtils {
	
	public static Alert exceptionAlert(String title, String header, String content, Exception e){
		StringWriter writer = new StringWriter();
		e.printStackTrace(new PrintWriter(writer));
		
		TextArea textArea = new TextArea(writer.toString());
		textArea.setEditable(false);
		textArea.setWrapText(true);
		
		Label label = new Label("Show this info to the developer:");
		
		GridPane.setVgrow(textArea, Priority.ALWAYS); // RIP Alan Rickman
		GridPane.setHgrow(textArea, Priority.ALWAYS);

		GridPane expContent = new GridPane();
		expContent.setMaxWidth(Double.MAX_VALUE);
		expContent.add(label, 0, 0);
		expContent.add(textArea, 0, 1);
		
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.getDialogPane().setExpandableContent(expContent);
		
		return alert;
	}
	
	public static Alert confirmAlert(String title, String header, String content){
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle(title);
		alert.setContentText(header);
		return alert;
	}
	
}
