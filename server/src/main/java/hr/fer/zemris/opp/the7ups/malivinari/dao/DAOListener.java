package hr.fer.zemris.opp.the7ups.malivinari.dao;

public interface DAOListener {
	
	void notify(DAOEvent event, DAO source);
}
