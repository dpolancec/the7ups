package hr.fer.zemris.opp.the7ups.malivinari.server.events;

import java.util.Set;

public class FinalizedRequestEvent extends ServerEvent {

	private String requestEventId;
	private int numAccepts;
	private String finalizedRequestInitiator;
	
	public FinalizedRequestEvent(Set<String> relevantUsers, Request request) {
		super(relevantUsers, request.isAccepted() ?  EventTypes.SUCCESS : EventTypes.REJECT, false);
		this.setRequestEventId(request.getEvent().getEventId());
		this.setNumAccepts(request.getNumAccepts());
		ServerEvent event = request.getEvent();
		if (event instanceof UserInitiatedEvent){
			this.finalizedRequestInitiator = ((UserInitiatedEvent) event).getInitiatorUsername();
		}
		
	}

	public int getNumAccepts() {
		return numAccepts;
	}

	public void setNumAccepts(int numAccepts) {
		this.numAccepts = numAccepts;
	}

	public String getRequestEventId() {
		return requestEventId;
	}

	public void setRequestEventId(String requestEventId) {
		this.requestEventId = requestEventId;
	}

	public String getFinalizedRequestInitiator() {
		return finalizedRequestInitiator;
	}

	public void setFinalizedRequestInitiator(String finalizedRequestInitiator) {
		this.finalizedRequestInitiator = finalizedRequestInitiator;
	}

}
