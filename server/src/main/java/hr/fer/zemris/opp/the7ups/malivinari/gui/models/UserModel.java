package hr.fer.zemris.opp.the7ups.malivinari.gui.models;

import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class UserModel {

	public StringProperty username;
	public StringProperty name;
	public StringProperty lastName;
	private User prototype;
	
	public UserModel(){}
	
	public UserModel(User prototype) {
		super();
		this.prototype = prototype;
		reset();
	}
	public String getUsername() {
		return username.get();
	}
	public User getPrototype() {
		return prototype;
	}
	public String getName() {
		return name.get();
	}
	public void setName(String name) {
		this.name.set(name);
	}
	public String getLastName() {
		return lastName.get();
	}
	public void setLastName(String lastName) {
		this.lastName.set(lastName);
	}

	public void reset(){
		this.username = new SimpleStringProperty(prototype.getUsername());
		this.name = new SimpleStringProperty(prototype.getName());
		this.lastName = new SimpleStringProperty(prototype.getLastName());
	}
	
}
