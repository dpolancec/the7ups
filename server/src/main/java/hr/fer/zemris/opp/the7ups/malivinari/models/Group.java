package hr.fer.zemris.opp.the7ups.malivinari.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="groups")
public class Group extends NamedModel{

	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(
			   name = "group_members", 
			   joinColumns = @JoinColumn(name = "group_id"), 
			   inverseJoinColumns = @JoinColumn(name = "user_id")
			 )
	private Set<User> members = new HashSet<User>();
//	@OneToMany(mappedBy = "group", fetch = FetchType.LAZY)
//	private Set<Communication> communications = new HashSet<Communication>();
	
	public Set<User> getMembers() {
		return members;
	}
	public void setMembers(Set<User> members) {
		this.members = members;
	}
	
}
