package hr.fer.zemris.opp.the7ups.malivinari.models;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="communications")
@NamedQueries({
	@NamedQuery(name=Communication.Q_ALL,query="select communication from Communication as communication")
})
public class Communication extends Model{
	
	public static final String Q_ALL = "Communication.findAll";

	@OneToMany(mappedBy="communication", fetch=FetchType.LAZY, cascade = {CascadeType.ALL})
	private Set<Message> messages = new HashSet<Message>();
	@Temporal(TemporalType.TIMESTAMP)
	private Date timeEnded;
	@Enumerated(EnumType.STRING)
	private CommunicationType type;
	private String groupName;
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(
			   name = "communication_participants", 
			   joinColumns = @JoinColumn(name = "communication_id"), 
			   inverseJoinColumns = @JoinColumn(name = "participant_id")
			 )
	private Set<User> participants = new HashSet<User>();
	
	public Set<Message> getMessages() {
		return messages;
	}
	
	public void setMessages(Set<Message> messages) {
		this.messages = messages;
	}
	
	public Date getTimeEnded() {
		return timeEnded;
	}
	
	public void setTimeEnded(Date timeEnded) {
		this.timeEnded = timeEnded;
	}
	
	public CommunicationType getType() {
		return type;
	}
	
	public void setType(CommunicationType type) {
		this.type = type;
	}

	public Set<User> getParticipants() {
		return participants;
	}

	public void setParticipants(Set<User> participants) {
		this.participants = participants;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
}
