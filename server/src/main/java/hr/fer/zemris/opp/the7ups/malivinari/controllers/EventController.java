package hr.fer.zemris.opp.the7ups.malivinari.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import hr.fer.zemris.opp.the7ups.malivinari.server.Session;
import hr.fer.zemris.opp.the7ups.malivinari.server.events.Request;
import hr.fer.zemris.opp.the7ups.malivinari.server.events.ServerEvent;
import hr.fer.zemris.opp.the7ups.malivinari.services.EventService;
import hr.fer.zemris.opp.the7ups.malivinari.services.SessionService;

@RestController
public class EventController {

	@Autowired
	private SessionService sessionService;
	@Autowired
	private EventService eventService;
	
	@RequestMapping(value = "/collect", method = RequestMethod.POST)
    public Object collect(
    		@RequestParam(value="token", defaultValue = "") String token
    		) {
		
		Session session = sessionService.getSession(token);
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		
		List<ServerEvent> events = eventService.collectEvents(session.getUser().getUsername());
		if (events == null || events.isEmpty()){
			return new Response(null, Status.NOT_CHANGED);
		}
		
		ServerEvent event = events.remove(0);
		return event;
    }
	
	@RequestMapping(value = "/pingMe", method = RequestMethod.POST)
    public Response pingMe(
    		@RequestParam(value="token") String token
    		) {
		
		Session session = sessionService.getSession(token);
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		Set<String> myUsername = new HashSet<>();
		myUsername.add(session.getUser().getUsername());
		ServerEvent pingEvent = new ServerEvent(myUsername, "SELF-PING", false);
		if (eventService.publishEvent(pingEvent, false)){
			return new Response(pingEvent.getEventId(), Status.OK);
		}
		
		return new Response(null, Status.ERR);
    }
	
	@RequestMapping(value = "/accept", method = RequestMethod.POST)
    public Response accept(
    		@RequestParam(value="token") String token,
    		@RequestParam(value="id") String id
    		) {
		
		Session session = sessionService.getSession(token);
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		
		Request request = eventService.acceptRequest(session.getUser().getUsername(), id);
		if (request != null){
			return new Response(request, Status.OK);
		}
		
		return new Response(null, Status.NOT_FOUND);
    }
	
	@RequestMapping(value = "/decline", method = RequestMethod.POST)
    public Response decline(
    		@RequestParam(value="token") String token,
    		@RequestParam(value="id") String id
    		) {
		
		Session session = sessionService.getSession(token);
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		
		Request request = eventService.declineRequest(session.getUser().getUsername(), id);
		if (request != null){
			return new Response(request, Status.OK);
		}
		
		return new Response(null, Status.NOT_FOUND);
    }
	
	@RequestMapping(value = "/listen", method = RequestMethod.POST)
    public DeferredResult<ServerEvent> listen(
    		@RequestParam(value="token") String token
    		) {
		
		Session session = sessionService.getSession(token);
		if (session == null){
			throw new ForbiddenException();
		}
		
		DeferredResult<ServerEvent> result = new DeferredResult<>();
		ServerEvent event = eventService.collectEvent(session.getUser().getUsername());
		if (event != null){
			result.setResult(event);
			return result;
		}
		result.onCompletion(() -> {
			eventService.unregister(session.getUser().getUsername());
		});
		eventService.register(session.getUser().getUsername(), result);
		
		return result;
    }
	
}
