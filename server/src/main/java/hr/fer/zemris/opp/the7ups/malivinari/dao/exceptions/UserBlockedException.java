package hr.fer.zemris.opp.the7ups.malivinari.dao.exceptions;

import hr.fer.zemris.opp.the7ups.malivinari.dao.DAO;

public class UserBlockedException extends DAOException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserBlockedException(DAO source) {
		super(source);
	}

	public UserBlockedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace,
			DAO source) {
		super(message, cause, enableSuppression, writableStackTrace, source);
	}

	public UserBlockedException(String message, Throwable cause, DAO source) {
		super(message, cause, source);
	}

	public UserBlockedException(String message, DAO source) {
		super(message, source);
	}

	public UserBlockedException(Throwable cause, DAO source) {
		super(cause, source);
	}

}
