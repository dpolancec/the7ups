package hr.fer.zemris.opp.the7ups.malivinari.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import hr.fer.zemris.opp.the7ups.malivinari.ServerApp;
import hr.fer.zemris.opp.the7ups.malivinari.gui.ContentBackgroundPane;
import hr.fer.zemris.opp.the7ups.malivinari.gui.GuiConstants;
import hr.fer.zemris.opp.the7ups.malivinari.gui.GuiUtils;
import hr.fer.zemris.opp.the7ups.malivinari.gui.SkinnableLabel;
import hr.fer.zemris.opp.the7ups.malivinari.services.SessionService;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

@Component
public class ServerControlPane extends ContentBackgroundPane {

	private BorderPane mainPane = new BorderPane();
	private GridPane inputPane = new GridPane();
	private BorderPane portInputPane = new BorderPane();
	private BorderPane maxSessionInputPane = new BorderPane();
	private ApplicationContext serverAppContext;
	
	private SkinnableLabel setPortLabel = new SkinnableLabel("Set port:");
	private SkinnableLabel setMaxSessionLabel = new SkinnableLabel("Max. sessions:");
	
	private TextField portTextField = new TextField();
	private TextField maxSessionTextField = new TextField();
	
	private Button startStopButton = new Button("Start server");
	
	private GridPane serverInfoPane = new GridPane();
	private SkinnableLabel serverStatusLabel = new SkinnableLabel("Server offline");
	private SkinnableLabel sessionNumberLabel = new SkinnableLabel("");
	
	private static final double PREF_BUTTON_WIDTH = 150;
	private static final double PREF_BUTTON_HEIGHT = 30;
	
	private static final double MAIN_PANE_WIDTH = 220;
	private static final double MAIN_PANE_HEIGHT = 150;

	protected static final long SESSION_REFRESH_TIME = 1000;
	
	private static final double V_GAP = 5;
	private static final double H_GAP = 5;
	
	public ServerControlPane(){
		portInputPane.setLeft(setPortLabel);
		portInputPane.setRight(portTextField);
		
		maxSessionInputPane.setLeft(setMaxSessionLabel);
		maxSessionInputPane.setRight(maxSessionTextField);
		
		GridPane.setRowIndex(portInputPane, 1);
		GridPane.setRowIndex(maxSessionInputPane, 2);
		
		inputPane.getChildren().addAll(portInputPane, maxSessionInputPane);
		startStopButton.setPrefSize(PREF_BUTTON_WIDTH, PREF_BUTTON_HEIGHT);
		
		startStopButton.setOnAction(new EventHandler<ActionEvent>() {
			 
            @Override
            public void handle(ActionEvent event) {
            	if (serverAppContext == null){
            		startServer();
            	} else {
            		stopServer();
            	}
            }

			private void startServer() {
				startStopButton.setDisable(true);
            	startStopButton.setText("Starting server...");
            	
            	final int serverPort;
            	try{
            		String portInputText = portTextField.getText();
            		serverPort = Integer.parseInt(portInputText);
            	} catch (NumberFormatException e){
            		Alert alert = new Alert(AlertType.ERROR);
            		alert.setTitle("Error");
            		alert.setHeaderText("Number format error");
            		alert.setContentText("Port value must be a number!");
            		alert.showAndWait();
            		startStopButton.setDisable(false);
            		startStopButton.setText("Start server");
            		return;
            	}
            	
            	final int maxSessions;
            	try{
            		String sessionInputText = maxSessionTextField.getText();
            		maxSessions = Integer.parseInt(sessionInputText);
            	} catch (NumberFormatException e){
            		Alert alert = new Alert(AlertType.ERROR);
            		alert.setTitle("Error");
            		alert.setHeaderText("Number format error");
            		alert.setContentText("Max. sessions value must be a number!");
            		alert.showAndWait();
            		startStopButton.setDisable(false);
            		startStopButton.setText("Start server");
            		return;
            	}
            	
            	new Thread(() -> {
            		try{
                		serverAppContext = SpringApplication.run(ServerApp.class, 
                				"--server.port=" + serverPort, 
                				"--session.max=" + maxSessions);
                		Platform.runLater(() -> {
                			startStopButton.setDisable(false);
                			startStopButton.setText("Stop server");
                			serverStatusLabel.setText("Server running on port: " + serverPort);
                		});
                		
                		new Thread(() -> {
            				while(serverAppContext != null){
            					Platform.runLater(() -> {
            						try{
            							SessionService service = serverAppContext.getBean(SessionService.class);
                						sessionNumberLabel.setText("Active sessions: " 
                	            				+ service.getSessionCount() 
                	            				+ "/" + maxSessions);
            						} catch (NullPointerException e){} // ignorable
            					});
            					try {
									Thread.sleep(SESSION_REFRESH_TIME);
								} catch (Exception e) {
									e.printStackTrace();
								}
            				}
            			}).start();
                	} catch (Exception e) {
                		
                		Platform.runLater(() -> {
                    		GuiUtils.exceptionAlert("Error", "Server startup error", 
                    				"An error occurred while starting the server", e).showAndWait();
                			startStopButton.setDisable(false);
                			startStopButton.setText("Start server");
                		});
                		return;
                	}
            	}).start();
			}
        });
		
		BorderPane controlPane = new BorderPane();
		controlPane.setTop(inputPane);
		controlPane.setBottom(startStopButton);
		
		BorderPane.setAlignment(startStopButton, Pos.CENTER);
		BorderPane.setMargin(maxSessionTextField, new Insets(0, 0, 0, GuiConstants.DEFAULT_H_GAP));
		BorderPane.setMargin(setMaxSessionLabel, new Insets(0, GuiConstants.DEFAULT_H_GAP, 0, 0));
		BorderPane.setMargin(startStopButton, new Insets(GuiConstants.DEFAULT_V_GAP, 0, 0, 0));
		BorderPane.setMargin(inputPane, new Insets(GuiConstants.DEFAULT_V_GAP, GuiConstants.DEFAULT_H_GAP, 
				GuiConstants.DEFAULT_V_GAP, GuiConstants.DEFAULT_H_GAP));
		
		serverInfoPane.add(serverStatusLabel, 0, 0);
		serverInfoPane.add(sessionNumberLabel, 0, 1);
		serverInfoPane.getStyleClass().add("serverInfoPane");
		
		inputPane.setVgap(V_GAP);
		inputPane.setHgap(H_GAP);
		
		mainPane.setMaxHeight(MAIN_PANE_HEIGHT);
		mainPane.setMaxWidth(MAIN_PANE_WIDTH);
		
		mainPane.setTop(controlPane);
		mainPane.setBottom(serverInfoPane);
		
		this.addContent(mainPane);
	}

	public ApplicationContext getServerAppContext() {
		return serverAppContext;
	}
	
	public void stopServer() {
		/*
		 * It takes an amount of time comparable to the session counting thread sleep time
		 * to exit the application, so serverAppContext should be copied and immediately nulled
		 * to minimise the console output pollution generated by frequent NPE stack traces when getting
		 * beans from serverAppContext. This operation is non-atomic, however, the probability
		 * of an NPE after this procedure is overwhelmingly small. NPEs generated this way
		 * should not be harmful to the rest of the application. Are they?
		 */
		ApplicationContext toExit = serverAppContext;
		serverAppContext = null;
		SpringApplication.exit(toExit);
		startStopButton.setText("Start server");
		serverStatusLabel.setText("Server offline");
		sessionNumberLabel.setText("");
	}
	
}
