package hr.fer.zemris.opp.the7ups.malivinari.controllers;

public class Response {

	public int status;
	public Object data;
	
	public Response(Object data, int status) {
		super();
		this.status = status;
		this.data = data;
	}
	
}
