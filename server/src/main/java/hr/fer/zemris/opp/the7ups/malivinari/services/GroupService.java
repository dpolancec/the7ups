package hr.fer.zemris.opp.the7ups.malivinari.services;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.fer.zemris.opp.the7ups.malivinari.dao.jpa.HibernateDao;
import hr.fer.zemris.opp.the7ups.malivinari.models.Group;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.server.events.EventTypes;
import hr.fer.zemris.opp.the7ups.malivinari.server.events.Request;
import hr.fer.zemris.opp.the7ups.malivinari.server.events.UserInitiatedEvent;
import hr.fer.zemris.opp.the7ups.malivinari.utils.ExtractUtils;

@Service
@Transactional
public class GroupService {

	@Autowired
	private HibernateDao dao;
	
	public Request createGroupCreateRequest(Set<User> invitedUsers, String groupName, User initiator){
		Request request = new Request(new UserInitiatedEvent(ExtractUtils.extractUsernames(invitedUsers),
				EventTypes.GROUP_REQUEST, true, initiator.getUsername()), () -> {
			Group group = new Group();
			group.setName(groupName);
			
			int groupId = dao.addGroup(group);
			for (User user: invitedUsers){
				dao.joinGroup(groupId, user.getId());
			}
		});
		
		return request;
	}
	
	public Group getGroup(int groupId){
		return dao.getGroup(groupId);
	}
	
	public Request createGroupDisbandRequest(int groupId, User initiator){
		Group group = dao.getGroup(groupId);
		if (group == null || !group.getMembers().contains(initiator)){
			return null;
		}
		
		Request request = new Request(new UserInitiatedEvent(ExtractUtils.extractUsernames(dao.getMembers(groupId)),
				EventTypes.GROUP_DISBAND_REQUEST, true, initiator.getUsername()), () -> {
			dao.removeGroup(groupId);
		});
		
		return request;
	}
	
	public Set<User> getGroupMembers(int groupId){
		Group group = dao.getGroup(groupId);
		if (group == null){
			return null;
		}
		
		return dao.getMembers(groupId);
	}
	
	public Set<Group> getGroups(String username){
		User user = dao.getUser(username);
		if (user == null){
			return null;
		}
		
		Set<Group> groups = dao.getGroups(user.getId());
		groups.size(); // trigger init
		return groups;
	}
	
	public boolean leaveGroup(int groupId, String username){
		User user = dao.getUser(username);
		if (user == null){
			return false;
		}
		
		Group group = getGroup(groupId);
		if (group == null){
			return false;
		}
		
		Set<User> members = group.getMembers();
		if (!members.contains(user)){
			return false;
		}
		
		dao.leaveGroup(groupId, user.getId());
		return true;
	}
	
}
