package hr.fer.zemris.opp.the7ups.malivinari.dao.exceptions;

import hr.fer.zemris.opp.the7ups.malivinari.dao.DAO;

public class UserNotFoundException extends DAOException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserNotFoundException(DAO source) {
		super(source);
	}

	public UserNotFoundException(String message, DAO source) {
		super(message, source);
	}

	public UserNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace,
			DAO source) {
		super(message, cause, enableSuppression, writableStackTrace, source);
	}

	public UserNotFoundException(String message, Throwable cause, DAO source) {
		super(message, cause, source);
	}

	public UserNotFoundException(Throwable cause, DAO source) {
		super(cause, source);
	}
	
}
