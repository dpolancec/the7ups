package hr.fer.zemris.opp.the7ups.malivinari.models;

// to be removed
public class PrivateCommunication extends Communication {

	private User prviKorisnik;
	private User drugiKorisnik;
	
	public User getPrviKorisnik() {
		return prviKorisnik;
	}
	public void setPrviKorisnik(User prviKorisnik) {
		this.prviKorisnik = prviKorisnik;
	}
	public User getDrugiKorisnik() {
		return drugiKorisnik;
	}
	public void setDrugiKorisnik(User drugiKorisnik) {
		this.drugiKorisnik = drugiKorisnik;
	}
	
}
