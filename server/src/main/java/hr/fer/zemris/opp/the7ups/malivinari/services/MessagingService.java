package hr.fer.zemris.opp.the7ups.malivinari.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.zemris.opp.the7ups.malivinari.dao.jpa.HibernateDao;
import hr.fer.zemris.opp.the7ups.malivinari.models.Communication;
import hr.fer.zemris.opp.the7ups.malivinari.models.CommunicationType;
import hr.fer.zemris.opp.the7ups.malivinari.models.Message;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.server.Session;

@Service
public class MessagingService {

	@Autowired
	private HibernateDao dao;
	
	@Autowired
	private SessionService sessionService;
	
//	public Communication sendMessage(Session session, User recipient, String message){
//		User sender = session.getUser();
//		if (sessionService.isLoggedId(recipient.getUsername())){
//			return openComm(sender, recipient, message);
//		} else {
//			return sendMail(sender, recipient);
//		}
//		
//	}
	
//	public Communication sendMessage(Session session, User recipient, String text, int commId){
//		Message message = new Message();
//		message.setBody(session.getUser().getUsername() + ":" + text);
//
//		dao.appendCommunication(commId, message);
//	}
//
//	private Communication openComm(User sender, User recipient) {
//		Communication communication = new Communication();
//		Set<User> participants = new HashSet<>();
//		participants.add(recipient);
//		participants.add(sender);
//		communication.setParticipants(participants);
//	}
}
