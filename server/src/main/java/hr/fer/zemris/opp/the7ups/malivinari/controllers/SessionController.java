package hr.fer.zemris.opp.the7ups.malivinari.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.server.Session;
import hr.fer.zemris.opp.the7ups.malivinari.services.SessionService;
import hr.fer.zemris.opp.the7ups.malivinari.services.UserService;
import hr.fer.zemris.opp.the7ups.malivinari.utils.ResponseUtils;

@RestController
public class SessionController {
	
	@Autowired
	private SessionService service;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
    public Response login(
    		@RequestParam(value="username") String username,
    		@RequestParam(value="password") String password
    		) {
		
		if (service.getSessionCount() >= service.getMaxSessions()){
			return new Response(null, Status.TOO_MANY_LOGGED_ID);
		}
		
		Session session = service.logIn(username, password);
		
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		
		return new Response(session.getToken(), Status.OK);
    }
	
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
    public Response logout(
    		@RequestParam(value="token") String token
    		) {
		
		Session session = service.getSession(token);
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		
		service.logOut(token);
		return new Response(null, Status.OK);
    }
	
	@RequestMapping(value = "/me", method = RequestMethod.GET)
    public Response me(
    		@RequestParam(value="token") String token
    		) {
		
		Session session = service.getSession(token);
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		
		return new Response(ResponseUtils.privateResponse(session.getUser()), Status.OK);
    }
	
	@RequestMapping(value = "/is-logged-in/{username}", method = RequestMethod.GET)
    public Response isLoggedIn(
    		@PathVariable(value="username") String username,
    		@RequestParam(value="token", defaultValue="") String token
    		) {
		
		User user = userService.getUser(username);
		if (user == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		Session session = service.getSession(token);
		if (session == null){
			return new Response(null, Status.FORBIDDEN);
		}
		
		if (userService.getBlockedUsers(user).contains(session.getUser())){
			return new Response(false, Status.OK);
		}
		
		return new Response(service.isLoggedIn(username), Status.OK);
    }
	
	@RequestMapping(value = "/is-busy/{username}", method = RequestMethod.GET)
    public Response isBusy(
    		@PathVariable(value="username") String username
    		) {
		
		User user = userService.getUser(username);
		if (user == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		return new Response(service.isBusy(username), Status.OK);
    }
	
	@RequestMapping(value = "/see", method = RequestMethod.GET)
    public Response see(
    		@RequestParam(value="token", defaultValue = "") String token
    		) {
		Session session = service.getSession(token);
		if (session == null){
			return new Response(null, Status.NOT_FOUND);
		}
		
		return new Response(null, Status.OK);
    }

}
