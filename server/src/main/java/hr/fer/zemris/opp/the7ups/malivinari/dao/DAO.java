package hr.fer.zemris.opp.the7ups.malivinari.dao;

import java.util.List;
import java.util.Set;

import hr.fer.zemris.opp.the7ups.malivinari.dao.exceptions.CommunicationNotFoundException;
import hr.fer.zemris.opp.the7ups.malivinari.dao.exceptions.DAOException;
import hr.fer.zemris.opp.the7ups.malivinari.dao.exceptions.GroupNotFoundException;
import hr.fer.zemris.opp.the7ups.malivinari.dao.exceptions.UserNotFoundException;
import hr.fer.zemris.opp.the7ups.malivinari.dao.exceptions.WineNotFoundException;
import hr.fer.zemris.opp.the7ups.malivinari.models.Communication;
import hr.fer.zemris.opp.the7ups.malivinari.models.Group;
import hr.fer.zemris.opp.the7ups.malivinari.models.Image;
import hr.fer.zemris.opp.the7ups.malivinari.models.Message;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.models.Wine;

public interface DAO {

	// user methods
	User getUser(String username) throws UserNotFoundException;
	User getUserByEmail(String email);
	User getUser(int id) throws UserNotFoundException;
	List<User> getAllUsers();
	Set<User> getBlockedUsers(int id);
	void addUser(User user) throws DAOException;
	void removeUser(int userId) throws UserNotFoundException, DAOException;
	void updateUser(User user) throws DAOException;
	void blockUser(int blockerId, int blockedId);
	void unblockUser(int blockerId, int blockedId);
	
	// wine methods
	List<Wine> getWines(int userId) throws UserNotFoundException;
	User getOwner(int wineId) throws WineNotFoundException;
	Wine getWine(int wineId) throws WineNotFoundException;
	Wine addWine(User owner, String name, byte[] imageBytes) throws UserNotFoundException, DAOException;
	void removeWine(int wineId) throws WineNotFoundException, DAOException;
	void updateWine(Wine wine) throws DAOException;
	
	// communication methods
	List<Communication> getCommunications(int userId) throws UserNotFoundException;
	List<Message> getMessages(int commId) throws CommunicationNotFoundException;
	Communication getCommunication(int commId) throws CommunicationNotFoundException;
	List<User> getParticipants(int commId);
	int startCommunication(Communication communication) throws DAOException;
	void removeCommunication(int commId) throws CommunicationNotFoundException;
	void appendCommunication(int commId, Message message) throws CommunicationNotFoundException, DAOException;
	void endCommunication(int commId);
	List<Communication> findAllCommunications();
	
	// group methods
	Set<Group> getGroups(int userId) throws UserNotFoundException;
	Set<User> getMembers(int groupId) throws GroupNotFoundException;
	Group getGroup(int groupId) throws GroupNotFoundException;
	int addGroup(Group group) throws DAOException;
	void joinGroup(int groupId, int userId) throws GroupNotFoundException, UserNotFoundException, DAOException;
	void leaveGroup(int groupId, int userId) throws GroupNotFoundException, UserNotFoundException, DAOException;
	void removeGroup(int groupId) throws GroupNotFoundException, DAOException;
	
	// image methods
	Image addImage(byte[] bytes);
	Image getImage(int imageId);
	
	// listener methods
	void addListener(DAOListener listener);
	void removeListener(DAOListener listener);
	void notifyListeners(DAOEvent event);
	
}
