package hr.fer.zemris.opp.the7ups.malivinari.gui;

import javafx.scene.Node;
import javafx.scene.control.Label;

public class SkinnableLabel extends Label implements Skinnable {
	
	public SkinnableLabel(){
		SkinContext.add(this);
		this.setTextFill(SkinContext.getSkinProvider().getFontColor());
	}
	
	public SkinnableLabel(String text, Node graphic) {
		super(text, graphic);
		SkinContext.add(this);
		this.setTextFill(SkinContext.getSkinProvider().getFontColor());
	}

	public SkinnableLabel(String text) {
		super(text);
		SkinContext.add(this);
		this.setTextFill(SkinContext.getSkinProvider().getFontColor());
	}

	@Override
	public void notifyProviderChanged(SkinProvider newProvider) {
		this.setTextFill(newProvider.getFontColor());
	}

}
