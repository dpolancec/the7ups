package hr.fer.zemris.opp.the7ups.malivinari.dao.jpa;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import hr.fer.zemris.opp.the7ups.malivinari.dao.DAO;
import hr.fer.zemris.opp.the7ups.malivinari.dao.DAOEvent;
import hr.fer.zemris.opp.the7ups.malivinari.dao.DAOListener;
import hr.fer.zemris.opp.the7ups.malivinari.dao.GroupDeletedEvent;
import hr.fer.zemris.opp.the7ups.malivinari.dao.exceptions.CommunicationNotFoundException;
import hr.fer.zemris.opp.the7ups.malivinari.dao.exceptions.DAOException;
import hr.fer.zemris.opp.the7ups.malivinari.dao.exceptions.GroupNotFoundException;
import hr.fer.zemris.opp.the7ups.malivinari.dao.exceptions.UserBlockedException;
import hr.fer.zemris.opp.the7ups.malivinari.dao.exceptions.UserNotFoundException;
import hr.fer.zemris.opp.the7ups.malivinari.dao.exceptions.WineNotFoundException;
import hr.fer.zemris.opp.the7ups.malivinari.models.Communication;
import hr.fer.zemris.opp.the7ups.malivinari.models.Group;
import hr.fer.zemris.opp.the7ups.malivinari.models.Image;
import hr.fer.zemris.opp.the7ups.malivinari.models.Message;
import hr.fer.zemris.opp.the7ups.malivinari.models.Model;
import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.models.Wine;

@Repository
public class HibernateDao implements DAO {

	private int test = new Random().nextInt();
	
	private List<DAOListener> listeners = new ArrayList<>();
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public User getUser(String username) throws UserNotFoundException {
		System.out.println(this.test);
		return (User)sessionFactory
				.getCurrentSession()
				.getNamedQuery(User.Q_USERNAME)
				.setParameter(User.Q_PARAM_USERNAME, username)
				.uniqueResult();
	}

	@Override
	public User getUser(int id) throws UserNotFoundException {
		System.out.println(this.test);
		return (User)sessionFactory.getCurrentSession().byId(User.class).load(id);
	}

	@Override
	public void addUser(User user) throws DAOException {
		persistModel(user);
	}

	@Override
	public void removeUser(int userId) throws UserNotFoundException, DAOException {
		User user = getUser(userId);
		for (Group group: user.getGroups()){
			group.getMembers().remove(user);
		}
		for (Communication comm: user.getCommunications()){
			comm.getParticipants().remove(user);
		}
		for (User other: getAllUsers()){
			other.getBlockedUsers().remove(user);
		}
		sessionFactory.getCurrentSession().delete(getUser(userId));
	}

	@Override
	public void updateUser(User user) throws DAOException {
		updateModel(user);
	}

	@Override
	public List<Wine> getWines(int userId) throws UserNotFoundException {
		return new ArrayList<Wine>(getUser(userId).getWines());
	}

	@Override
	public User getOwner(int wineId) throws WineNotFoundException {
		return getWine(wineId).getOwner();
	}

	@Override
	public Wine getWine(int wineId) throws WineNotFoundException {
		System.out.println(this.test);
		return (Wine)sessionFactory.getCurrentSession().byId(Wine.class).load(wineId);
	}

	@Override
	public Wine addWine(User owner, String name, byte[] imageBytes) throws UserNotFoundException, DAOException {
		Image image = addImage(imageBytes);
		Wine wine = new Wine();
		wine.setImage(image);
		wine.setOwner(owner);
		wine.setName(name);
		
		persistModel(wine);
		
		return wine;
	}

	@Override
	public void removeWine(int wineId) throws WineNotFoundException, DAOException {
		deleteModel(getWine(wineId));
	}

	@Override
	public void updateWine(Wine wine) throws DAOException {
		updateModel(wine);
	}

	@Override
	public List<Communication> getCommunications(int userId) throws UserNotFoundException {
		return new ArrayList<Communication>(getUser(userId).getCommunications());
	}

	@Override
	public List<Message> getMessages(int commId) throws CommunicationNotFoundException {
		return new ArrayList<Message>(getCommunication(commId).getMessages());
	}

	@Override
	public Communication getCommunication(int commId) throws CommunicationNotFoundException {
		Communication comm = (Communication)sessionFactory.getCurrentSession().byId(Communication.class).load(commId);
		return comm;
	}

	@Override
	public int startCommunication(Communication communication) throws DAOException {
		return saveModel(communication);
	}
	@Override
	public void removeCommunication(int commId) throws CommunicationNotFoundException {
		deleteModel(getCommunication(commId));
	}

	@Override
	public void appendCommunication(int commId, Message message) throws CommunicationNotFoundException, DAOException {
		
		Communication comm = getCommunication(commId);
		message.setCommunication(comm);
		comm.getMessages().add(message);
		persistModel(message);
		updateModel(comm);
	}

	@Override
	public void endCommunication(int commId) {
		Communication comm = getCommunication(commId);
		comm.setTimeEnded(new Date());
		updateModel(comm);
	}

	@Override
	public Set<Group> getGroups(int userId) throws UserNotFoundException {
		return getUser(userId).getGroups();
	}

	@Override
	public Set<User> getMembers(int groupId) throws GroupNotFoundException {
		return (Set<User>) removeIfDeleted(getGroup(groupId).getMembers());
	}

	@Override
	public Group getGroup(int groupId) throws GroupNotFoundException {
		return (Group)sessionFactory.getCurrentSession().byId(Group.class).load(groupId);
	}

	@Override
	public int addGroup(Group group) throws DAOException {
		return saveModel(group);
	}

	@Override
	public void joinGroup(int groupId, int userId) throws GroupNotFoundException, UserNotFoundException, DAOException {
		Group group = getGroup(groupId);
		User user = getUser(userId);
		
		Set<User> members = group.getMembers();
		Set<User> blockedUsers = new HashSet<User>();
		for (User member: members){
			blockedUsers.addAll(getBlockedUsers(member.getId()));
		}
		
		if (blockedUsers.contains(user)){
			throw new UserBlockedException(this);
		}
		
		members.add(user);
		updateModel(group);
	}

	@Override
	public void leaveGroup(int groupId, int userId) throws GroupNotFoundException, UserNotFoundException, DAOException {
		Group group = getGroup(groupId);
		User user = getUser(userId);
		
		group.getMembers().remove(user);
		updateModel(group);
		
		if (getMembers(groupId).isEmpty()){
			deleteModel(group);
		}
	}

	@Override
	public void removeGroup(int groupId) throws GroupNotFoundException, DAOException {
		Group group = getGroup(groupId);
		GroupDeletedEvent event = new GroupDeletedEvent(group);
		deleteModel(group);
		notifyListeners(event);
	}
	
	@Override
	public List<User> getParticipants(int commId) {
		return new ArrayList<User>(getCommunication(commId).getParticipants());
	}
	
	@Override
	public void blockUser(int blockerId, int blockedId) {
		User blocker = getUser(blockerId);
		User blocked = getUser(blockedId);
		blocker.getBlockedUsers().add(blocked);
		updateModel(blocker);
		
		Set<Group> sharedGroups = new HashSet<Group>(blocker.getGroups());
		sharedGroups.retainAll(blocked.getGroups());
		
		for (Group group: sharedGroups){
			removeGroup(group.getId());
		}
	}
	
	@Override
	public Set<User> getBlockedUsers(int id) {
		return (Set<User>) removeIfDeleted(getUser(id).getBlockedUsers());
	}
	
	private void deleteModel(Model model){
		Session session = sessionFactory.getCurrentSession();
		model.setTimeCreated();
		model.setTimeUpdated(model.getTimeCreated());
		session.delete(model);
		session.flush();
	}
	
	private void updateModel(Model model){
		Session session = sessionFactory.getCurrentSession();
		model.setTimeUpdated(new Date());
		session.merge(model);
		session.flush();
	}

	private void persistModel(Model model){
		Session session = sessionFactory.getCurrentSession();
		model.setTimeCreated();
		model.setTimeUpdated(model.getTimeCreated());
		session.persist(model);
		session.flush();
	}
	
	private int saveModel(Model model){
		Session session = sessionFactory.openSession();
		model.setTimeCreated();
		model.setTimeUpdated(model.getTimeCreated());
		int id = (Integer) session.save(model);
		session.flush();
		session.close();
		
		return id;
	}
	
	
	private Collection<User> removeIfDeleted(Collection<User> users){
		Iterator<User> iter = users.iterator();
		while(iter.hasNext()){
			if (iter.next().isDeleted()){
				iter.remove();
			}
		}
		
		return users;
	}
	
	@SuppressWarnings("unused")
	private User nullifyIfDeleted(User user){
		if (user != null && user.isDeleted()){
			return null;
		}
		
		return user;
	}

	@Override
	public User getUserByEmail(String email) {
		return (User)sessionFactory
				.getCurrentSession()
				.getNamedQuery(User.Q_EMAIL)
				.setParameter(User.Q_PARAM_EMAIL, email)
				.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAllUsers() {
		return (List<User>)removeIfDeleted((List<User>)sessionFactory
				.getCurrentSession()
				.getNamedQuery(User.Q_ALL)
				.list());
	}

	@Override
	public Image addImage(byte[] bytes) {
		if (bytes == null || bytes.length == 0){
			return null;
		}
		Blob blob = Hibernate.getLobCreator(sessionFactory.getCurrentSession()).createBlob(bytes);
		Image image = new Image();
		image.setData(blob);
		
		return getImage(saveModel(image));
	}
	
	public Image getImage(int imageId){
		return (Image)sessionFactory.getCurrentSession().byId(Image.class).load(imageId);
	}

	@Override
	public void addListener(DAOListener listener) {
		System.out.println("DAO: Register listener " + listener);
		listeners.add(listener);
	}

	@Override
	public void removeListener(DAOListener listener) {
		listeners.remove(listener);
	}

	@Override
	public void notifyListeners(DAOEvent event) {
		listeners.forEach((listener) -> listener.notify(event, this));
	}

	@Override
	public void unblockUser(int blockerId, int blockedId) {
		User blocker = getUser(blockerId);
		blocker.getBlockedUsers().remove(getUser(blockedId));
		updateModel(blocker);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Communication> findAllCommunications() {
		return (List<Communication>)sessionFactory
				.getCurrentSession()
				.getNamedQuery(Communication.Q_ALL)
				.list();
	}

}
