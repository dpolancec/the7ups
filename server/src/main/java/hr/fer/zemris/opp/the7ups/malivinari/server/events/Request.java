package hr.fer.zemris.opp.the7ups.malivinari.server.events;

public class Request {

	private ServerEvent event;
	private int numAccepts;
	private boolean isAccepted;
	private Runnable onAccepted;
	
	public Request(ServerEvent event, Runnable onAccepted) {
		this.event = event;
		this.onAccepted = onAccepted;
	}

	public Runnable getOnAccepted() {
		return onAccepted;
	}

	public void setOnAccepted(Runnable onAccepted) {
		this.onAccepted = onAccepted;
	}

	public synchronized ServerEvent getEvent() {
		return event;
	}

	public synchronized void setEvent(ServerEvent event) {
		this.event = event;
	}

	public synchronized int getNumAccepts() {
		return numAccepts;
	}

	public synchronized void setNumAccepts(int numAccepts) {
		this.numAccepts = numAccepts;
		if (numAccepts >= event.getRelevantUsers().size()){
			isAccepted = true;
		}
	}

	public synchronized boolean isAccepted() {
		return isAccepted;
	}

	public synchronized void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((event == null) ? 0 : event.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Request other = (Request) obj;
		if (event == null) {
			if (other.event != null)
				return false;
		} else if (!event.equals(other.event))
			return false;
		return true;
	}

}
