package hr.fer.zemris.opp.the7ups.malivinari.server;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import hr.fer.zemris.opp.the7ups.malivinari.models.User;
import hr.fer.zemris.opp.the7ups.malivinari.utils.HashUtils;

public class Session {

	private User user;
	private final String token;
	private Date lastSeen;
	private boolean busy;
	
	@Autowired
	private ServerContext serverContext;
	
	public Session(String token){
		this.token = token;
	}
	
	public Session(User user) {
		this.user = user;
		this.token = user.getId() + "_" + HashUtils.hashString(user.getUsername());
		this.lastSeen = new Date();
	}
	
	public User getUser() {
		return user;
	}

	public String getToken() {
		return token;
	}

	public synchronized Date getLastSeen() {
		return lastSeen;
	}

	public synchronized void see(){
		lastSeen = new Date();
	}
	
	public void destroy(){
		serverContext.removeSession(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Session other = (Session) obj;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		return true;
	}

	public synchronized boolean isBusy() {
		return busy;
	}

	public synchronized void setBusy(boolean busy) {
		see();
		this.busy = busy;
	}
	
	

}
